package communications;


class CommunicationErrors
{
    public enum FailReason
    {
        DIDNT_FAIL,
        CANNOT_ESTABLISH_CONNECTION,
        TIMEOUT_ESTABLISHING_CONNECTION,
        CANNOT_SEND_OBJECT,
        CANNOT_RECEIVE_OBJECT,
        CANNOT_SETUP_SOCKET,
        CANNOT_RECEIVE_COMMAND,
        RECEIVED_INVALID_OBJECT
    }

    static class CommunicationException extends Exception
    {
        FailReason failReason;

        CommunicationException(String msg, Throwable cause, FailReason failReason)
        {
            super(msg,cause);
            this.failReason = failReason;
        }

        CommunicationException(String msg)
        {
            super(msg);
        }
    }
}