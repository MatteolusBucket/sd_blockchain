package communications;


import blockchain.blocks.IBlock;
import utils.Commons;

/**
 * Command to send a block to a node, even if he didn't ask for it. (e.g. used to spread new blocks in the graph)
 */
class CommandSpreadBlock extends SuperCommand
{
    public static final long serialVersionUID = 1L;

    private IBlock block;
    private int timeToLive;
    static final int INITIAL_TIME_TO_LIVE = 100;
    CommandSpreadBlock(IBlock block, int timeToLive, long transactionId)
    {
        super(transactionId);
        this.block = block;
        this.timeToLive=timeToLive;
    }
    CommandSpreadBlock(IBlock block, int timeToLive)
    {
        this.block = block;
        this.timeToLive=timeToLive;
    }

    public CommandSpreadBlock(IBlock block)
    {
        this(block,INITIAL_TIME_TO_LIVE);
    }



    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        Commons.logger.info("Receive command with block id "+ block.getID() + " ["+ block.getMinerAddress() +"] from " + sender);

        if(cm.getBlockTransactionExchanger().onReceive(block) && timeToLive>1)//se non mi era mai arrivato
        {
            Commons.logger.info("Accepted block "+block.getID()+" ["+ block.getMinerAddress() +"] with ttl = "+timeToLive+" from "+sender);
            cm.spreadBlockAsync(block, timeToLive - 1, sender);
        }
        else
        {
            Commons.logger.info("Rejected block "+block.getID()+" ["+ block.getMinerAddress() +"] with ttl = "+timeToLive+" from "+sender);
        }
        return new CommandReturn(null);
    }


    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        return null;
    }
}
