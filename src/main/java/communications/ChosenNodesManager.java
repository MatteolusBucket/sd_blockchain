package communications;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import utils.Commons;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

/**
    This class manages the serverNode we decide to startConnection to. We will receive transaction/blocks from these nodes and we will also
    send them our blocks/transactions
 */
class ChosenNodesManager
{
    private final ConcurrentSkipListMap<Node, Node> chosenNodes = new ConcurrentSkipListMap<>();
    private static final int MAX_CHOSEN_NODES = 16;
    private final Object waitForAtLeastOneChosenNodeNotifier = new Object();

    /**
     * Closes all the current subscriptions and opens new ones
     * @param cm the communications manager
     */
    synchronized void updateNodes(CommunicationsManager cm)
    {
        Commons.logger.info("updating subscriptions...");
        int prevChosenNodesSize = chosenNodes.size();
        //Thread.dumpStack();
        ConcurrentSkipListMap<Node,RemoteNode> reachableNodes = cm.getReachableNodes();
        List<Node> nodesSubscribedToUs = cm.getIncomingSubscriptionsManager().getNodes();
        for(Node n : nodesSubscribedToUs)
        {
            if(!reachableNodes.containsKey(n))
                reachableNodes.put(n,new RemoteNode(n));
        }
        if(reachableNodes.size()==0)
        {
            Commons.logger.warning("No reachable chosenNodes! no new subscriptions");
            return;
        }

        /*
            The idea is to choose chosenNodes at different distances. After ordering the reachable chosenNodes by increasing ping
            we choose 8 'stepNodes' at increasing distance. The first is the first reachable serverNode in the list, the
            second the second, the third the fourth, then the eigth, the 16th, the 32th and the 64th. So its indexes 0,1,3,7,8,15,31,63
            Then the remaining chosenNodes are chosen starting from these 'stepNodes'. We choose the nodes after the stepNodes
         */

        List<RemoteNode> candidates = new ArrayList<>(reachableNodes.values());
        Comparator<RemoteNode> byRttComparator = (rn1, rn2) -> (int)(rn1.getEstimatedRttNano()/1000_000-rn2.getEstimatedRttNano()/1000_000);
        candidates.sort(byRttComparator);

        /*
        Ad ogni giro del while analizzo un nodo in ognuno dei segmenti delimitati dagli step.
        Se il nodo è già stato scelto non faccio nulla, se no lo scelgo.
        Così anche se ci sono dei nodi già scelti mantengo il bilanciamento tra i vari segmenti
         */

        int[] intervalStartingpoints = new int[8];
        for(int i=0;i<intervalStartingpoints.length;i++)
            intervalStartingpoints[i] = (int)Math.pow(2,i)-1;
        int[] nextNodeIndexToLookAtInStep = new int[8];
        for(int i=0;i<nextNodeIndexToLookAtInStep.length;i++)
            nextNodeIndexToLookAtInStep[i] = intervalStartingpoints[i];

        Commons.logger.info("candidates="+candidates);
        int consideredNodes = 0;
        while(chosenNodes.size()<MAX_CHOSEN_NODES && consideredNodes<candidates.size())
        {
        /*    try
            {
                Thread.sleep(5000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }*/
        //    Commons.logger.info("subscriptions update loop: chosenNodes.size()="+chosenNodes.size()+"  candidates.size()="+candidates.size()+"   consideredNodes="+consideredNodes);
            for (int i = 0; i < nextNodeIndexToLookAtInStep.length; i++)
            {//    Commons.logger.info("interval "+i+" reached "+nextNodeIndexToLookAtInStep[i]);
                if(nextNodeIndexToLookAtInStep[i]>=candidates.size())
                {
            //        Commons.logger.info("interval "+i+" depleted");
                    continue;//allora è l'ultimo intervallo e l'abbiamo esaurito
                }
                if(i+1<intervalStartingpoints.length && nextNodeIndexToLookAtInStep[i]>=intervalStartingpoints[i+1])
                {
            //        Commons.logger.info("interval "+i+" depleted");
                    continue;//allora non è l'ultimo intervallo e l'abbiamo esaurito
                }

                RemoteNode nextNodeInStep = candidates.get(nextNodeIndexToLookAtInStep[i]);
            //    Commons.logger.info("thinking of subscribing to "+nextNodeInStep);
                consideredNodes++;

                if (!chosenNodes.containsKey(nextNodeInStep.getNode()))//se non è già scelto
                {
                    //sceglilo

                //    Commons.logger.info("attempting to subscribe to"+nextNodeInStep);
                    try
                    {
                        cm.getNodeConnectionsManager().useConnectionToNode(nextNodeInStep.getNode(), cm, false, connectedNode -> {

                            if (connectedNode != null)
                            {
                                boolean subscriptionAccepted = connectedNode.subscribeToNode();
                                if (subscriptionAccepted)
                                {
                                    Commons.logger.info("successfully subscribed to "+nextNodeInStep);
                                    chosenNodes.put(nextNodeInStep.getNode(), connectedNode.getNode());
                                    //Commons.logger.info("added "+nextNodeInStep+" to subscriptons list");
                                }
                                else
                                {
                                    Commons.logger.info("can't subscribe to " + nextNodeInStep);
                                }
                            }
                            else
                            {
                                Commons.logger.info("can't startConnection to " + nextNodeInStep);
                            }
                        });
                    }
                    catch (CommunicationErrors.CommunicationException e)
                    {
                        Commons.logger.info("couldn't connect to "+nextNodeInStep+" to try the subscription");
                    }
                }
                //e passa al prossimo nodo
                nextNodeIndexToLookAtInStep[i]++;
            }
        }
        //Commons.logger.info("subscriptions update loop completed (chosenNodes.size()="+chosenNodes.size()+")");

        if(chosenNodes.size()>prevChosenNodesSize)
        {
            cm.startNewThread(()->cm.addKnownNodes(askForNeighbors(cm)));
            synchronized (waitForAtLeastOneChosenNodeNotifier)
            {
                waitForAtLeastOneChosenNodeNotifier.notifyAll();
            }
        }

        Commons.logger.info("subscriptions updated (chosenNodes.size()="+chosenNodes.size()+", reachableNodes.size()="+reachableNodes.size()+")");
    }


    synchronized void unsubscribeOneRandomNode(CommunicationsManager cm)
    {
        if(chosenNodes.size()==0)
            return;
        Node node = chosenNodes.firstKey();
        for(int i=0;i<(chosenNodes.size()-1)*Math.random();i++)
            node = chosenNodes.higherKey(node);

        final Node theNode = node;
        try
        {
            cm.getNodeConnectionsManager().useConnectionToNode(node, cm, false, connectedNode -> {
                    connectedNode.sendUnsubscriptionRequest();
            });
            chosenNodes.remove(theNode);
        }
        catch (CommunicationErrors.CommunicationException e)
        {
            Commons.logger.info("couldn't connect to "+theNode+" to unsubscribe");
        }
    }

    void disconnectAll(CommunicationsManager cm)
    {
        Commons.logger.info("unsubscribing from all nodes");
        Node node;
        try
        {
            node = chosenNodes.firstKey();
        }
        catch (NoSuchElementException e)
        {
            return;
        }

        while(node!=null)
        {
            if(cm.getNodeConnectionsManager().isNodeRegistered(node))
            {
                try
                {
                    cm.getNodeConnectionsManager().useConnectionToNode(node, cm, true, cnode -> {
                        cnode.sendUnsubscriptionRequest();
                        //cnode.disconnect(cm);
                    });
                }
                catch (CommunicationErrors.CommunicationException e)
                {
                    //well, thats not a problem
                }
                chosenNodes.remove(node);
                node = chosenNodes.higherKey(node);
            }
        }

        synchronized (chosenNodes)
        {
            chosenNodes.clear();
        }
    }

    void onNodeDisconnected(Node node, CommunicationsManager cm)
    {
        chosenNodes.remove(node);
        if(cm.getNodeConnectionsManager().isNodeRegistered(node))
        {
            try
            {
                cm.getNodeConnectionsManager().useConnectionToNode(node, cm, true, cnode -> {
                    cnode.sendUnsubscriptionRequest();
                    //cnode.disconnect(cm);
                });
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                //well, thats not a problem
            }
            chosenNodes.remove(node);
            node = chosenNodes.higherKey(node);
        }
    }



    /**
     * waits for at least one connection to be established (if there is already at least one it returns immediately)
     * @param timeoutMillis timeout for the waiting, if negative it means to wait indefinitely
     * @throws InterruptedException if the thread gets interrupted
     */
    void waitForAtLeastOneChosenNode(long timeoutMillis) throws InterruptedException
    {
        long t0 = System.currentTimeMillis();
        synchronized (waitForAtLeastOneChosenNodeNotifier)
        {
            while(chosenNodes.size()<1 && (timeoutMillis<0 || System.currentTimeMillis()-t0<timeoutMillis))
            {
                //Commons.logger.warning("waiting...");
                if(timeoutMillis>0)
                    waitForAtLeastOneChosenNodeNotifier.wait(timeoutMillis - (System.currentTimeMillis()-t0));
                else
                    waitForAtLeastOneChosenNodeNotifier.wait();
                //Commons.logger.warning("nodesWeAreSubscribedTo.size()="+chosenNodes.size());
            }
        }
    }

    int chosenNodesNumber()
    {
        return chosenNodes.size();
    }

    boolean wantsMoreNodes()
    {
        return chosenNodesNumber()<MAX_CHOSEN_NODES;
    }

    /**
     * Asks to the chosen nodes for the blocks with the specified id
     * @param id the blocs id
     * @return A set containing the blocks that were found. If no block is found an empty set is returned
     */
    Set<IBlock> askBlock(int id, CommunicationsManager cm)
    {
        if(chosenNodes.size()<1)
        {
            Commons.logger.warning(" no supplier nodes");
            return new HashSet<>();
        }

        Set<IBlock> blocks = new HashSet<>();
        Node node = chosenNodes.firstKey();
        while(node!=null)
        {
            try
            {
                cm.getNodeConnectionsManager().useConnectionToNode(node, cm, false, cnode -> {
                    List<IBlock> newBlocks = cnode.askBlock(id);
                    if (newBlocks != null)
                        blocks.addAll(newBlocks);
                });
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                Commons.logger.warning("couldn't ask blocks to "+node+", couldn't establish connection");
            }
            node=chosenNodes.higherKey(node);
        }

        return blocks;
    }


    /**
     * Asks to the chosen nodes for their neighbors
     * @return A set containing the blocks that were found. If no block is found an empty set is returned
     */
    List<RemoteNode> askForNeighbors(CommunicationsManager cm)
    {
        if(chosenNodes.size()<1)
        {
            Commons.logger.warning(" no supplier nodes");
            return new ArrayList<>();
        }

        Set<RemoteNode> allNewNodes = new HashSet<>();
        Node node = chosenNodes.firstKey();
        while(node!=null)
        {
            try
            {
                cm.getNodeConnectionsManager().useConnectionToNode(node, cm, false, cnode -> {
                    List<RemoteNode> newNodes = cnode.askForNeighbors();
                    if (newNodes != null)
                    {
                        List<RemoteNode> correctedNodes = new ArrayList<>();
                        for(int i =0;i<newNodes.size();i++)
                        {
                            RemoteNode rn = newNodes.get(i);
                            if(rn.getNode().getInetAddress().isLoopbackAddress())
                            {
                                newNodes.remove(i);
                                i--;
                                //we can't add directly to newRemoteNodes, it would cause an infinite loop
                                correctedNodes.add(new RemoteNode(new Node(cnode.getNode().getInetAddress(),rn.getNode().getPort())));
                            }
                        }
                        allNewNodes.addAll(newNodes);
                        allNewNodes.addAll(correctedNodes);
                    }
                });
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                Commons.logger.warning("couldn't ask blocks to "+node+", couldn't establish connection");
            }
            node=chosenNodes.higherKey(node);
        }

        return new ArrayList<>(allNewNodes);
    }

    List<Node> getNodes()
    {
        return new ArrayList<>(chosenNodes.keySet());
    }

    int askLastKnownBlockId(CommunicationsManager cm)
    {
        if(chosenNodes.size()==0)
            return 0;
        final Object synchronizer = new Object();
        int[] maxId = new int[]{0};
        CommUtils.runInParallel((node)->{
//            Commons.logger.info("estimating rtt for "+remoteNode);

            try
            {
                cm.getNodeConnectionsManager().useConnectionToNode((Node)node,cm, false,(cnode)->{
                    int id = cnode.askLastKnownBlockId();
                    synchronized(synchronizer)
                    {
                        if(id>maxId[0])
                            maxId[0]=id;
                    }
                });
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                Commons.logger.warning("couldn't ask last block id to "+node+", couldn't establish connection");
            }
        } , 10, chosenNodes);

        return maxId[0];
    }








    /**
     * Sends the block to all the subscribed nodes
     * @param block the block to be sent
     * @return In the first element the number of sends attempted, in the second the number of nodes to which the block was successfully sent
     */
    Integer[] spreadBlock(IBlock block, int timeToLive, CommunicationsManager cm, Node excludedNode)
    {
        return spread(block,timeToLive,cm,excludedNode);
    }


    /**
     * Sends the transaction to all the subscribed nodes
     * @param transaction the block to be sent
     * @return In the first element the number of sends attempted, in the second the number of nodes to which the transaction was successfully sent
     */
    Integer[] spreadTransaction(ITransaction transaction, int timeToLive, CommunicationsManager cm, Node excludedNode)
    {
        return spread(transaction,timeToLive,cm, excludedNode);
    }

    private Integer[] spread(Object bOrT, int timeToLive, CommunicationsManager cm, Node excludedNode)
    {
        Node node;
        try
        {
            node = chosenNodes.firstKey();
        }
        catch (NoSuchElementException e)
        {
            return new Integer[]{0,0};
        }

        int successfulSends = 0;
        int attemptedSends = 0;
        while(node!=null)
        {
            if(!node.equals(excludedNode))
            {
                boolean[] res=new boolean[]{false};
                try
                {
                    if (bOrT instanceof IBlock)
                    {
                        cm.getNodeConnectionsManager().useConnectionToNode(node,cm,false, cnode->{
                            res[0] = cnode.sendBlock((IBlock) bOrT, timeToLive);
                        });
                    }
                    else if (bOrT instanceof ITransaction)
                    {
                        cm.getNodeConnectionsManager().useConnectionToNode(node,cm,false, cnode->{
                            res[0] = cnode.sendTransaction((ITransaction) bOrT, timeToLive);
                        });
                    }
                    else
                        throw new IllegalArgumentException("neither a block nor a transaction, it's a " + bOrT.getClass().getName());
                }
                catch (CommunicationErrors.CommunicationException e)
                {
                    Commons.logger.warning("couldn't sends block/transaction to "+node+" , couldn't establish connection");
                }
                if (res[0])
                    successfulSends++;
                attemptedSends++;
            }
            node = chosenNodes.higherKey(node);
        }
        return new Integer[]{attemptedSends,successfulSends};
    }
}
