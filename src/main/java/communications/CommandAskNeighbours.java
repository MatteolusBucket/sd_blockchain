package communications;


import utils.Commons;

import java.util.ArrayList;
import java.util.List;

/**
 * Command to ask a Node for the remote nodes he knows.
 * The {@link communications.SuperCommand.CommandReturn#arg } in the Response contains a {@link java.util.List} Object containing instances of {@link RemoteNode}
 * containing the obtained remote nodes (if successful)
 */
class CommandAskNeighbours extends SuperCommand
{
    public static final long serialVersionUID = 1L;


    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        List<Node> nodes = cm.getReachableNodes(sender);

        Commons.logger.info("CommandAskNeighbours: sending back "+nodes.size()+" addresses:"+nodes);
        return new CommandReturn(nodes);
    }

    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        Object o =  res.getArg();
        if(o instanceof List)
        {
            if(((List)o).size()>0 && !(((List)o).get(0) instanceof Node))//se però è una lista che non contiene RemoteNode
            {
                Commons.logger.warning("received invalid List");
                return null;
            }
            List<RemoteNode> remoteNodes = new ArrayList<>();
            List<Node> nodes = (List<Node>)o;
            for(Node n : nodes)
            {
                remoteNodes.add(new RemoteNode(n));
            }
            return remoteNodes;
        }
        else
        {
            Commons.logger.warning("received invalid response");
            return null;
        }
    }
}
