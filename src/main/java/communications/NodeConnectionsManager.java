package communications;

import utils.Commons;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Consumer;

/**
 * Manages all the connected registeredConnections, which then are used by {@link ChosenNodesManager} and {@link IncomingSubscriptionsManager}
 */
public class NodeConnectionsManager
{
    private final ConcurrentSkipListMap<Node, NodeConnection> registeredConnections = new ConcurrentSkipListMap<>();

    private static final int MAX_INCOMING_CONNECTIONS = 100;
    private boolean stopped = false;

    public synchronized void stop(CommunicationsManager cm)
    {
        if(stopped)
        {
            Commons.logger.warning("tried to stop NodeConnectionsManager twice");
            return;
        }
        stopped = true;
        Node node;
        try
        {
            node = registeredConnections.firstKey();
        }
        catch (NoSuchElementException e)
        {
            return;
        }

        while(node!=null)
        {
            try
            {
                useConnectionToNode(node, cm, false, cnode -> {
                    try
                    {
                        cnode.waitConnection(5000);
                    }
                    catch (InterruptedException e)
                    {
                        //do nothing
                    }
                    if (cnode.isConnectionEstablished())
                        cnode.askToDisconnect();
                    else if (!cnode.isDisconnecting())
                        cnode.askToDisconnectForced();
                });
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                //no problem
            }

            node = registeredConnections.higherKey(node);
        }
    }


    /**
     * Registers a connection from the specified node
     * @param clientNode
     * @return 0 if all good, -1 if already enough connections registered, -2 if there is already a connection for this node
     */
    synchronized int registerIncomingConnection(NodeConnection clientNode)
    {
        if(registeredConnections.size()>MAX_INCOMING_CONNECTIONS)
            return -1;
        if(isNodeRegistered(clientNode.getNode()))
            return -2;
        registeredConnections.put(clientNode.getNode(),clientNode);
        return 0;
    }

    /**
     * Registers a connection to the specified node
     * @param serverNode
     * @return if the connection registration was accepted (i.e. there wasnt already a connection for this node)
     */
    synchronized boolean registerOutgoingConnection(NodeConnection serverNode)
    {
        Commons.logger.fine("registering outgoing connection");
        if(isNodeRegistered(serverNode.getNode()))
            return false;
        //Commons.logger.info("registering outgoing connection: adding node");
        registeredConnections.put(serverNode.getNode(),serverNode);
        return true;
    }

    /**
     * Unregisters a connection that was registered by {@link #registerIncomingConnection(NodeConnection)} or {@link #registerOutgoingConnection(NodeConnection)}
     * @param node
     */
    synchronized void unregisterConnection(NodeConnection node)
    {
        Object v = registeredConnections.remove(node.getNode());
        if(v==null)
            Commons.logger.warning("tried to unregister a non-registered node ("+node+")");
    }

    private final Object getConnectionGuard = new Object();
    private NodeConnection getConnection(Node node, CommunicationsManager cm, boolean dontCreateNewConnection)
    {
        synchronized (getConnectionGuard)
        {
            int attemptsCount=0;
            boolean isEstablished = false;
            Commons.logger.fine("getConnection...");
            while(!isNodeRegistered(node) && attemptsCount<3 /*&& !isEstablished*/)
            {
                attemptsCount++;
                if (dontCreateNewConnection)
                    return null;
                Commons.logger.fine("recreating connection...");
                NodeConnection cnode = new NodeConnection(node);//NodeConnection.FACTORY.getConnectionObject(node);
                cnode.startConnection(cm);
                try
                {
                    isEstablished = cnode.waitConnection(10_000);
                }
                catch (InterruptedException e)
                {
                    Commons.logger.fine("recreating connection waitConnection timed out");
                    return null;
                }
                if (!isEstablished && attemptsCount < 3)
                {
                    Commons.logger.info("connection to " + node + " failed, retrying in a few seconds");
                    try
                    {
                        Thread.sleep((int) (3000 * Math.random()));
                    }
                    catch (InterruptedException e)
                    {
                        break;
                    }
                }
            }
            NodeConnection cnode = registeredConnections.get(node);
            if (cnode == null)
                return null;
            cnode.holdConnection();
            return cnode;
        }
    }

    private void giveBackConnection(NodeConnection cnode)
    {
        cnode.freeConnection();
    }

    /**
     * Permits to operate on a connection to the specified node
     * If the connection isnt established it will establish it for the occasion
     * @param node the node to startConnection to
     * @param cm the CommunicationsManager
     * @param dontCreateNewConnection if this is set to true the method will fail instead of creating a new connection
     * @param stuffToDo The operations taht have to be performed on the connection
     * @throws CommunicationErrors.CommunicationException If the method tries to establish a new connection but is unsuccessful
     */
    void useConnectionToNode(Node node, CommunicationsManager cm, boolean dontCreateNewConnection, Consumer<NodeConnection> stuffToDo) throws CommunicationErrors.CommunicationException
    {
        if(stopped)
            throw new CommunicationErrors.CommunicationException("unallowed connection usage, already stopped");
        Commons.logger.fine("useConnectionToNode...");
        NodeConnection cnode = getConnection(node,cm,dontCreateNewConnection);
        if(cnode==null)
            throw new CommunicationErrors.CommunicationException("couldn't establish connection to "+cnode);

        stuffToDo.accept(cnode);

        giveBackConnection(cnode);
    }

    synchronized boolean isNodeRegistered(Node node)
    {
        return registeredConnections.containsKey(node);
    }


    synchronized boolean isNodeConnectionEstablished(Node node)
    {
        if(!isNodeRegistered(node))
            return false;
        return registeredConnections.get(node).isConnectionEstablished();//established implies negotiatied
    }

    int nodesNumber()
    {
        return registeredConnections.size();
    }

    List<Node> getRegisteredConnections()
    {
        return new ArrayList<>(registeredConnections.keySet());
    }








    public static class DialogueDuplex extends Dialogue
    {
        transient NodeConnection serverNode;

        public DialogueDuplex(int senderPort, NodeConnection nodeConnection)
        {
            super(senderPort);
            this.serverNode = nodeConnection;
        }

        @Override
        protected SuperCommand.CommandReturn run(ObjectOutputStream out, ObjectInputStream in, Node server, CommunicationsManager cm, Socket socket) throws CommunicationErrors.CommunicationException
        {
            Commons.logger.fine("running DuplexDialogue for "+server);
            boolean didRegisterSuccessfully = cm.getNodeConnectionsManager().registerOutgoingConnection(serverNode);
            if(!didRegisterSuccessfully)
            {
                //this shouldn't really happen that often
                Commons.logger.warning("Outgoing connection rejected by the NodeConnectionsManager");
                return new SuperCommand.CommandReturn(false,"Outgoing connection rejected by the NodeConnectionsManager",null);
            }

            serverNode.setupConnectionDetails(out, in, cm, socket);
            boolean wasAccepted = serverNode.clientNegotiation(in, out, socket, cm);//onConnected is called inside here
            if (wasAccepted)
            {
                Commons.logger.fine("client negotiation successful");
                serverNode.setOnDisconnectingCallback(()->{cm.getNodeConnectionsManager().unregisterConnection(serverNode);});
                serverNode.listenConnection();
            }
            else
            {
                cm.getNodeConnectionsManager().unregisterConnection(serverNode);
                Commons.logger.info("clientNegotiation toward " + serverNode + " failed");
            }

            serverNode.abort();
            //NodeConnection.FACTORY.giveBackConnectionObject(serverNode);
            Commons.logger.info("DialogueDuplex with "+serverNode+": run ended");
            return null;
        }

        @Override
        protected int handle(ObjectOutputStream out, ObjectInputStream in, CommunicationsManager cm, InetAddress senderAddress, Socket socket)
        {
            Commons.logger.fine("handling incoming DuplexDialogue from "+senderAddress);
            RemoteNode rNode = cm.getKnownNode(senderAddress, getSenderPort());
            Commons.logger.fine("got known node");
            if(rNode==null)
            {
                Commons.logger.warning("handling incoming dialogue from unknown node "+senderAddress+":"+getSenderPort());
                return -1;
            }
            Commons.logger.fine("building NodeConnection");
            NodeConnection clientNode = new NodeConnection(rNode.getNode());//NodeConnection.FACTORY.getConnectionObject(rNode.getNode());

            clientNode.setupConnectionDetails(out,in,cm,socket);
            int negotiationResult = clientNode.serverNegotiation(out, socket, cm);
            if (negotiationResult==0)
            {
                Commons.logger.fine("server negotiation successful");
                clientNode.setOnDisconnectingCallback(()->{cm.getNodeConnectionsManager().unregisterConnection(clientNode);});
                clientNode.listenConnection();
            }
            else
            {
                Commons.logger.info("refused connection request from " + clientNode+" negotiation result is "+negotiationResult);
            }

            clientNode.abort();
            //NodeConnection.FACTORY.giveBackConnectionObject(clientNode);
            Commons.logger.info("DialogueDuplex with "+clientNode+": handle ended");
            return 0;
        }


    }
}
