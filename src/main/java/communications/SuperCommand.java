package communications;

import utils.Commons;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;

/**
 * Abstract class representing a generic command to be sent to a {@link RemoteNode}
 */
abstract class SuperCommand implements Serializable
{
    private long commandId;//used to distinguish different commands

    SuperCommand(long commandId)
    {
        this.commandId = commandId;
    }

    SuperCommand()
    {
        this(0);
    }

    final void send(ObjectOutputStream out) throws CommunicationErrors.CommunicationException
    {
        try
        {
            out.writeObject(this);
        }
        catch (IOException e)
        {
            throw new CommunicationErrors.CommunicationException("IOException, send unsuccessful", e,CommunicationErrors.FailReason.CANNOT_SEND_OBJECT);
        }
    }


    final void handleAndRespond(ObjectOutputStream out, CommunicationsManager cm, InetAddress senderAddress, int senderPort) throws CommunicationErrors.CommunicationException
    {
        RemoteNode senderNode = new RemoteNode(senderAddress,senderPort);

        CommandReturn ret = handleCommand(cm,senderNode.getNode());
        ret.setCommandId(commandId);
        try
        {
            out.writeObject(ret);
        }
        catch(IOException e)
        {
            throw new CommunicationErrors.CommunicationException("IOException, couldn't send response", e,CommunicationErrors.FailReason.CANNOT_SEND_OBJECT);
        }
    }

    final CommandReturn waitResponse(ObjectInputStream in) throws CommunicationErrors.CommunicationException
    {
        CommandReturn response;
        Commons.logger.fine("waiting command response, command = "+this.getClass().getSimpleName()+" i'm "+Thread.currentThread().getName());
        try
        {
            response = (CommandReturn) in.readObject();
        }
        catch (IOException e)
        {
            throw new CommunicationErrors.CommunicationException("IOException, receive unsuccessful", e,CommunicationErrors.FailReason.CANNOT_RECEIVE_OBJECT);
        }
        catch (ClassNotFoundException e)
        {
            throw new CommunicationErrors.CommunicationException("ClassNotFoundException, class of received serialized object cannot be found", e,CommunicationErrors.FailReason.CANNOT_RECEIVE_OBJECT);
        }
        Commons.logger.fine("received command response, command = "+this.getClass().getSimpleName());
        return response;
    }

    /**
     * Executed on the receiving side. Here you can handleCommand the received data and generate the response
     * @param cm the communicationsManager,
     * @param sender the serverNode we received the command from
     * @return the response to be sent to the sender
     */
    abstract CommandReturn handleCommand(CommunicationsManager cm, Node sender);

    /**
     * extracts the response object from a commandReturn checking for it to be conforming to the command
     * @param res the CommandReturn to read the response from
     * @return the command response
     */
    abstract Object getResponseArgsChecking(CommandReturn res);

    long getCommandId()
    {
        return commandId;
    }

    void setCommandId(long commandId)
    {
        this.commandId = commandId;
    }

    /**
     * Class describing the Response the exchange gives to the client
     */
    protected static class CommandReturn implements Serializable
    {
        private boolean isOk;
        private String  errorMsg;
        private Object arg;
        private long commandId;

        boolean isOk()
        {
            return isOk;
        }

        String getErrorMsg()
        {
            return errorMsg;
        }

        Object getArg()
        {
            return arg;
        }

        public CommandReturn(Object arg)
        {
            this(true,null,arg);
        }

        public CommandReturn(boolean isOk, String errorMsg, Object arg)
        {
            this.isOk=isOk;
            this.errorMsg=errorMsg;
            this.arg=arg;
        }

        long getCommandId()
        {
            return commandId;
        }

        void setCommandId(long id)
        {
            commandId = id;
        }
    }
}
