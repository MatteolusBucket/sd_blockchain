package communications;

import utils.Commons;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

public class Node implements Serializable, Comparable<Node>
{

    private InetAddress hostname;
    private int port;

    static final long serialVersionUID = 1;


    public Node(byte[] hostname, int port) throws UnknownHostException
    {
        this.hostname =InetAddress.getByAddress(hostname);
        this.port = port;
    }

    public Node(String hostname, int port) throws UnknownHostException
    {
        this.hostname =InetAddress.getByName(hostname);
        this.port = port;
    }

    public Node(InetAddress hostname, int port)
    {
        this.hostname = hostname;
        this.port = port;
    }

    int getPort()
    {
        return port;
    }

    InetAddress getInetAddress()
    {
        return hostname;
    }

    public String toString()
    {
        return hostname+":"+port;
    }

    @Override
    public boolean equals(Object o)
    {
        //Commons.logger.info(""+this+" .equals("+o+")");
        if(o instanceof Node)
        {
            boolean r = compareTo((Node)o)==0;
            //Commons.logger.info("checking equality between "+((Node)o)+" and "+this+" result = "+r);
            return r;
        }
        else
        {
            //Commons.logger.info(""+this+" .equals("+o+")=false");
            return false;
        }
    }

    @Override
    public int compareTo(Node n)
    {
        int r;
        if(getInetAddress().equals(n.getInetAddress()) && n.getPort() == getPort())
            r = 0;
        else
        {
            byte[] addr1 = getInetAddress().getAddress();
            byte[] addr2 = n.getInetAddress().getAddress();
            long addr1Num = (((long)addr1[0])<<(24+32))+
                    (((long)addr1[1])<<(16+32))+
                    (((long)addr1[2])<<(8+32))+
                    (((long)addr1[3])<<(32))+
                    ((long)getPort());

            long addr2Num = (((long)addr2[0])<<(24+32))+
                    (((long)addr2[1])<<(16+32))+
                    (((long)addr2[2])<<(8+32))+
                    (((long)addr2[3])<<(32))+
                            ((long)n.getPort());
        /*    Commons.logger.info("addr1:"+addr1[0]+"."+addr1[1]+"."+addr1[2]+"."+addr1[3]+".");
            Commons.logger.info("addr2:"+addr2[0]+"."+addr2[1]+"."+addr2[2]+"."+addr2[3]+".");
            Commons.logger.info("port1:"+getPort());
            Commons.logger.info("port2:"+n.getPort());
            Commons.logger.info("comparing "+this+" and "+n+": "+String.format("0x%x",addr1Num)+" vs "+String.format("0x%x",addr1Num));
            Commons.logger.info("that is "+this+" and "+n+": "+addr1Num+" vs "+addr1Num);
         */
            if(addr1Num==addr2Num)
                r=0;
            else
                r = (addr1Num<addr2Num)?-1:1;
        }

        //Commons.logger.info("comparing "+((Node)n)+" and "+this+" result = "+r);

        return r;
    }

}
