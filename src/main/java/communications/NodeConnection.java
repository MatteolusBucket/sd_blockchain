package communications;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import utils.Commons;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class NodeConnection implements Comparable<NodeConnection>
{
    private Node node;
    private ConcurrentHashMap<Long, SuperCommand.CommandReturn> commandReturns;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Socket socket;
    private final Object connectionStatusNotifier = new Object();
    private volatile boolean connectionEstablished;
    private CommunicationsManager communicationsManager;
    private int commandsSentCount;
    private final Object commandReturnsNotifier = new Object();
    private volatile boolean sentDialogueEnd;
    private volatile boolean sentDialogueStart;
    private volatile boolean disconnecting;//when disconnecting we will refuse to send new commands

    private boolean didAlreadyConnectOnce = false;//It's only possible to use startConnection() once
    private int awaitedResponsesCount = 0;


    private long lastMessageTimeMillis;

    private static final long DIALOGUE_TIMEOUT_BY_SILENCE = 60_000;


    private boolean imSubscribedToThisNode = false;
    private boolean thisNodeIsSubscribedToMe = false;
    private Runnable onDisconnectingCallback = ()->{};
    private int connectionHolders;//number of "people" that are using this connection
    private int objectHolders;


    private static final Integer CONNECTION_ACCEPTED = 1;
    private static final Integer CONNECTION_REFUSED = 2;
    private static final int NEGOTIATION_TIMEOUT = 10_000;


    private AtomicBoolean isClientNegotiationRunning = new AtomicBoolean(false);
    private boolean objectDestroyed = false;

    private long estimatedRttNano = -1;


    private long lastCommandSentSuccessfully_timeMillis = -1;
    private int sendErrorStreakCounter = 0;

    private static final int READ_ERROR_COUNT_LIMIT = 20;
    private static final int WRITE_ERROR_COUNT_LIMIT = 20;
    private static final int WRITE_ERROR_LIMIT_MINIMUM_TIME = 60_000;

    NodeConnection(Node node)
    {
        this.node = node;
        commandReturns = new ConcurrentHashMap<>();
        commandsSentCount = 0;
        sentDialogueEnd = false;
        sentDialogueStart = false;
        disconnecting = false;
    }

    void startConnection(CommunicationsManager cm)
    {
        if(didAlreadyConnectOnce)
            throw new IllegalStateException("can't start the connection twice on the same object");
        didAlreadyConnectOnce=true;
        cm.startNewThread(() -> {
            NodeConnectionsManager.DialogueDuplex dd = new NodeConnectionsManager.DialogueDuplex(cm.getLocalPort(), this);
            dd.start(node, 20_000, 20_000, cm);
        });
    }

    @Deprecated
    synchronized void disconnect(CommunicationsManager cm)
    {
        askToDisconnectForced();
        disconnecting = true;
    }

    private synchronized void setDisconnecting()
    {
        onDisconnectingCallback.run();
        disconnecting = true;
    }


    void setOnDisconnectingCallback(Runnable onDisconnectingCallback)
    {
        this.onDisconnectingCallback = onDisconnectingCallback;
    }


    /**
     * Sets up the serverNode with the provided connection details
     *
     * @param out    The ObjectOutputStream that will be used
     * @param in     The ObjectInputStream that will be used
     * @param cm     The CommunicationsManager
     * @param socket The Socket that will be used
     * @return negative in case of error
     */
    int setupConnectionDetails(ObjectOutputStream out, ObjectInputStream in, CommunicationsManager cm, Socket socket)
    {
        Commons.logger.fine("setting up connection details");
        try
        {
            socket.setSoTimeout(500);
        }
        catch (SocketException e)
        {
            Commons.logger.warning("[" + node + "]: couldn't set connection timeout, aborting connection");
            return -1;
        }
        this.out = out;
        this.in = in;
        this.socket = socket;
        this.communicationsManager = cm;
        Commons.logger.fine("setted up connection details");
        return 0;
    }

    /**
     * Waits for the connection to be established. If the timeout expires you can check if the dialogue has already ended (i.e. it
     * was refused by the server) using {@link #isDisconnecting()}
     *
     * @param timeoutMillis
     * @return true if the connection was established, false otherwise
     * @throws InterruptedException
     */
    boolean waitConnection(long timeoutMillis) throws InterruptedException
    {
        long t0 = System.currentTimeMillis();
        synchronized (connectionStatusNotifier)
        {
            while (!connectionEstablished && !objectDestroyed && System.currentTimeMillis() - t0 < timeoutMillis)
            {
                if (timeoutMillis < 0)
                    connectionStatusNotifier.wait();
                else
                    connectionStatusNotifier.wait(timeoutMillis);
            }
        }
        return connectionEstablished;
    }

    /**
     * If this is true no more commands can be sent
     *
     * @return
     */
    public synchronized boolean isDisconnecting()
    {
        return disconnecting;
    }

    public boolean isConnectionEstablished()
    {
        return connectionEstablished;
    }

    private synchronized void updateLastMessageTimeMillis()
    {
        lastMessageTimeMillis = System.currentTimeMillis();
    }


    /**
     * The connection handler. This function sets up the connection and then handles incoming message
     */
    void listenConnection()
    {
        if (in == null)
            throw new IllegalStateException("The connection isn't set up correctly, did you call setupConnectionDetails()?");

        Object inMsg;
        boolean continueLooping = true;
        int readErrorCount = 0;


        /*
          If either the client or the server wants to end the dialogue they send a DIALOGUE_END message.
        The other serverNode will respond with another DIALOGUE_END message and close the connection. The first serverNode then, upon
        receiving the DIALOGUE_END response, will close the connection. So if you want to close the connection you just have
        to send a DIALOGUE_END message, this listener will do the rest.
         */


        connectionEstablished = true;
        synchronized (connectionStatusNotifier)
        {
            connectionStatusNotifier.notifyAll();//notify who is waiting for connectionEstablished
        }
        Commons.logger.fine("established connection");

        lastMessageTimeMillis = System.currentTimeMillis();
        //Commons.logger.info("continueLooping="+continueLooping);
        while (continueLooping && !(isDisconnecting() && getAwaitedResponsesCount() <= 0))
        {
            //Commons.logger.info("listening connection to "+this);
            inMsg = null;
            try
            {
                inMsg = in.readObject();
            }
            catch (SocketTimeoutException e)
            {
                //good
            }
            catch (IOException e)
            {
                Commons.logger.warning("[" + node + "] IOException, cannot read received fields. msg=" + e.getMessage() + " e.getClass()=" + e.getClass());
                readErrorCount++;
            }
            catch (ClassNotFoundException e)
            {
                Commons.logger.warning("[" + node + "] ClassNotFoundException, class of received serialized fields cannot be found. msg=" + e.getMessage());
                readErrorCount++;
            }

            if (inMsg != null)
            {
                updateLastMessageTimeMillis();
                if (inMsg instanceof SuperCommand)
                {
                    onCommandReceived((SuperCommand) inMsg);
                } else if (inMsg instanceof ConnectionSpecialCmd)
                {
                    switch ((ConnectionSpecialCmd) inMsg)
                    {
                        case DIALOGUE_END:
                            //continueLooping=false;
                            setDisconnecting();
                            //Commons.logger.info("received DIALOGUE_END, ending dialogue");
                            break;
                        default:
                            Commons.logger.warning("[" + node + "] received invalid ConnectionSpecialCmd " + inMsg);
                            readErrorCount++;
                            break;
                    }
                } else if (inMsg instanceof SuperCommand.CommandReturn)
                {
                    onResponseReceived((SuperCommand.CommandReturn) inMsg);
                } else
                {
                    Commons.logger.warning("[" + node + "] received invalid message.");
                    readErrorCount++;
                }
            }
            else
            {
                if ((!imSubscribedToThisNode && !thisNodeIsSubscribedToMe) && System.currentTimeMillis() - lastMessageTimeMillis > DIALOGUE_TIMEOUT_BY_SILENCE)
                {
                    askToDisconnectForced();
                }
            }

            if (socket.isClosed())
            {
                Commons.logger.warning("socket closed, ending dialogue");
                continueLooping = false;
            }
            if (Thread.interrupted())
            {
                Commons.logger.warning("thread interrupted, ending dialogue");
                continueLooping = false;
            }
            if (readErrorCount > READ_ERROR_COUNT_LIMIT)
            {
                Commons.logger.warning("[" + node + "] too many read errors, ending subscription");
                continueLooping = false;
            }
            if (sendErrorStreakCounter>WRITE_ERROR_COUNT_LIMIT && lastCommandSentSuccessfully_timeMillis-System.currentTimeMillis()>WRITE_ERROR_LIMIT_MINIMUM_TIME)
            {
                Commons.logger.warning("[" + node + "] too many send errors, ending subscription");
                continueLooping = false;
            }
            if(!continueLooping)//se ha deciso di terminare il loop di forza
                setDisconnecting();
        }


        if(imSubscribedToThisNode)
        {
            Commons.logger.fine("["+this+"]  connection closing down with outward subscription still on, unsubscribing");
            communicationsManager.getChosenNodesManager().onNodeDisconnected(getNode(),communicationsManager);
        }
        if(thisNodeIsSubscribedToMe)
        {
            Commons.logger.fine("["+this+"]  connection closing down with inward subscription still on, unsubscribing");
            communicationsManager.getIncomingSubscriptionsManager().unsubscribeFromUs(getNode(),communicationsManager);
        }

        communicationsManager.onNodeMaybeTurnedOff(getNode());
        /*
        if(!socket.isClosed() && !sentDialogueEnd)//se sono stato io a mandare DIALOGUE_END allora non devo mandarlo di nuovo
        {
            if(!askToDisconnectForced())
                Commons.logger.warning("[" + serverNode + "] failed to send DIALOGUE_END");

        }
        */
    }

    private enum ConnectionSpecialCmd
    {
        DIALOGUE_END,
        DIALOGUE_START,
        CONNECTION_ACCEPTED,
        CONNECTION_REFUSED
    }

    /**
     * @param scmd The command
     * @return true if the command was sent, false if an error occurred
     */
    private boolean sendSpecialCommand(ConnectionSpecialCmd scmd)
    {
        synchronized (out)
        {
            try
            {
                out.writeObject(scmd);
                updateLastMessageTimeMillis();
                return true;
            }
            catch (IOException e)
            {
                return false;
            }
        }
    }


    boolean sendDialogueStartForced()
    {
        sentDialogueStart = true;
        return sendSpecialCommand(ConnectionSpecialCmd.DIALOGUE_START);
    }

    boolean askToDisconnectForced()
    {
        sentDialogueEnd = true;
        setDisconnecting();
        return sendSpecialCommand(ConnectionSpecialCmd.DIALOGUE_END);
    }

    void askToDisconnect()
    {
        if (sentDialogueEnd || !connectionEstablished)
            throw new IllegalStateException("sentDialogueEnd=" + sentDialogueEnd + " disconnecting=" + isDisconnecting() + " connectionEstablished=" + connectionEstablished + " sentDialogueEnd=" + sentDialogueEnd);

        askToDisconnectForced();
    }

    /**
     * @param cmd The command
     */
    private void onCommandReceived(SuperCommand cmd)
    {
        synchronized (out)
        {
            try
            {
                cmd.handleAndRespond(out, communicationsManager, node.getInetAddress(), node.getPort());
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                Commons.logger.warning("[" + node + "] failed to send response for command " + cmd.getClass().getSimpleName());
            }
        }
    }

    private void onResponseReceived(SuperCommand.CommandReturn ret)
    {
        Commons.logger.fine("received command response with id = " + ret.getCommandId());
        commandReturns.put(ret.getCommandId(), ret);
        synchronized (commandReturnsNotifier)
        {
            commandReturnsNotifier.notifyAll();
        }
    }

    /**
     * @param cmd The command
     * @return true if the command was sent, false if an error occurred
     */
    private boolean sendCommand(SuperCommand cmd)
    {
        synchronized (out)
        {
            if (isDisconnecting() || !connectionEstablished)
            {
                Commons.logger.warning("Can't send new command, connection is closing down or isn't opening yet");
                sendErrorStreakCounter++;
                return false;
            }
            try
            {
                cmd.setCommandId(commandsSentCount++);
                Commons.logger.fine("sending command with id = " + cmd.getCommandId());
                cmd.send(out);
                synchronized (this)
                {
                    updateLastMessageTimeMillis();
                    awaitedResponsesCount++;
                }
                //Commons.logger.info("sent command with id = "+cmd.getCommandId());
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                Commons.logger.warning("Error sending command: " + e.getMessage() + " " + e.failReason + " " + e.getCause());
                sendErrorStreakCounter++;
                return false;
            }
            sendErrorStreakCounter=0;
            lastCommandSentSuccessfully_timeMillis = System.currentTimeMillis();
            return true;
        }
    }


    private int getAwaitedResponsesCount()
    {
        synchronized (this)
        {
            return awaitedResponsesCount;
        }
    }

    /**
     * @param sentCommand
     * @param timeoutMillis set it to a negative value to wait indefinetly
     * @return the obtained CommandRetuen, or null in case of a timeout
     */
    SuperCommand.CommandReturn waitForResponse(SuperCommand sentCommand, long timeoutMillis)
    {
        SuperCommand.CommandReturn ret = null;
        long t0 = System.currentTimeMillis();
        synchronized (commandReturnsNotifier)
        {
            do
            {
                ret = commandReturns.get(sentCommand.getCommandId());
                //Commons.logger.info("got ret = "+ret);
                if (ret == null)
                {
                    try
                    {
                        //Commons.logger.info("waiting...");
                        timeoutMillis -= System.currentTimeMillis() - t0;
                        if (timeoutMillis < 0)
                            commandReturnsNotifier.wait();
                        else
                            commandReturnsNotifier.wait(timeoutMillis);
                        //Commons.logger.info("command return notification received");
                    }
                    catch (InterruptedException e)
                    {
                        timeoutMillis = Math.min(timeoutMillis, 5_000);//se ti dicono di interromperti setta un timeout di al masismo 5 secondi
                    }
                }
            } while (ret == null && (timeoutMillis < 0 || System.currentTimeMillis() - t0 < timeoutMillis));
        }
        synchronized (this)
        {
            awaitedResponsesCount--;
        }
        //Commons.logger.info("returning ret = "+ret);
        return ret;
    }

    public int compareTo(NodeConnection cnode)
    {
        return node.compareTo(cnode.node);
    }


    Node getNode()
    {
        return node;
    }


    boolean sendBlock(IBlock b, int timeToLive)
    {
        Commons.logger.fine("sending block " + b.getID() + " to " + this);
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
            return false;
        CommandSpreadBlock cmd = new CommandSpreadBlock(b, timeToLive);
        boolean wasSent = sendCommand(cmd);
        //Commons.logger.info("wasSent="+wasSent);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
            ret = waitForResponse(cmd, 30000);
        else
            return false;
        //Commons.logger.info("ret="+ret);
        //Commons.logger.info("ret.isOk()="+ret.isOk());
        //Commons.logger.info("ret.getErrorMsg()="+ret.getErrorMsg());

        if (ret != null && ret.isOk())
        {
            return true;
        } else
        {
            Commons.logger.warning("error sending block " + b.getID() + " to " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
            return false;
        }
    }

    boolean sendTransaction(ITransaction t, int timeToLive)
    {
        Commons.logger.fine("sending transaction " + t.getID() + " to " + this);
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
            return false;
        CommandSpreadTransaction cmd = new CommandSpreadTransaction(t, timeToLive);
        boolean wasSent = sendCommand(cmd);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
            ret = waitForResponse(cmd, 30000);
        else
            return false;

        if (ret != null && ret.isOk())
        {
            return true;
        } else
        {
            Commons.logger.warning("error sending block " + t.getID() + " to " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
            return false;
        }
    }


    /**
     * Asks the nod for the blocks with the specified id
     *
     * @param blockId the block id
     * @return a list containing the obtained blocks, or null if no block with the specified id was found
     */
    List<IBlock> askBlock(int blockId)
    {
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
            return null;
        CommandAskBlock cmd = new CommandAskBlock(blockId);
        boolean wasSent = sendCommand(cmd);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
            ret = waitForResponse(cmd, 30000);
        else
            return null;

        if (ret == null || !ret.isOk())
        {
            Commons.logger.warning("error asking block " + blockId + " to " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
            return null;
        }


        return (List<IBlock>) cmd.getResponseArgsChecking(ret);
    }




    /**
     * Asks the node for the his neighbors
     *
     * @return a list containing the nodes
     */
    List<RemoteNode> askForNeighbors()
    {
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
            return null;
        CommandAskNeighbours cmd = new CommandAskNeighbours();
        boolean wasSent = sendCommand(cmd);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
            ret = waitForResponse(cmd, 30000);
        else
            return null;

        if (ret == null || !ret.isOk())
        {
            Commons.logger.warning("error asking neighbors to " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
            return null;
        }


        return (List<RemoteNode>) cmd.getResponseArgsChecking(ret);
    }


    /**
     * Asks the serverNode for the highest id from the blocks he has
     *
     * @return the id
     */
    int askLastKnownBlockId()
    {
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
            return -1;
        CommandAskLastKnownBlockID cmd = new CommandAskLastKnownBlockID();
        boolean wasSent = sendCommand(cmd);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
            ret = waitForResponse(cmd, 30000);
        else
            return -2;


        if (ret == null || !ret.isOk())
        {
            Commons.logger.warning("error asking last block id to " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
            return -3;
        }

        return (Integer) cmd.getResponseArgsChecking(ret);
    }




    /**
     * Asks the serverNode for the highest id from the blocks he has
     *
     * @return the id
     */
    long pingNano()
    {
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
            return -1;
        CommandPing cmd = new CommandPing();
        long t0 = System.nanoTime();
        boolean wasSent = sendCommand(cmd);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
            ret = waitForResponse(cmd, 30000);
        else
            return -2;
        long t1 = System.nanoTime();


        if (ret == null || !ret.isOk())
        {
            Commons.logger.warning("error asking last block id to " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
            return -3;
        }

        return t1-t0;
    }

    /**
     * retuens the round trip time estimated by {@link #estimateRoundTripTime()}
     * @return
     */
    long getEstimatedRttNano()
    {
        return estimatedRttNano;
    }

    /**
     * estimates the round trip time of the connection
     * @return
     */
    NodeConnection estimateRoundTripTime()
    {
        long pingSum=0;
        int validSamples=0;
        for(int i=0;i<5 && validSamples<1;i++)
        {
            long ping = pingNano();
            //Commons.logger.info("pingNanoCompleted");
            if(ping>0)
            {
                pingSum+=ping;
                validSamples++;
            }
        }
        if(validSamples>0)
            estimatedRttNano = pingSum/validSamples;
        else
            estimatedRttNano = -1;
        Commons.logger.fine("estimated ping for "+this+" = "+estimatedRttNano);
        return this;
    }




    /**
     * Attempts to subscribe to the node on the other side of the connection.
     * This could fail for a variety of reasons:
     * - because this connection is already closing down (i.e. if isDisconnecting()==true)
     * - because the connection hasn't been established yet
     * - because we already sent the dialogue end command (and so we can't submit new commands)
     * - because the other node rejects  our request (e.g. it already has too many subscriptions)
     * - if I/O problems happen
     * @return true if the subscription was accepted and everything went smooth
     */
    boolean subscribeToNode()
    {
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
            return false;
        CommandSubscribe cmd = new CommandSubscribe();
        boolean wasSent = sendCommand(cmd);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
        {
            ret = waitForResponse(cmd, 30000);
        }
        else
        {
            sendUnsubscriptionRequest();
            return false;
        }


        if (ret == null || !ret.isOk())
        {
            Commons.logger.warning("error subscribing to " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
            return false;
        }

        imSubscribedToThisNode=true;
        return (Boolean) cmd.getResponseArgsChecking(ret);
    }

    /**
     * Unsubscribe from the node on the opposite side. This undoes a subscription established by {@link #subscribeToNode()}
     * If there is not a subscription from the other node to us then, after this method the connection is free to go down
     */
    void sendUnsubscriptionRequest()
    {
        if (sentDialogueEnd || isDisconnecting() || !connectionEstablished)
        {
            Commons.logger.warning("called with setDialogueEnd="+sentDialogueEnd+" isDisconnecting()="+isDisconnecting()+" connectionEstablished="+connectionEstablished);
            imSubscribedToThisNode=false;
            return;
        }
        CommandUnsubscribe cmd = new CommandUnsubscribe();
        boolean wasSent = sendCommand(cmd);
        SuperCommand.CommandReturn ret = null;
        if (wasSent)
        {
            ret = waitForResponse(cmd, 30000);
            if (ret == null || !ret.isOk())
                Commons.logger.warning("error unsubscribing from " + this + ": " + (ret == null ? "response timed out" : ret.getErrorMsg()));
        }
        imSubscribedToThisNode=false;
    }


    /**
     * Register a subscription started from the other side of the connection.
     * When there are open subscriptions to the connection the connection stays on
     * @return if the subscription can be accepted byt the connection (it can't if the connection is already closing down)
     */
    boolean registerIncomingSubscription()
    {
        if(isDisconnecting())//se stiamo già chiudendo la connessione non accettiamo nuove aperture di sottoscrizioni
            return false;
        thisNodeIsSubscribedToMe =true;
        return true;
    }

    /**
     * Unregisters a subscription that was set up by {@link #registerIncomingSubscription()}
     */
    void unregisterIncomingSubscription()
    {
        thisNodeIsSubscribedToMe =false;
    }

    @Override
    public String toString()
    {
        return getNode().toString();
    }

    /**
     * increases the connectionHolders counter
     */
    public void holdConnection()
    {
        connectionHolders++;
    }

    /**
     * decreases the connectionHolders counter
     */
    public void freeConnection()
    {
        connectionHolders--;
    }

    /**
     * returns the connectionHolders count
     */
    public int connectionHoldersCount()
    {
        return connectionHolders;
    }



    /**
     * increases the connectionHolders counter
     */
    public void holdObject()
    {
        objectHolders++;
    }

    /**
     * decreases the connectionHolders counter
     */
    public void freeObject()
    {
        objectHolders--;
    }

    /**
     * returns the connectionHolders count
     */
    public int objectHoldersCount()
    {
        return objectHolders;
    }



















    /**
     * Connection negotiation on the client side (called on the client side)
     * This method does not proceed to register the connection on the NodeConnectionsManager, you should do that yourself.
     * The idea is that you do that before calling this  method (so to call it only if you really do startConnection)
     *
     * @return true if the connection gets accepted by the server
     */
    boolean clientNegotiation(ObjectInputStream in, ObjectOutputStream out,  Socket socket,CommunicationsManager cm)
    {
        isClientNegotiationRunning.set(true);
        Commons.logger.fine("starting client negotiation");
        Object inMsg;
        int prevTimeout = 500;
        try
        {
            prevTimeout = socket.getSoTimeout();
            socket.setSoTimeout(NEGOTIATION_TIMEOUT);
        }
        catch (SocketException e)
        {
            Commons.logger.warning("[" + this + "]: couldn't set connection timeout, aborting connection");

            isClientNegotiationRunning.set(false);
            return false;
        }

        checkSelfTalk(out,in,getNode(),null,null);

        try
        {
            inMsg = in.readObject();//aspetta il primo messaggio dovrebbe essere DIALOGUE_START oppure DIALOGUE_END
        }
        catch (IOException e)
        {
            Commons.logger.warning("[" + this + "] IOException, cannot read connection established message. msg=" + e.getMessage() + " e.getClass()=" + e.getClass());

            isClientNegotiationRunning.set(false);
            return false;
        }
        catch (ClassNotFoundException e)
        {
            Commons.logger.warning("[" + this + "] ClassNotFoundException, class of received serialized fields cannot be found. msg=" + e.getMessage());

            isClientNegotiationRunning.set(false);
            return false;
        }


        try
        {
            socket.setSoTimeout(prevTimeout);
        }
        catch (SocketException e)
        {
            Commons.logger.warning("[" + this + "]: couldn't set connection timeout, aborting connection");

            isClientNegotiationRunning.set(false);
            return false;
        }

        if(!(inMsg instanceof Integer))
        {
            Commons.logger.warning("invalid negotiation response "+inMsg.getClass().getSimpleName()+"  "+inMsg);
            isClientNegotiationRunning.set(false);
            return false;
        }


        if (((Integer)inMsg).intValue() == CONNECTION_ACCEPTED.intValue())
        {
            isClientNegotiationRunning.set(false);
            return true;
        }
        else if (((Integer)inMsg).intValue() == CONNECTION_REFUSED.intValue())
        {
            isClientNegotiationRunning.set(false);
            return false;
        }
        else
        {
            Commons.logger.warning("invalid negotiation response "+inMsg.getClass().getSimpleName()+"  "+inMsg);
            isClientNegotiationRunning.set(false);
            return false;
        }
    }


    /**
     * Connection negotiation on the server side (called on the server side)
     * If the connection is accepted this method also proceeds to register the node by the connectedNodesManager
     *
     * @return 0 if the negotiation is successful, a negative value in case of error. Values between -1 an -9 represent the registration rsult
     *         from {@link NodeConnectionsManager#registerIncomingConnection(NodeConnection)} values below -10 represent IO errors
     */
    int serverNegotiation(ObjectOutputStream out, Socket socket, CommunicationsManager cm)
    {
        Commons.logger.fine("starting server negotiation");
        int prevTimeout = 500;
        try
        {
            prevTimeout = socket.getSoTimeout();
            socket.setSoTimeout(NEGOTIATION_TIMEOUT);
        }
        catch (SocketException e)
        {
            Commons.logger.warning("[" + this + "]: couldn't set connection timeout, aborting connection");
            return -11;
        }

        int[][] ourRandVal = new int[8][0];
        int[][] theirRandVal = new int[8][0];
        boolean imTalkingToMyself = checkSelfTalk(out,in,this.getNode(),ourRandVal,theirRandVal);
        int registrationResult = -100;
        if(!imTalkingToMyself)
        {
            registrationResult = cm.getNodeConnectionsManager().registerIncomingConnection(this);

            /*
            //Excluded as too convoluted, I'll just use a random backoff time

            //if the registration is successful everything is good. As we decide the server negotiation is ok
            //the other side will agree, as the other side is just waiting for our response to go on.
            //Other connections that may be attempted by the other side will be refused by the registration
            //and the client wont be running as it hasn't started until now (because wasAccepted==true) and
            //can't start as registerOutgoing won't allow it.
            if(!wasAccepted && isClientNegotiationRunning.get())
            {
                //if the registration isn't successful:
                //  if isClientNegotiationRunning==false then there is already a connection that is started or will be soon
                //  if isClientNegotiationRunning==true then we have to decide if this connection will live on or the one
                //      in the clientNegotiation will. That is: the connection started by us (that is in the
                //      clientNegotiation) or the connection started by the other side (this one, in the serverNegotiation).
                //      We decide to keep the serverNegotiation one if ourRandVal is greater that theirRandVal.
                //      (Which are two random values that are shared (and switched) between the two sides)
                //  So: if the other side is in our same situation it will take the opposite decision, if it isn't it will
                //  decide based on our decision.

                //In reality we make the comparison just between the less significant bytes of the two values, it should be the same
                if(ourRandVal[0][0]>theirRandVal[0][0])
                    wasAccepted=true;
            }
            */
        }
        //wasAccepted == true <=> !imTalkingToMySelf && acceptedByRegister

        Integer response = null;
        if (registrationResult==0)
            response = CONNECTION_ACCEPTED;
        else
            response = CONNECTION_REFUSED;


        try
        {
            out.writeObject(response);
        }
        catch (IOException e)
        {
            Commons.logger.warning("[" + this + "] IOException, cannot send server negotiation response. msg=" + e.getMessage() + " e.getClass()=" + e.getClass());
            return -12;
        }

        try
        {
            socket.setSoTimeout(prevTimeout);
        }
        catch (SocketException e)
        {
            Commons.logger.warning("[" + this + "]: couldn't set connection timeout, aborting connection");
            return -13;
        }

        return registrationResult;
    }


    /**
     * Checks if the two streams are talking to themselves by exhanging a random number
     * @param out
     * @param in
     * @return
     */
    private boolean checkSelfTalk(ObjectOutputStream out, ObjectInputStream in, Node otherNode, int[][] ourRandVal, int[][] theirRandVal)
    {
        Commons.logger.fine("checking self talk");
        boolean imTalkingToMyself = true;

        int[] randVal = new int[8];//direi che 256 bit possono bastare
        if(ourRandVal!=null && ourRandVal[0].length>0)
            ourRandVal[0]=randVal;
        Random rand = new Random();
        for (int i = 0; i < randVal.length; i++)
            randVal[i] = rand.nextInt();

        boolean sent = false;
        try
        {
            out.writeObject(randVal);
            sent = true;
        }
        catch (IOException e)
        {
            Commons.logger.warning("Error establishing connection, sending randVal, IOException msg=" + e.getMessage() + " e.getClass()=" + e.getClass());
        }
        if (sent)
        {
            int[] otherRandVal = null;
            boolean received = false;
            try
            {
                Object otherRandValObj = in.readObject();
                if (otherRandValObj instanceof int[] && ((int[]) otherRandValObj).length == randVal.length)
                {
                    otherRandVal = (int[]) otherRandValObj;
                    if(theirRandVal!=null && theirRandVal[0].length>0)
                        theirRandVal[0]=otherRandVal;
                    received = true;
                } else
                {
                    Commons.logger.warning("received invalid object as randVal, class = " + otherRandValObj.getClass().getName());
                }
            }
            catch (IOException e)
            {
                Commons.logger.warning("Error establishing connection, receiving randVal, IOException msg=" + e.getMessage() + " e.getClass()=" + e.getClass());
            }
            catch (ClassNotFoundException e)
            {
                Commons.logger.warning("[" + otherNode + "] ClassNotFoundException, class of received serialized fields cannot be found. msg=" + e.getMessage());
            }
            if (received)
            {
                boolean theyMatch = true;
                for (int i = 0; i < randVal.length; i++)
                {
                    theyMatch &= randVal[i] == otherRandVal[i];
                }
                if (!theyMatch)
                {
                    imTalkingToMyself = false;
                }
            }
        }

        Commons.logger.fine("checkSelfTalk completed, imTalkingToMyself="+imTalkingToMyself);
        return imTalkingToMyself;
    }


    void abort()
    {
        synchronized (connectionStatusNotifier)
        {
            objectDestroyed = true;
            connectionStatusNotifier.notifyAll();
        }
    }

/*
    static NodeConnectionFactory FACTORY = new NodeConnectionFactory();

    static class NodeConnectionFactory
    {
        private final ConcurrentSkipListMap<Node, NodeConnection> allConnections = new ConcurrentSkipListMap<>();

        synchronized NodeConnection getConnectionObject(Node node)
        {
            if(allConnections.containsKey(node))
            {
                NodeConnection nodeConn = allConnections.get(node);
                nodeConn.holdObject();
                return nodeConn;
            }

            NodeConnection newConnection = new NodeConnection(node);
            allConnections.put(node,newConnection);
            newConnection.holdObject();
            return newConnection;
        }

        synchronized void giveBackConnectionObject(NodeConnection nodeConn)
        {
            nodeConn.freeObject();
            if(nodeConn.objectHoldersCount()<=0)
            {
                nodeConn.setDestroyed();
                allConnections.remove(nodeConn.getNode());
            }
        }

    }*/
}
