package communications;

import utils.Commons;

/**
 * Command to ask a Node to subscribe to him
 * The {@link communications.SuperCommand.CommandReturn#arg} in the Response contains a Boolean indicating if the subscription has been accepted
 */
public class CommandSubscribe  extends SuperCommand
{
    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        Commons.logger.info("handling CommandSubscribe");
        return new CommandReturn(cm.getIncomingSubscriptionsManager().subscribeToUs(sender,cm));
    }

    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        Object o =  res.getArg();
        if((o instanceof Boolean))
            return o;
        else
        {
            Commons.logger.warning("received invalid response");
            return null;
        }
    }
}
