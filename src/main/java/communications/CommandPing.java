package communications;

import utils.Commons;

/**
 * Command used to Ping a Node
 * The {@link communications.SuperCommand.CommandReturn#arg} in the Response contains just a string saying "pingNano"
 */
class CommandPing extends SuperCommand
{

    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        return new CommandReturn("pingNano");
    }

    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        Object o =  res.getArg();
        if(o instanceof String)
            return o;
        else
        {
            Commons.logger.warning("received invalid response");
            return null;
        }
    }
}
