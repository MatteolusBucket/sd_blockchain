package communications;

import org.apache.commons.lang3.exception.ExceptionUtils;
import utils.Commons;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;


/**
 * This class handles the incoming connections. To use it just create an fields and call {@link #start()}, it will
 * create a thread that waits incoming connections and spawns new threads to handleCommand them. When you wish to shut it down
 * just call {@link #stop()}.
 */
class IncomingConnectionsHandler
{
    private Looper looperThread;
    private boolean continueLooping;
    private CommunicationsManager communicationsManager;
    boolean stopped = false;

    IncomingConnectionsHandler(CommunicationsManager cm)
    {
        communicationsManager = cm;
    }
    /**
     * Start the incoming connections handler Thread. This method can be invoked only one time per fields.
     * If already started it does nothing
     */
    public void start()
    {
        if(isContinueLooping())
            return;
        setContinueLooping(true);
        looperThread = new Looper();
        looperThread.start();
    }

    /**
     * Says if the handler thread is running (i.e. {@link #start()} has been called and {@link #stop()} hasn't)
     * @return true if it is running
     */
    public boolean isRunning()
    {
        return isContinueLooping();
    }

    /**
     * Stops the incoming connections handler Thread.
     * If not started it does nothing.
     */
    void stop()
    {
        Commons.logger.info("IncomingConnectionsHandler stopping");
        stopped=true;
        if(!isContinueLooping())
            return;
        setContinueLooping(false);
        boolean wasInterrupted = false;
        boolean didJoin = false;
        while(!didJoin)
        {
            try
            {
                looperThread.join();
               // System.out.println("joined");
                didJoin = true;
            }
            catch (InterruptedException e)
            {
                wasInterrupted = true;
            }
        }
        if(wasInterrupted)
            Thread.currentThread().interrupt();
    }

    private synchronized boolean isContinueLooping()
    {
        return continueLooping;
    }

    private synchronized void setContinueLooping(boolean continueLooping)
    {
        this.continueLooping = continueLooping;
    }

    private class Looper extends Thread
    {

        @Override
        public void run()
        {
            ServerSocket server;
            try
            {
                server = new ServerSocket(communicationsManager.getLocalPort());
            }
            catch (IOException e)
            {
                Commons.logger.severe("IncomingConnectionsHandler.start: unable to create socket for port "+communicationsManager.getLocalPort()+", returning\n"+ExceptionUtils.getStackTrace(e));
                return;
            }
            try
            {
                server.setReuseAddress(true);
            }
            catch(SocketException e)
            {
                Commons.logger.warning("Couldn't set SO_REUSEADDRESS to true\n"+ExceptionUtils.getStackTrace(e));
            }
            int failCount=0;
            while (true)
            {
                if(!isContinueLooping())
                    break;
                try
                {
                    server.setSoTimeout(500);
                    try
                    {
                        Socket newConnection = server.accept();
                        Commons.logger.fine("received connection from "+newConnection.getInetAddress());
                        if(stopped)
                        {
                            newConnection.close();
                            break;
                        }
                        Commons.logger.fine("handling connection from "+newConnection.getInetAddress());
                        RequestHandler handler = new RequestHandler(newConnection);
                        failCount=0;
                        try
                        {
                            communicationsManager.startNewThread(handler);
                        }
                        catch(IllegalStateException e)
                        {
                            //If this happens it means that communication manager was stopped, so isContinueLooping will be false
                            //So we can just go on
                            newConnection.close();
                        }
                    }
                    catch(SocketTimeoutException e)
                    {
                        //do nothing
                    }
                }
                catch (IOException e)
                {
                    if(failCount>50)
                    {
                        Commons.logger.warning("IncomingConnectionsHandler.start: IOException (msg="+e.getMessage()+"): accept failed on socket for port "+communicationsManager.getLocalPort()+" 10 times, terminating");
                        break;
                    }
                    else
                    {
                        Commons.logger.warning("IncomingConnectionsHandler.start: IOException (msg="+e.getMessage()+"): accept failed on socket for port " + communicationsManager.getLocalPort()+ ", retrying");
                        failCount++;
                        try
                        {
                            Thread.sleep(500);
                        }
                        catch (InterruptedException ie)
                        {
                            Thread.currentThread().interrupt();
                            System.out.println("IncomingConnectionsHandler.start: InterruptedException, terminating");
                            break;
                        }
                    }
                }
            }
            try
            {
                server.close();
            }
            catch(IOException e)
            {
                Commons.logger.info("IncomingConnectionsHandler.start: IOException (msg="+e.getMessage()+"): cannot close socket, sorry");
            }
        }
    }

    protected class RequestHandler extends Thread
    {
        Socket socket;
        RequestHandler(Socket socket)
        {
            this.socket=socket;
        }

        private int handle(ObjectInputStream in, ObjectOutputStream out)
        {

            Object inMsg;
            try
            {
                inMsg = in.readObject();
            }
            catch (IOException e)
            {
                Commons.logger.warning("["+socket.getInetAddress()+"] IOException, cannot read received fields, dropping connection. msg="+e.getMessage());
                return -1;
            }
            catch(ClassNotFoundException e)
            {
                Commons.logger.warning("["+socket.getInetAddress()+"] ClassNotFoundException, class of received serialized fields cannot be found, dropping connection. msg="+e.getMessage());
                e.printStackTrace();
                return -2;
            }

            Commons.logger.fine("received a dialogue "+(inMsg.getClass().getSimpleName()));

            if(inMsg instanceof Dialogue)
            {
                //He's sending us a valid command, so he's a valid serverNode
                RemoteNode senderNode = new RemoteNode(socket.getInetAddress(),((Dialogue) inMsg).getSenderPort());
                Commons.logger.fine("Incoming connection from "+senderNode + " (msg type = "+inMsg.getClass().getSimpleName()+" hash="+inMsg.hashCode()+")");
                /*   if(communicationsManager.getChosenNodesManager().wantsMoreNodes() && isNew)
                    communicationsManager.updateChosenNodesAsync(2000);*/
                int handleResult;
                try
                {
                    handleResult = ((Dialogue) inMsg).handle(out, in, communicationsManager, socket.getInetAddress(),socket);
                }
                catch (CommunicationErrors.CommunicationException e)
                {
                    if(e.getCause()==null)
                        Commons.logger.warning("["+socket.getInetAddress()+"] Communication error handling dialogue. "+e.getMessage());
                    else
                        Commons.logger.warning("["+socket.getInetAddress()+"] Communication error handling dialogue. "+e.getMessage()+"\t "+e.getCause()+"\n" + ExceptionUtils.getStackTrace(e));
                    return -4;
                }
                Commons.logger.fine("handled incoming connection from "+senderNode + " (msg type = "+inMsg.getClass().getSimpleName()+" hash="+inMsg.hashCode()+")");
                boolean isNew = communicationsManager.addKnownNode(senderNode);
                Commons.logger.fine("added node ("+isNew+") "+(inMsg).getClass().getSimpleName()+" hash="+inMsg.hashCode());
                communicationsManager.getKnownNode(senderNode.getNode().getInetAddress(), senderNode.getNode().getPort()).noteIncomingRequestReceived();
                return handleResult;

            }
            else
            {
                Commons.logger.warning("["+socket.getInetAddress()+"] Invalid message, class="+inMsg.getClass().getSimpleName()+", dropping connection");
                return -5;
            }
        }
        @Override
        public void run()
        {

            boolean cancel=false;
            try
            {
                socket.setSoTimeout(10_000);
            }
            catch (SocketException e)
            {
                Commons.logger.warning("handler for ("+socket.getInetAddress()+"): couldn't set connection timeout, dropping connection\n"+ExceptionUtils.getStackTrace(e));
                cancel=true;
            }

            if(!cancel)
            {
                ObjectInputStream in;
                ObjectOutputStream out;
                try
                {
                    in = new ObjectInputStream(socket.getInputStream());
                    out = new ObjectOutputStream(socket.getOutputStream());
                }
                catch (IOException e)
                {
                    Commons.logger.warning("IncomingConnectionsHandler.RequestHandler: [" + socket.getInetAddress() + "]: IOException, cannot create input or output streams, dropping connection");
                    e.printStackTrace();
                    return;
                }

                int r = handle(in, out);
                if (r < 0)
                    Commons.logger.warning("IncomingConnectionsHandler.RequestHandler: [" + socket.getInetAddress() + "]: handleCommand(...) returned " + r);

                try
                {
                    out.close();
                }
                catch (IOException e)
                {
                    Commons.logger.warning("IncomingConnectionsHandler.RequestHandler: [" + socket.getInetAddress() + "] cannot close output stream, sorry");
                }

                try
                {
                    in.close();
                }
                catch (IOException e)
                {
                    Commons.logger.warning("IncomingConnectionsHandler.RequestHandler: [" + socket.getInetAddress() + "] cannot close input stream, sorry");
                }
            }

            try
            {
                socket.close();
            }
            catch(IOException e)
            {
                Commons.logger.warning("IncomingConnectionsHandler.RequestHandler: ["+socket.getInetAddress()+"] cannot close socket, sorry");
            }
        }
    }
}
