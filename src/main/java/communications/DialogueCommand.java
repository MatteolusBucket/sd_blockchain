package communications;

import utils.Commons;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

class DialogueCommand extends Dialogue
{
    transient private SuperCommand command;

    DialogueCommand(int senderPort, SuperCommand command)
    {
        super(senderPort);
        this.command=command;
    }

    @Override
    protected SuperCommand.CommandReturn run(ObjectOutputStream out, ObjectInputStream in, Node server, CommunicationsManager cm, Socket socket) throws CommunicationErrors.CommunicationException
    {
        Commons.logger.fine("running DialogueCommand to "+server+" with command "+command.getClass().getSimpleName());
        command.send(out);
        return command.waitResponse(in);
    }

    @Override
    protected int handle(ObjectOutputStream out, ObjectInputStream in, CommunicationsManager cm, InetAddress senderAddress, Socket socket) throws CommunicationErrors.CommunicationException
    {
        Commons.logger.fine("handling DialogueCommand with hash="+hashCode()+" from "+senderAddress);
        try
        {
            Object obj = in.readObject();
            if(obj instanceof SuperCommand)
                command = (SuperCommand)obj;
            else
                throw new CommunicationErrors.CommunicationException("Received invalid object as command (received a "+obj.getClass().getName()+")",null,CommunicationErrors.FailReason.RECEIVED_INVALID_OBJECT);
        }
        catch (IOException | ClassNotFoundException e)
        {
            throw new CommunicationErrors.CommunicationException("Couldn't receive command ",e,CommunicationErrors.FailReason.CANNOT_RECEIVE_COMMAND);
        }
        Commons.logger.fine("DialogueCommand with hash="+hashCode()+"received a "+(command.getClass().getSimpleName()));
        command.handleAndRespond(out,cm,senderAddress,getSenderPort());
        return 0;
    }
}
