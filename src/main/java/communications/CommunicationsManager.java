package communications;

import blockchain.blocks.IBlock;
import incoming.IBlocksTransactionsExchanger;
import blockchain.transactions.ITransaction;

import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.apache.commons.lang3.exception.ExceptionUtils;
import utils.Commons;

public class CommunicationsManager
{
    public static final int DEFAULT_PORT = 54618;

    /*
        - The knownNodes set is a large set containing all the nodes we know of, these are not guaranteed to be reachable.
        - reachableNodes is a subset of knownNodes, these nodes have been recently checked to be reachable.
        - chosenNodes is a subset on reachableNodes, these nodes are the ones we chose to ask blocks and transactions to
          and to receive blocks and transactions from us.

        Beside these there are the nodes that chose us as one of their chosenNodes, we will call these clientNodes.
        So we will have to forward blocks and transactions both to chosenNodes and to clientNodes
     */


    /**
     * The nodes we know to exist (or existed)
     */
    private final ConcurrentSkipListMap<Node, RemoteNode> knownNodes;
    /**
     * Nodes we are able to startDialogueDuplexSubscription to
     */
    private final ConcurrentSkipListMap<Node, RemoteNode> reachableNodes;

    private IncomingConnectionsHandler inConnectionsHandler;
    private IBlocksTransactionsExchanger blockOrTransactionReceiver;
    private int localPort;
    private final Object startedGuard = new Object();//used for synchronization of started
    private boolean started;
    private Set<Thread> runningThreads;
    private static final int STOP_THREAD_TIMEOUT = 10000;


    private double connectivityQuality = -1;


    //private SubscribedNodesManager subscribedNodesManager;
    //private SubscriptionsManager subscriptionsManager;

    private final IncomingSubscriptionsManager incomingSubscriptionsManager;
    private final ChosenNodesManager chosenNodesManager;


    private final NodeConnectionsManager nodeConnectionsManager;
    private boolean alreadyStartedOnce=false;
    private static final long FORGETTING_TIME = 24*60*1000;//one day

    private static class RemoteNodeDispenser
    {
        NavigableMap<Node,RemoteNode> nodes;
        Node lastKeyGot;
        boolean finished=false;
        RemoteNodeDispenser(NavigableMap<Node,RemoteNode> nodes)
        {
            this.nodes=nodes;
        }

        public synchronized RemoteNode getNode()
        {
            if(finished)
                return null;
            Node key;
            if(lastKeyGot ==null)
            {
                if(nodes.size()>0)
                    key=nodes.firstKey();
                else
                    key=null;
            }
            else
            {
                key=nodes.higherKey(lastKeyGot);
            }
            lastKeyGot=key;
            if(lastKeyGot==null)
            {
                finished=true;
                return null;
            }
            return nodes.get(key);
        }
    }

    /**
     * Constructor,
     * @param initialHostsIPs list of strings indicating the remote nodes to use as the first known nodes of the network, each string has to be of the format hostname[:port]
     * @param incomingManager class to send the received blocks and transactions to
     * @param localPort the port which will be used to listen to incoming connections, if less than one the default port {@link #DEFAULT_PORT} will be used
     */
    public CommunicationsManager(String[] initialHostsIPs, IBlocksTransactionsExchanger incomingManager, int localPort)
    {
        this(stringArrayToRemoteNodeList(initialHostsIPs),incomingManager, localPort);
    }

    /**
     * Same as {@link #CommunicationsManager(String[], IBlocksTransactionsExchanger, int)} but with the first argument translated to a list of {@link RemoteNode}
     * @param startingKnownRemoteNodes list of the remote nodes to use as the first known nodes of the network
     * @param incomingManager class to send the received blocks and transactions to
     * @param localPort the port which will be used to listen to incoming connections, if less than one the default port {@link #DEFAULT_PORT} will be used
     */
    public CommunicationsManager(List<RemoteNode> startingKnownRemoteNodes, IBlocksTransactionsExchanger incomingManager, int localPort)
    {
        runningThreads = ConcurrentHashMap.newKeySet();
        knownNodes = new ConcurrentSkipListMap<>();
        reachableNodes = new ConcurrentSkipListMap<>();

        removeOurAddresses(startingKnownRemoteNodes);
        for(RemoteNode rn : startingKnownRemoteNodes)
            knownNodes.put(rn.getNode(),rn);

//        System.out.println("knownNodes.size()="+knownNodes.size());
        inConnectionsHandler = new IncomingConnectionsHandler(this);
        this.blockOrTransactionReceiver = incomingManager;
        if(localPort>0)
            this.localPort = localPort;
        else
            this.localPort = DEFAULT_PORT;

        nodeConnectionsManager = new NodeConnectionsManager();
        chosenNodesManager = new ChosenNodesManager();
        incomingSubscriptionsManager = new IncomingSubscriptionsManager();
    }

    /**
     * If {@link #start()}  has been called and {@link #stop()} hasn't
     * @return if is started
     */
    public boolean isStarted()
    {
        return started;
    }

    /**
     * Starts the communicationsManager, this can't be started twice
     */
    public void start()
    {
        started=true;
        if(alreadyStartedOnce)
            throw new IllegalStateException("You can't start the same communicationsManager twice");
        alreadyStartedOnce=true;
        /*
        if(!setupUpnp())
            Commons.logger.warning("UPNP setup failed");
            */
//        Commons.logger.info("starting communication manager");
        inConnectionsHandler.start();
//        Commons.logger.info("started inConnectionsHandler");
        Commons.logger.info("initial known nodes: "+getKnownNodes());
        //updateChosenNodesAsync(0);
        startNewThread(this::initialize);

    }


    /**
     * Stops the CommunicationsManager, it can't be started again
     */
    public void stop()
    {
        Commons.logger.info("CommunicationsManager stopping (waiting for "+runningThreads.size()+" threads)...");
        inConnectionsHandler.stop();
        chosenNodesManager.disconnectAll(this);
        incomingSubscriptionsManager.disconnectAll(this);
        nodeConnectionsManager.stop(this);
        /*
        shutdownUpnp();
        */
        synchronized (startedGuard)//to prevent a stop during startNewThread (more specifically between the check on start and the thread be added to runningThreads
        {
            started = false;
        }

        long t0 = System.currentTimeMillis();
        while(runningThreads.size()>0)
        {
            for (Thread t : runningThreads)
            {
                try
                {
                    t.join(1000);
                    if(!t.isAlive())
                        runningThreads.remove(t);
                }
                catch (InterruptedException e)
                {
                    Commons.logger.info("InterruptedException, but I'm waiting for the other threads");
                }
            }
            if(runningThreads.size()>0 && System.currentTimeMillis()-t0> STOP_THREAD_TIMEOUT)
            {
                Commons.logger.warning(""+runningThreads.size()+" threads not responding, interrupting...");
                for (Thread t : runningThreads)
                {
                    Commons.logger.info("interrupting thread "+t.getName());
                    t.interrupt();
                    if(t instanceof IncomingConnectionsHandler.RequestHandler)
                    {
                        try
                        {
                            ((IncomingConnectionsHandler.RequestHandler) t).socket.close();
                        }
                        catch(IOException e)
                        {
                            e.printStackTrace();
                            Commons.logger.warning("IOException couldn't close socket");
                        }
                    }
                }
            }
        }
        Commons.logger.info("CommunicationsManager stopped.");
    }


    /**
     * Gets all the known nodes
     * @return The List of all the non nodes
     */
    public List<Node> getKnownNodes()
    {
        return new ArrayList<>(knownNodes.keySet());
    }


    /**
     * Gets all the known nodes
     * @return The List of all the non nodes
     */
    public List<Node> getReachableNodesList()
    {
        return new ArrayList<>(reachableNodes.keySet());
    }


    /**
     * returns the local port at which this node is reachable from other nodes
     * @return the port
     */
    public int getLocalPort()
    {
        return localPort;
    }

    /**
     * Gets all the known nodes
     * @return The List of all the non nodes
     */
    ConcurrentSkipListMap<Node,RemoteNode> getReachableNodes()
    {
        return reachableNodes;
    }


    public IBlocksTransactionsExchanger getBlockTransactionExchanger()
    {
        return blockOrTransactionReceiver;
    }


    public double getConnectivityQuality()
    {
        return connectivityQuality;
    }

    public boolean canReceiveConnections()
    {
        return connectivityQuality>0;
    }






    public void updateReachableNodesAsync()
    {
        startNewThread(this::updateReachableNodes);
    }

    ChosenNodesManager getChosenNodesManager()
    {
        return chosenNodesManager;
    }

    IncomingSubscriptionsManager getIncomingSubscriptionsManager()
    {
        return incomingSubscriptionsManager;
    }

    public List<Node> getNodesThatChoseUs()
    {
        return incomingSubscriptionsManager.getNodes();
    }

    public List<Node> getNodesWeChose()
    {
        return chosenNodesManager.getNodes();
    }

    /**
     * Returns true if the CommunicationsManager is connected to input and output nodes. This
     * means it is able to receive and spread blocks and transactions
     * @return see description
     */
    public boolean isConnected()
    {
        return chosenNodesManager.chosenNodesNumber()>0 /*&& incomingSubscriptionsManager.nodesNumber()>0*/;
    }

    /**
     * waits for isConnected to become true
     * @param timeoutMillis timeout for the waiting, if negative it means to wait indefinitely
     * @throws InterruptedException if it is interrupted
     */
    public void waitConnection(long timeoutMillis) throws InterruptedException
    {
        long t0 = System.currentTimeMillis();
        Commons.logger.info("waiting for connection...");
        while(!isConnected() && (timeoutMillis<0 || System.currentTimeMillis()-t0<timeoutMillis))
        {

        //    incomingSubscriptionsManager.waitForAtLeastOneNodeToChooseUs(timeoutMillis-(System.currentTimeMillis()-t0));
            //Commons.logger.info("one node chose us (or I timed out)");
            long timeout2 = timeoutMillis-(System.currentTimeMillis()-t0);
            if(timeout2>0)
                chosenNodesManager.waitForAtLeastOneChosenNode(timeout2);
            //Commons.logger.info("we chose one node (or I timed out)");
        }
    }



    public int getLastKnownBlockID()
    {
        return getChosenNodesManager().askLastKnownBlockId(this);
    }

    public NodeConnectionsManager getNodeConnectionsManager()
    {
        return nodeConnectionsManager;
    }







    //::::::::::::::::TRANSACTION SPREADING:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


    /**
     * Calls {@link #spreadTransaction(ITransaction,int,Node)} in a new Thread
     * @param transaction the transaction to be sent
     * @param onCompletion This will executed after {@link #spreadTransaction(ITransaction, int,Node)} has finished. The parameter contains the number of nodes to which the transaction was successfully sent.
     * @param timeToLive time to live to use (gets decremented at each hop)
     * @param excludedNode This serverNode will be excluded from the first hop of the spreading, set it to null if you don't need this
     */
    public void spreadTransactionAsync(ITransaction transaction, Consumer<Integer[]> onCompletion, int timeToLive,Node excludedNode)
    {
        startNewThread(()->onCompletion.accept(spreadTransaction(transaction, timeToLive,excludedNode)));
    }

    /**
     * Same as {@link #spreadTransactionAsync(ITransaction,Consumer,int,Node)} but without the callback
     * @param transaction The transaction to be sent
     * @param timeToLive time to live to use (gets decremented at each hop)
     * @param excludedNode This serverNode will be excluded from the first hop of the spreading, set it to null if you don't need this
     */
    public void spreadTransactionAsync(ITransaction transaction, int timeToLive, Node excludedNode)
    {
        spreadTransactionAsync(transaction, (result)->{
            if(result[1]==0)
                Commons.logger.warning("Couldn't send transaction to anyone ("+result[0]+" send attempted)");
        },timeToLive,excludedNode);
    }

    /**
     * Same as {@link #spreadTransactionAsync(ITransaction,int,Node)} but with timeToLive = {@link CommandSpreadTransaction#INITIAL_TIME_TO_LIVE}
     * @param transaction The transaction to be sent
     * @param excludedNode This serverNode will be excluded from the first hop of the spreading, set it to null if you don't need this
     */
    public void spreadTransactionAsync(ITransaction transaction, Node excludedNode)
    {
        spreadTransactionAsync(transaction,CommandSpreadTransaction.INITIAL_TIME_TO_LIVE,excludedNode);
    }

    /**
     * Same as {@link #spreadTransactionAsync(ITransaction,int,Node)} but with timeToLive = {@link CommandSpreadTransaction#INITIAL_TIME_TO_LIVE} and excludedNode=null
     * @param transaction The transaction to be sent
     */
    public void spreadTransactionAsync(ITransaction transaction)
    {
        spreadTransactionAsync(transaction,CommandSpreadTransaction.INITIAL_TIME_TO_LIVE,null);
    }




    //::::::::::::::::BLOCK SPREADING::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::




    /**
     * Calls {@link #spreadBlock(IBlock, int,Node)} in a new Thread
     * @param block the block to be sent
     * @param onCompletion This will executed after {@link #spreadBlock(IBlock, int,Node)} has finished
     * @param timeToLive time to live to use (gets decremented at each hop)
     * @param excludedNode This serverNode will be excluded from the first hop of the spreading, set it to null if you don't need this
     */
    public void spreadBlockAsync(IBlock block, Consumer<Integer[]> onCompletion, int timeToLive, Node excludedNode)
    {
        startNewThread(()->onCompletion.accept(spreadBlock(block, timeToLive,excludedNode)));
    }

    /**
     * Same as {@link #spreadBlockAsync(IBlock,Consumer, int, Node)} but without the callback
     * @param block The block to be sent
     * @param timeToLive time to live to use (gets decremented at each hop)
     * @param excludedNode This serverNode will be excluded from the first hop of the spreading, set it to null if you don't need this
     */
    public void spreadBlockAsync(IBlock block, int timeToLive, Node excludedNode)
    {
        spreadBlockAsync(block, (result)->{
            if(result[1]==0)
                Commons.logger.warning("Couldn't send block to anyone ("+result[0]+" send attempted)");
        },timeToLive,excludedNode);
    }


    /**
     * Same as {@link #spreadBlockAsync(IBlock,int,Node)} but with timeToLive = {@link CommandSpreadBlock#INITIAL_TIME_TO_LIVE}
     * @param block The block to be sent
     * @param excludedNode This serverNode will be excluded from the first hop of the spreading, set it to null if you don't need this
     */
    public void spreadBlockAsync(IBlock block, Node excludedNode)
    {
        spreadBlockAsync(block,CommandSpreadBlock.INITIAL_TIME_TO_LIVE,excludedNode);
    }

    /**
     * Same as {@link #spreadBlockAsync(IBlock,Node)} but with excludedNode=null
     * @param block The block to be sent
     */
    public void spreadBlockAsync(IBlock block)
    {
        spreadBlockAsync(block,CommandSpreadBlock.INITIAL_TIME_TO_LIVE,null);
    }




    //::::::::::::::::NODE SEARCHING::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * Searches for new nodes asking to all known ones
     * @return A Set containing the newly discovered nodes
     */
    public List<RemoteNode> searchNewNodes()
    {
        if(knownNodes.size()<1)
        {
            Commons.logger.warning(" no known nodes");
            return new ArrayList<>();
        }

        RemoteNodeDispenser rnd = new RemoteNodeDispenser(knownNodes);
        List<RemoteNode> allNewNodes = new ArrayList<>();//mi segno quelli da aggiungere, per non chiedere anche ai nuovi nodi appena scoperti, potrebbe diventare eccessivo

        while(true)
        {
            //Commons.logger.info("asking "+rn.toString());
            RemoteNode provider = rnd.getNode();
            if(provider==null)
                break;
            List<RemoteNode> newRemoteNodes = provider.askForNeighbours(localPort,this);
            Commons.logger.info(""+provider+" provided "+newRemoteNodes);
            if(newRemoteNodes!=null)
            {
                List<RemoteNode> correctedNodes = new ArrayList<>();
                for(int i =0;i<newRemoteNodes.size();i++)
                {
                    RemoteNode rn = newRemoteNodes.get(i);
                    if(rn.getNode().getInetAddress().isLoopbackAddress())
                    {
                        newRemoteNodes.remove(i);
                        i--;
                        //we can't add directly to newRemoteNodes, it would cause an infinite loop
                        correctedNodes.add(new RemoteNode(new Node(provider.getNode().getInetAddress(),rn.getNode().getPort())));
                    }
                }
                allNewNodes.addAll(newRemoteNodes);
                allNewNodes.addAll(correctedNodes);
            }
        }


        removeOurAddresses(allNewNodes);

        Commons.logger.info("discovered new nodes: "+allNewNodes);

        if(allNewNodes.size()>0)//if new nodes were found
        {
            //add them to the known nodes
            addKnownNodes(allNewNodes);
            //And now from the new set of known nodes select which ones to use
            updateChosenNodesAsync(1000);
        }

        return allNewNodes;
    }


    /**
     * Calls {@link #searchNewNodes()} in a new Thread
     * @param onCompletion This will executed after {@link #searchNewNodes()} has finished, the parameter contains he newly added nodes
     */
    public void searchNewNodesAsync(Consumer<List<RemoteNode>> onCompletion)
    {
        startNewThread(()->onCompletion.accept(searchNewNodes()));
    }


    /**
     * Same as {@link #searchNewNodesAsync(Consumer)} but with a default callback
     */
    public void searchNewNodesAsync()
    {
        searchNewNodesAsync((newNodes)->{
            if(newNodes.size()==0)
                Commons.logger.warning("CommunicationsManager.searchNewNodesAsync: didn't get any new serverNode");
        });
    }




//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//      PRIVATE METHODS
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::











    /**
     * Initialization of the communicationsManager
     */
    private void initialize()
    {
        searchNewNodes();
        updateReachableNodes();
        RemoteNodeDispenser rnd = new RemoteNodeDispenser(knownNodes);
        RemoteNode rn = rnd.getNode();
        while (rn!=null)
        {
            Commons.logger.info("ping to "+rn+" = "+(rn.getEstimatedRttNano()/1000_000)+"ms: reachable="+reachableNodes.containsKey(rn.getNode()));
            rn=rnd.getNode();
        }
        //subscribeToNodes();


        startReachabilityPoller();
    }


    /**
     * Checks if address may be reached from thirdParty, knowing that both address and thirdParty are reachable from ourselves.
     * This is defined to be true if:
     *      - address is public (neither link-local neither site-local)
     *      - address and third party are both in the same subnet from our piont of view
     *      - the port used by address is different from our (so there could be a port forward towards him)
     * @param address the address to check
     * @param thirdParty the third party
     * @return if address is reachable by thirdParty
     */
    private boolean isReachableByThirdParty(Node address, Node thirdParty)
    {
        if(!address.getInetAddress().isLinkLocalAddress() && !address.getInetAddress().isSiteLocalAddress())
            return true;//se l'indirizzo è pubblico
        if(CommUtils.areInSameSubnet(address.getInetAddress(),thirdParty.getInetAddress()))
            return true;//se non è pubblico ma è nella stessa subnet di thirdParty
        if(address.getPort()!=localPort)
            return true;//se l'indirizzo non è pubblico, però la porta è diversa, quindi potrebbe esserci un port forward anche verso address
        return false;
    }

    /**
     * Gets the known nodes which should be reachable by requestingNode (according to {@link #isReachableByThirdParty(Node, Node)}
     * @param requestingNode the serverNode from which the nodes should be reachable
     * @return a List of Nodes
     */
    List<Node> getReachableNodes(Node requestingNode)
    {
        List<Node> nodes = new ArrayList<>(knownNodes.keySet());
        List<Node> filteredNodes = new ArrayList<>(nodes.size());
        for(int i=0;i<nodes.size();i++)
        {
            Node n = nodes.get(i);
            if(isReachableByThirdParty(n,requestingNode) && !n.equals(requestingNode))
                filteredNodes.add(n);
        }
        return filteredNodes;
    }

    /**
     * gets the known nodes with these details
     * @param addr the ip address
     * @param port the port
     * @return null if there is no such known node
     */
    RemoteNode getKnownNode(InetAddress addr, int port)
    {
        return getKnownNode(new Node(addr,port));
    }

    /**
     * get the known node for the specified node
     * @param n the node
     * @return null if there is no such known node
     */
    RemoteNode getKnownNode(Node n)
    {
        return knownNodes.get(n);
    }


    /**
     * Adds the node to our knowledge
     * @param rn the node to be added
     * @return true if the node has been added (i.e. we didn't knw it already)
     */
    boolean addKnownNode(RemoteNode rn)
    {
//        Thread.currentThread().dumpStack();
        List<RemoteNode> nodes = new ArrayList<>();
        nodes.add(rn);
        return addKnownNodes(nodes)==1;
    }

    /**
     * Adds the provided nodes to the known nodes
     * @param newNodes the nodes to add
     * @return the number of nodes that were added and weren't known beforehand
     */
    int addKnownNodes(List<RemoteNode> newNodes)
    {
        Commons.logger.fine("addKnownNodes...");
        //Commons.logger.info("known nodes = "+knownNodes);
        removeOurAddresses(newNodes);
        convertOurAddressesToLocalhost(newNodes);

        int addedNodesCount=0;
        TreeMap<Node,RemoteNode> nodesThatHaveToBeChecked = new TreeMap<>();
        for(RemoteNode rn :newNodes)
        {
            if(!knownNodes.containsKey(rn.getNode()))
            {
                //Commons.logger.info(" known nodes doesnt contain " + rn);
                knownNodes.put(rn.getNode(),rn);
                Commons.logger.info("knew new node " + rn);
                addedNodesCount++;
                nodesThatHaveToBeChecked.put(rn.getNode(),rn);
            }
            else if(knownNodes.get(rn.getNode()).getEstimatedRttNano()<0)
            {
                //Commons.logger.info(""+rn+" is already known but we will check if we can reach him");
                nodesThatHaveToBeChecked.put(rn.getNode(),knownNodes.get(rn.getNode()));
                //knownNodes.get(rn.getNode()).setReachable();//to avoid an infinite loop on this function
                //Commons.logger.info("serverNode " + rn+" already present "+"(contains "+knownNodes.get(rn)+")");
            }
        }
        /*
        for(RemoteNode rn :newNodes)
        {
            updateNodeReachability(getKnownNode(rn.getNode()));
        }*/

        if(nodesThatHaveToBeChecked.size()>0)
            updateReachableNodes(nodesThatHaveToBeChecked);
        /*if(addedNodesCount>0 && chosenNodesManager.wantsMoreNodes())
        {
            updateChosenNodesAsync();
        }*/
        return addedNodesCount;
    }

    /**
     * Removes the node from memory, if it is known
     * @param n the node to be removed
     */
    private void removeKnownNode(Node n)
    {
        knownNodes.remove(n);
    }

    private static List<RemoteNode> stringArrayToRemoteNodeList(String[] stringhosts)
    {
        List<RemoteNode> rns = new ArrayList<>();
        for(String addr : stringhosts)
        {
            String[] addrAndPort = addr.split(":");
            if(addrAndPort.length==1)
                addrAndPort = new String[]{addrAndPort[0],""+DEFAULT_PORT};
            try
            {
                rns.add(new RemoteNode(addrAndPort[0],Integer.parseInt(addrAndPort[1])));
            }
            catch (UnknownHostException e)
            {
                Commons.logger.warning("Unknown Host "+addr);
            }
        }
        return rns;
    }



    private static Thread buildThread(Runnable runnable)
    {
        Thread t = new Thread(runnable);
        t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(Thread t, Throwable e)
            {
                Commons.logger.warning("CommunicationsManager Thread "+t.getName()+" died with: "+e.getCause()+"\n stacktrace: "+ ExceptionUtils.getStackTrace(e));
            }
        });
        return t;
    }



    /**
     * Starts a new thread on the provided Runnable and keeps memory of it
     * @param t the runnable to run in the thread
     * @throws IllegalStateException If called while {@link CommunicationsManager} not started
     */
    void startNewThread(Runnable t)
    {
        synchronized (startedGuard)
        {
            if (!started)
                throw new IllegalStateException("Requested thread start while not started");
            Thread wrapper = buildThread(new Runnable()
            {
                @Override
                public void run()
                {
                    t.run();
                    runningThreads.remove(Thread.currentThread());
                    //Commons.logger.info("ended thread (t#="+runningThreads.size()+")");
                }
            });
            wrapper.start();
            runningThreads.add(wrapper);
            //Commons.logger.info("starting new thread (t#="+runningThreads.size()+")");
        }
    }


    /**
     * Internal utility method for spreading both blocks and transactions
     * @param blockOrTransaction the block or transaction to be spread
     * @param timeToLive  time to live to use (gets decremented at each hop)
     * @param excludedNode the block/transaction wont be sent to this node
     * @return a two-value array with the number of attempted sends in cell 0 and the number of successful sends on cell 1
     */
    private Integer[] spread(Object blockOrTransaction, int timeToLive, Node excludedNode)
    {
        if(!(blockOrTransaction instanceof IBlock) && !(blockOrTransaction instanceof ITransaction))
            throw new IllegalArgumentException("blockOrTransaction should be either an IBlock or an ITransaction");
        int successfulSends = 0;
        int attemptedSends = 0;


        if(chosenNodesManager.chosenNodesNumber()>0)
        {
            Integer[] sends;
            if(blockOrTransaction instanceof IBlock)
                sends = chosenNodesManager.spreadBlock((IBlock) blockOrTransaction, timeToLive,this, excludedNode);
            else
                sends = chosenNodesManager.spreadTransaction((ITransaction)blockOrTransaction, timeToLive,this, excludedNode);
            attemptedSends += sends[0];
            successfulSends += sends[1];
        }

        if(incomingSubscriptionsManager.nodesNumber()>0)
        {
            Integer[] sends;
            if(blockOrTransaction instanceof IBlock)
                sends = incomingSubscriptionsManager.spreadBlock((IBlock) blockOrTransaction, timeToLive,this, excludedNode);
            else
                sends = incomingSubscriptionsManager.spreadTransaction((ITransaction)blockOrTransaction, timeToLive,this, excludedNode);
            attemptedSends += sends[0];
            successfulSends += sends[1];
        }

        if(attemptedSends==0)
        {
            Commons.logger.warning("Could not spread the "+((blockOrTransaction instanceof IBlock)? "block" : "transaction")+", no subscribed nodes and no chosen nodes");
        }

        return new Integer[]{attemptedSends,successfulSends};
    }




    /**
     * Sends the transaction to all chosen nodes
     * @param transaction the transaction to be sent
     * @return In the first element the number of sends attempted, in the second the number of nodes to which the transaction was successfully sent
     */
    Integer[] spreadTransaction(ITransaction transaction, int timeToLive, Node excludedNode)
    {
        return spread(transaction,timeToLive,excludedNode);
    }


    /**
     * Sends the block to all chosen nodes
     * @param block the block to be sent
     * @param timeToLive
     * @param excludedNode This serverNode will be excluded from the first hop of the spreading, set it to null if you don't need this
     * @return In the first element the number of sends attempted, in the second the number of nodes to which the block was successfully sent
     */
    Integer[] spreadBlock(IBlock block, int timeToLive, Node excludedNode)
    {
        return spread(block,timeToLive,excludedNode);
    }




    /**
     * Asks the network for a block
     * @param id the id of the block to ask
     * @return A Set containing the received blocks
     */
    public Set<IBlock> getBlocks(int id)
    {
        return chosenNodesManager.askBlock(id,this);
    }


    /**
     * Removes addresses that correspond to us from the list
     * @param nodes the list
     */
    private void removeOurAddresses(List<RemoteNode> nodes)
    {
        if(nodes.size()==0)
            return;

        List<InetAddress> ourLocalAddresses = new ArrayList<>();
        try
        {
            for (Enumeration<NetworkInterface> nie = NetworkInterface.getNetworkInterfaces(); nie.hasMoreElements(); )
            {
                for (Enumeration<InetAddress> addressEnumeration = nie.nextElement().getInetAddresses(); addressEnumeration.hasMoreElements(); )
                    ourLocalAddresses.add(addressEnumeration.nextElement());
            }
        }
        catch(SocketException e)
        {
            Commons.logger.severe("Couldn't get network interfaces to determine localhost addresses");
        }
        //Commons.logger.info("our addresses = "+ourLocalAddresses);

        for(int i=0;i<nodes.size();i++)
        {
            RemoteNode remoteNode = nodes.get(i);
            if(ourLocalAddresses.contains(remoteNode.getNode().getInetAddress()) && remoteNode.getNode().getPort()==getLocalPort())
            {
                //Commons.logger.info("removed serverNode "+remoteNode);
                nodes.remove(i);
                i--;
            }
        }
    }


    /**
     * Converts the ip addresses that correspond to us in the list to localhost addresses, the port is kept as is
     * @param nodes the list
     */
    private void convertOurAddressesToLocalhost(List<RemoteNode> nodes)
    {
        if(nodes.size()==0)
            return;

        List<InetAddress> ourLocalAddresses = new ArrayList<>();
        try
        {
            for (Enumeration<NetworkInterface> nie = NetworkInterface.getNetworkInterfaces(); nie.hasMoreElements(); )
            {
                for (Enumeration<InetAddress> addressEnumeration = nie.nextElement().getInetAddresses(); addressEnumeration.hasMoreElements(); )
                    ourLocalAddresses.add(addressEnumeration.nextElement());
            }
        }
        catch(SocketException e)
        {
            Commons.logger.severe("Couldn't get network interfaces to determine localhost addresses");
        }


        //just to be sure
        for(int i=0;i<ourLocalAddresses.size();i++)
        {
            if(ourLocalAddresses.get(i).isLoopbackAddress())
            {
                ourLocalAddresses.remove(i);
                i--;
            }
        }

        //Commons.logger.info("our addresses = "+ourLocalAddresses);

        for(int i=0;i<nodes.size();i++)
        {
            RemoteNode remoteNode = nodes.get(i);
            if(ourLocalAddresses.contains(remoteNode.getNode().getInetAddress()))
            {
                nodes.remove(i);
                i--;
                RemoteNode newRn = null;
                try
                {
                    newRn = new RemoteNode("localhost",remoteNode.getNode().getPort());
                    nodes.add(newRn);
                }
                catch (UnknownHostException e)
                {
                    //this shouldn't happen
                }
                //Commons.logger.info("converted serverNode "+remoteNode+" to "+newRn);
            }
        }
    }


    /**
     * checks the connectivity of this node, the result of this operation can be seen in {@link #getConnectivityQuality()}
     */
    private void checkConnectivity()
    {
        if(reachableNodes.size()<1)
        {
            Commons.logger.warning(" no reachable nodes");
            throw new IllegalStateException("No reachable nodes, can't check connectivity");
        }

        final AtomicInteger positiveResultsCount = new AtomicInteger(0);
        final AtomicInteger testsAttempted = new AtomicInteger(0);
        final AtomicInteger testsPerformed = new AtomicInteger(0);
        CommUtils.runInParallel((remoteNode)->
        {
            //Commons.logger.info("checking with "+remoteNode);
            testsAttempted.incrementAndGet();
            for(int retries=3;retries>0;retries--)
            {
                boolean testIsPositive = ((RemoteNode)remoteNode).testMyConnection(localPort,this);
                testsPerformed.incrementAndGet();
                if(testIsPositive)
                {
                    positiveResultsCount.incrementAndGet();
                    break;
                }
            }
        }, 10, reachableNodes);

        //Commons.logger.info("connectivity check contacted "+testsPerformed.get()+" nodes");
        if(testsPerformed.get()>0)
            connectivityQuality = ((double)positiveResultsCount.get())/testsPerformed.get();

    }


    /**
     * Checks the reachability of the provided nodes and updates reachableNodes
     * @param nodesToCheck this should be a subset of knownNodes
     */
    private void updateReachableNodes( NavigableMap<Node,RemoteNode> nodesToCheck)
    {
        //Commons.logger.info("estimating rtts... (nodesToCheck.size()="+nodesToCheck.size()+", reachableNodes.size()="+reachableNodes.size()+")");
        if(knownNodes.size()==0)
        {
            return;
        }
        int[] newReachables=new int[]{0};
        CommUtils.runInParallel((obj)->{
            RemoteNode rn = ((RemoteNode)obj);
            //Commons.logger.info("estimating rtt for "+rn+" i'm "+Thread.currentThread().getName());
            synchronized (CommunicationsManager.this)
            {
                //Commons.logger.info("really estimating rtt for "+rn+" i'm "+Thread.currentThread().getName());

                rn.estimateRoundTripTime(getLocalPort(), this);
                //Commons.logger.info("estimated rtt for "+rn+" i'm "+Thread.currentThread().getName());
                boolean wasAdded = false;
                if (rn.getEstimatedRttNano() >= 0)
                {
                    wasAdded = reachableNodes.put(rn.getNode(), rn)==null;
                }
                else
                {
                    boolean[] reachableThroughConnection = new boolean[]{false};
                    try
                    {
                        getNodeConnectionsManager().useConnectionToNode(rn.getNode(), this, true, (connection) -> {
                            connection.estimateRoundTripTime();
                            if (connection.getEstimatedRttNano() >= 0)
                                reachableThroughConnection[0] = true;
                        });
                    }
                    catch (CommunicationErrors.CommunicationException e)
                    {
                        //thats fine
                    }

                    if(reachableThroughConnection[0])
                    {
                        wasAdded = reachableNodes.put(rn.getNode(), rn)==null;
                    }
                    else
                    {
                        reachableNodes.remove(rn.getNode());
                        RemoteNode rnFromKnown = getKnownNode(rn.getNode());
                        if(rnFromKnown!=null && System.currentTimeMillis()-rn.getLastContactTime()>FORGETTING_TIME)
                            removeKnownNode(rnFromKnown.getNode());
                        Commons.logger.info("" + rn.getNode() + " was removed from the reachable nodes");
                    }
                }
                if(wasAdded)
                {
                    Commons.logger.info(""+rn.getNode()+" was added to the reachable nodes");
                    newReachables[0]++;
                }
            }
            //Commons.logger.info("updated reachability rtt for "+rn+" i'm "+Thread.currentThread().getName());
            //Commons.logger.info("estimated");
        } , 10, nodesToCheck);
        if(newReachables[0]>0 && getChosenNodesManager().wantsMoreNodes())
            updateChosenNodesAsync(2000);

        Commons.logger.info("done, estimated rtts (knownNodes.size()="+knownNodes.size()+", reachableNodes.size()="+reachableNodes.size()+")");
    }


    /**
     * updates the reachable nodes list
     */
    private void updateReachableNodes()
    {
        updateReachableNodes(knownNodes);
    }


    /**
     * same as {@link #updateReachableNodes()} but asynchronous
     * @param delay specifies a delay to be used before doing the update
     */
    void updateChosenNodesAsync(long delay)
    {
        long t0 = System.currentTimeMillis();
        while(System.currentTimeMillis()-t0<delay)
        {
            try
            {
                Thread.sleep(delay-(System.currentTimeMillis()-t0));
            }
            catch (InterruptedException e)
            {
            }
        }
        //Commons.logger.info("updateChosenNodesAsync");
        startNewThread(()->
        {
            //Commons.logger.info("updateChosenNodesAsync thread started");
            chosenNodesManager.updateNodes(this);
        });
    }


    /**
     * This tells the CommunicationsManager object that maybe the Node did shutdown, this will cause some checks on the node
     * @param node the node
     */
    void onNodeMaybeTurnedOff(Node node)
    {
        RemoteNode rn = getKnownNode(node);
        if(rn!=null)
        {
            TreeMap<Node,RemoteNode> nodeMap = new TreeMap<>();
            nodeMap.put(rn.getNode(),rn);
            updateReachableNodes(nodeMap);
        }
    }


    /**
     * starts a thread that periodically checks the reachability of the nodes and searches for new nodes if necessary
     */
    private void startReachabilityPoller()
    {
        startNewThread(()->{
            while(isStarted())
            {
                try
                {
                    Thread.sleep(10*60_000);//ten minutes
                }
                catch (InterruptedException e)
                {
                    break;
                }

                chosenNodesManager.unsubscribeOneRandomNode(this);
                if(chosenNodesManager.wantsMoreNodes())
                    searchNewNodes();
                updateReachableNodes();
                if(reachableNodes.size()>0)
                    checkConnectivity();
            }
        });
    }
}
