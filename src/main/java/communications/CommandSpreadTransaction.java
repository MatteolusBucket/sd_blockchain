package communications;


import blockchain.transactions.ITransaction;
import utils.Commons;

/**
 * Command to send a transaction to a node, even if he didn't ask for it. (e.g. used to spread new transactions in the graph)
 */
class CommandSpreadTransaction extends SuperCommand
{
    public static final long serialVersionUID = 1L;

    private ITransaction transaction;
    private int timeToLive;
    static final int INITIAL_TIME_TO_LIVE = 100;
    CommandSpreadTransaction(ITransaction transaction, int timeToLive, long commandTransactionId)
    {
        super(commandTransactionId);
        this.transaction = transaction;
        this.timeToLive = timeToLive;
    }

    CommandSpreadTransaction(ITransaction transaction, int timeToLive)
    {
        this.transaction = transaction;
        this.timeToLive = timeToLive;
    }




    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        if(cm.getBlockTransactionExchanger().onReceive(transaction) && timeToLive>1)//se non mi era mai arrivato
        {
            Commons.logger.info("Accepted transaction "+transaction.getID()+" with ttl = "+timeToLive+" from "+sender);
            cm.spreadTransactionAsync(transaction, timeToLive - 1, sender);
        }
        else
        {
            Commons.logger.info("Rejected transaction "+transaction.getID()+" with ttl = "+timeToLive+" from "+sender);
        }
        return new CommandReturn(null);
    }


    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        return null;
    }
}
