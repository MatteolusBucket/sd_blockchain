package communications;

import utils.Commons;

/**
 * command used to ask a node to ping us, and so tell us if we are reachable from the outside
 */
class CommandTestMyConnection extends SuperCommand
{
    public static final long serialVersionUID = 1L;

    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        return new CommandReturn(new RemoteNode(sender).pingNano(cm.getLocalPort(), cm)>0);
    }

    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        Object o =  res.getArg();
        if(o instanceof Boolean)
            return o;
        else
        {
            Commons.logger.warning("received invalid response");
            return null;
        }
    }
}
