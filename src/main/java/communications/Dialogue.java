package communications;

import org.apache.commons.lang3.exception.ExceptionUtils;
import utils.Commons;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.*;

abstract class Dialogue implements Serializable
{

    /**
     * Useful to contact the sender back
     */
    private int senderPort;

    Dialogue(int senderPort)
    {
        this.senderPort = senderPort;
    }

    int getSenderPort()
    {
        return senderPort;
    }



    /**
     * Class representing the result of the send() method. It also contains the Response from the server
     */
    public static class Result
    {
        private CommunicationErrors.FailReason failReason=CommunicationErrors.FailReason.DIDNT_FAIL;

        private SuperCommand.CommandReturn response;

        /**
         * Successful here means the command was sent and a response was received, this doesn't imply the response was positive
         * @return true if the command was executed correctly
         */
        boolean wasCommandExecutedCorrectly()
        {
            return failReason==CommunicationErrors.FailReason.DIDNT_FAIL;
        }

        CommunicationErrors.FailReason getFailReason()
        {
            return failReason;
        }

        public SuperCommand.CommandReturn getResponse()
        {
            return response;
        }
    }

    /**
     * Starts the dialogue towards the provided destination
     * @param destination The serverNode with which we start the connection
     * @param connectionTimeout Timeout for establishing the connection
     * @param responseTimeout Timeout for each response coming from the remote serverNode during the dialogue
     * @param cm The communication manager
     * @return the result of the dialogue
     */
    public Result start(Node destination, int connectionTimeout, int responseTimeout, CommunicationsManager cm)
    {
        return start(destination,connectionTimeout,responseTimeout,cm,false);
    }

    /**
     * Starts the dialogue towards the provided destination
     * @param destination The serverNode with which we start the connection
     * @param connectionTimeout Timeout for establishing the connection
     * @param responseTimeout Timeout for each response coming from the remote serverNode during the dialogue
     * @param cm The communication manager
     * @param dontPrintErrors to disable the logging of the errors
     * @return the result of the dialogue
     */
    public Result start(Node destination, int connectionTimeout, int responseTimeout, CommunicationsManager cm, boolean dontPrintErrors)
    {
        Result res = new Result();
        Socket socket = new Socket();
        boolean cancel = false;
        try
        {
            socket.setSoTimeout(responseTimeout);
        }
        catch (SocketException e)
        {
            if(!dontPrintErrors)
                Commons.logger.warning("dialogue("+ getClass().getSimpleName()+"): couldn't set connection timeout, dropping connection\n"+ExceptionUtils.getStackTrace(e));
            cancel=true;
            res.failReason=CommunicationErrors.FailReason.CANNOT_SETUP_SOCKET;
        }
        if(!cancel)
        {
            try
            {
                socket.connect(new InetSocketAddress(destination.getInetAddress(), destination.getPort()), connectionTimeout);
            }
            catch (SocketTimeoutException e)
            {
                if(!dontPrintErrors)
                    Commons.logger.warning("dialogue("+ getClass().getSimpleName()+"): Socket connection timed out, dropping connection to " + destination.getInetAddress() + ":" + destination.getPort()+"\t"+e.getMessage());
                res.failReason=CommunicationErrors.FailReason.TIMEOUT_ESTABLISHING_CONNECTION;
            }
            catch (IOException e)
            {
                if(!dontPrintErrors)
                    Commons.logger.warning("dialogue("+ getClass().getSimpleName()+"): IOException, cannot create streams for socket to " + destination.getInetAddress() + ":" + destination.getPort() + "\t"+e.getMessage());
                res.failReason = CommunicationErrors.FailReason.CANNOT_ESTABLISH_CONNECTION;
                return res;
            }
            if (socket.isConnected())
            {
                try (
                        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                        ObjectInputStream in = new ObjectInputStream(socket.getInputStream())
                )
                {
                    //send the dialogue to the recipient to tell him what to do
                    boolean exchangeSent=false;
                    try
                    {
                        out.writeObject(this);
                        exchangeSent = true;
                    }
                    catch (IOException e)
                    {
                        if(!dontPrintErrors)
                            Commons.logger.warning("dialogue("+ getClass().getSimpleName()+"): dialogue failed: couldn't send dialogue. \t"+e.getMessage()+"\t"+e.getCause()+"\n cause stacktrace: "+ExceptionUtils.getStackTrace(e));
                    }

                    //start our side of the dialogue
                    if(exchangeSent)
                    {
                        try
                        {
                            res.response = run(out, in, destination, cm, socket);
                        }
                        catch (CommunicationErrors.CommunicationException e)
                        {
                            if(!dontPrintErrors)
                                Commons.logger.warning("dialogue(" + getClass().getSimpleName() + "): dialogue failed: " + e.failReason + "\t" + e.getMessage() + "\t" + e.getCause()+"\n cause stacktrace: "+ExceptionUtils.getStackTrace(e));
                            res.failReason = e.failReason;
                        }
                    }
                }
                catch (IOException e)
                {
                    if(!dontPrintErrors)
                        Commons.logger.warning("dialogue("+ getClass().getSimpleName()+"): IOException, cannot create streams for socket to " + destination.getInetAddress() + ":" + destination.getPort() + "\t"+e.getMessage()+"\n" + ExceptionUtils.getStackTrace(e));
                    res.failReason = CommunicationErrors.FailReason.CANNOT_ESTABLISH_CONNECTION;

                }
            }
        }

        try
        {
            socket.close();
        }
        catch (IOException e)
        {
            if(!dontPrintErrors)
                Commons.logger.warning("dialogue("+ getClass().getSimpleName()+"): IOException, can't close socket, sorry.\t"+e.getMessage()+"\n"+ExceptionUtils.getStackTrace(e));
        }

        return res;
    }

    /**
     * This method is executed by the client to perform the communication.
     * Keep in mind the exchange object has already been sent to the server
     * @param out output stream to be used
     * @param in input stream to be used
     * @return the CommandRturn for the executed command
     * @throws CommunicationErrors.CommunicationException if an error happens
     */
    protected abstract SuperCommand.CommandReturn run(ObjectOutputStream out, ObjectInputStream in, Node server, CommunicationsManager cm, Socket socket) throws CommunicationErrors.CommunicationException;

    /**
     * This method is executed by the server to handleCommand the incoming connection from the client
     * @param out output stream to be used
     * @param in input stream to be used
     * @return 0 if all is good
     */
    protected abstract int handle(ObjectOutputStream out, ObjectInputStream in, CommunicationsManager cm, InetAddress senderAddress, Socket socket)  throws CommunicationErrors.CommunicationException;

}
