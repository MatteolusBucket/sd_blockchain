package communications;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import utils.Commons;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * This class manages the nodes that choose to subscribeToNode to us. We have to send them our blocks/transactions
 */
class IncomingSubscriptionsManager
{
    private final ConcurrentSkipListMap<Node, Node> nodes = new ConcurrentSkipListMap<>();
    private static final int MAX_INCOMING_CONNECTIONS = 100;
    private final Object waitForAtLeastOneNodeToChooseUsNotifier = new Object();


    private boolean addNode(Node node)
    {
        if(nodes.size()>MAX_INCOMING_CONNECTIONS)
            return false;

        nodes.put(node,node);
        Commons.logger.info("added choosing serverNode "+node);
        synchronized (waitForAtLeastOneNodeToChooseUsNotifier)
        {
            waitForAtLeastOneNodeToChooseUsNotifier.notifyAll();
        }
        return true;
    }

    private Node removeNode(Node node)
    {
        return nodes.remove(node);
    }


    boolean subscribeToUs(Node node, CommunicationsManager cm)
    {
        boolean[] result = new boolean[]{false};
        try
        {
            cm.getNodeConnectionsManager().useConnectionToNode(node, cm, false, nodeConnection -> {

                if (nodeConnection == null)//ci sta arrivando la richiesta da lui, dovrebbe esistere
                {
                    Commons.logger.warning("incoming subscription request on an invalid connection");
                    result[0] = false;
                    return;
                }

                boolean connectionCanAcceptSubscription = nodeConnection.registerIncomingSubscription();
                if (connectionCanAcceptSubscription)
                {
                    result[0] = addNode(node);
                }
            });
        }
        catch (CommunicationErrors.CommunicationException e)
        {
            /*
            This shouldn't happen, as the connection should have been opened by the requesting node.
            useConnectionToNode shouldnt try to establish a new one
             */
            Commons.logger.warning("couldn't accept subscription request, coulnt establish connection");
        }

        return result[0];
    }


    void unsubscribeFromUs(Node node, CommunicationsManager cm)
    {
        if(removeNode(node)!=null)
        {
            try
            {
                cm.getNodeConnectionsManager().useConnectionToNode(node,cm,false,NodeConnection::unregisterIncomingSubscription);
            }
            catch (CommunicationErrors.CommunicationException e)
            {
                /*
                This shouldn't happen, if there is a subscription the connection should be already opened
                 */
                Commons.logger.warning("couldn't unsubscribe "+node+" from us, couldn't establish connection");
            }
        }
    }


    /**
     * Sends the block to all the subscribed nodes
     * @param block the block to be sent
     * @return In the first element the number of sends attempted, in the second the number of nodes to which the block was successfully sent
     */
    Integer[] spreadBlock(IBlock block, int timeToLive, CommunicationsManager cm, Node excludedNode)
    {
        return spread(block,timeToLive,cm,excludedNode);
    }


    /**
     * Sends the transaction to all the subscribed nodes
     * @param transaction the block to be sent
     * @return In the first element the number of sends attempted, in the second the number of nodes to which the transaction was successfully sent
     */
    Integer[] spreadTransaction(ITransaction transaction, int timeToLive, CommunicationsManager cm, Node excludedNode)
    {
        return spread(transaction,timeToLive,cm, excludedNode);
    }

    private Integer[] spread(Object bOrT, int timeToLive, CommunicationsManager cm, Node excludedNode)
    {
        Node node;
        try
        {
            node = nodes.firstKey();
        }
        catch (NoSuchElementException e)
        {
            return new Integer[]{0,0};
        }

        int successfulSends = 0;
        int attemptedSends = 0;
        while(node!=null)
        {
            if(!node.equals(excludedNode))
            {
                boolean[] res=new boolean[]{false};
                try
                {
                    if (bOrT instanceof IBlock)
                    {
                        cm.getNodeConnectionsManager().useConnectionToNode(node,cm,false, cnode->{
                            res[0] = cnode.sendBlock((IBlock) bOrT, timeToLive);
                        });
                    }
                    else if (bOrT instanceof ITransaction)
                    {
                        cm.getNodeConnectionsManager().useConnectionToNode(node,cm,false, cnode->{
                            res[0] = cnode.sendTransaction((ITransaction) bOrT, timeToLive);
                        });
                    }
                    else
                        throw new IllegalArgumentException("neither a block nor a transaction, it's a " + bOrT.getClass().getName());
                }
                catch (CommunicationErrors.CommunicationException e)
                {
                    Commons.logger.warning("couldn't sends block/transaction to "+node+" , couldn't establish connection");
                }
                if (res[0])
                    successfulSends++;
                attemptedSends++;
            }
            node = nodes.higherKey(node);
        }
        return new Integer[]{attemptedSends,successfulSends};
    }

    void disconnectAll(CommunicationsManager cm)
    {
        Commons.logger.info("unsubscribing nodes");
        Node node;
        try
        {
            node = nodes.firstKey();
        }
        catch (NoSuchElementException e)
        {
            return;
        }

        while(node!=null)
        {
            if(cm.getNodeConnectionsManager().isNodeRegistered(node))
            {
                try
                {
                    cm.getNodeConnectionsManager().useConnectionToNode(node, cm, true, cnode -> {
                        try
                        {
                            cnode.waitConnection(5000);
                        }
                        catch (InterruptedException e)
                        {
                            //do nothing
                        }
                        if (cnode.isConnectionEstablished())
                            cnode.askToDisconnect();
                        else if (!cnode.isDisconnecting())
                            cnode.askToDisconnectForced();

                    });
                }
                catch (CommunicationErrors.CommunicationException e)
                {
                    //this is not a problem
                }
                node = nodes.higherKey(node);
            }
        }
    }

    int nodesNumber()
    {
        return nodes.size();
    }

    List<Node> getNodes()
    {
        return new ArrayList<>(nodes.keySet());
    }

    /**
     * waits for at least one connection to be established (if there is already at least one it returns immediately)
     * @param timeoutMillis timeout for the waiting, if negative it means to wait indefinitely
     * @throws InterruptedException if the thread gets interrupted
     */
    void waitForAtLeastOneNodeToChooseUs(long timeoutMillis) throws InterruptedException
    {
        long t0 = System.currentTimeMillis();
        synchronized (waitForAtLeastOneNodeToChooseUsNotifier)
        {
            while(nodes.size()<1 && (timeoutMillis<0 || System.currentTimeMillis()-t0<timeoutMillis))
            {
                //Commons.logger.warning("waiting...");
                if(timeoutMillis>0)
                    waitForAtLeastOneNodeToChooseUsNotifier.wait(timeoutMillis - (System.currentTimeMillis()-t0));
                else
                    waitForAtLeastOneNodeToChooseUsNotifier.wait();
                //Commons.logger.warning("nodesWeAreSubscribedTo.size()="+nodesWeAreSubscribedTo.size());
            }
        }
    }

}
