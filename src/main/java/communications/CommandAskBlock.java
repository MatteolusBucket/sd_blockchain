package communications;

import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import utils.Commons;

import java.util.List;

/**
 * Command to ask a Node for a specific block.
 * The {@link communications.SuperCommand.CommandReturn#arg} in the Response contains a List of {@link Block} Object (if successful)
 */
class CommandAskBlock extends SuperCommand
{
    public static final long serialVersionUID = 1L;

    private int blockId;
    CommandAskBlock(int blockId)
    {
        this.blockId = blockId;
    }


    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        List<? extends IBlock> blocks = cm.getBlockTransactionExchanger().getBlock(blockId);
        if(blocks!=null)
            return new CommandReturn(blocks);
        else
            return new CommandReturn(false,"block not found",null);
    }

    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        Object o =  res.getArg();
        if((o instanceof List) && ((List)o).size()>0 && (((List)o).get(0) instanceof IBlock))
            return o;
        else
        {
            Commons.logger.warning("received invalid response");
            return null;
        }
    }
}
