package communications;

import utils.Commons;

/**
 * Command used to cancel a subscription we set up using  {@link CommandSubscribe}
 */
public class CommandUnsubscribe  extends SuperCommand
{
    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        Commons.logger.info("handling CommandUnubscribe");
        cm.getIncomingSubscriptionsManager().unsubscribeFromUs(sender,cm);
        return new CommandReturn(null);
    }

    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        return null;
    }
}
