package communications;

import utils.Commons;


/**
 * Command to ask a remote Node for the highest block id in its blockchain.
 * The {@link communications.SuperCommand.CommandReturn#arg} in the Response contains an  Integer representing the response
 */
public class CommandAskLastKnownBlockID extends SuperCommand
{
    public static final long serialVersionUID = 1L;

    CommandAskLastKnownBlockID()
    { }


    @Override
    public CommandReturn handleCommand(CommunicationsManager cm, Node sender)
    {
        return new CommandReturn(new Integer(cm.getBlockTransactionExchanger().getLastKnownBlockId()));
    }

    @Override
    public Object getResponseArgsChecking(CommandReturn res)
    {
        Object o =  res.getArg();
        if((o instanceof Integer))
            return o;
        else
        {
            Commons.logger.warning("received invalid response");
            return null;
        }
    }
}
