package communications;

import java.util.List;
import java.util.Scanner;

import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import incoming.IBlocksTransactionsExchanger;
import blockchain.transactions.ITransaction;
import blockchain.transactions.Transaction;
import utils.Utils;

public class ConnectionTests
{
    public static void main(String[] args)
    {
        Utils.CliArgs parsedArgs = new Utils.CliArgs().parseCliInput(args);

        //test1();
     //   test2b();
//        test2();
        if(parsedArgs.emptyNode)
            emptyNode(parsedArgs.args,parsedArgs.localPort);
        else
            test3(parsedArgs.args, parsedArgs.localPort);
    }

    private static void emptyNode(String[] initialHosts, int localPort)
    {
        CommunicationsManager cm = new CommunicationsManager(initialHosts, new IBlocksTransactionsExchanger(){

            public boolean onReceive(ITransaction transaction)
            {
                return false;
            }

            public boolean onReceive(IBlock block)
            {
                return false;
            }

            public List<Block> getBlock(int i)
            {
                return null;
            }

            @Override
            public int getLastKnownBlockId()
            {
                return 0;
            }
        }, localPort);
        cm.start();

        System.out.println(":::::::::::::::::::::::::::::::::::::");
        System.out.println("Running emptyNode...");
        System.out.println(":::::::::::::::::::::::::::::::::::::");

        boolean stop=false;
        Scanner inputReader = new Scanner(System.in);
        while(!stop)
        {
            System.out.println("enter 'q' to close. (connectivity quality="+cm.getConnectivityQuality()/*+" suppliers = "+cm.getChosenNodes()+*/+")");
            String input = inputReader.nextLine();
            if(input.equals("q"))
                stop=true;
        }

        System.out.println("shutting down...");
        cm.stop();
        System.out.println("program ended.");
    }

    private static void printInfo(CommunicationsManager cm)
    {
        System.out.println("local port = "+cm.getLocalPort());
        List<Node> l = cm.getKnownNodes();
        System.out.println("known nodes                = "+l.size());
        l = cm.getReachableNodesList();
        System.out.println("reachable nodes            = "+l.size());
        l = cm.getNodesThatChoseUs();
        System.out.println("nodes subscribed to us     = "+l.size());
        l = cm.getNodesWeChose();
        System.out.println("nodes we are subscribed to = "+l.size());
    }

    private static void test3(String[] hosts, int port)
    {
        CommunicationsManager cm = new CommunicationsManager(hosts, new IBlocksTransactionsExchanger(){
            public boolean onReceive(ITransaction transaction)
            {
                System.out.println("received transaction "+transaction.getID());
                return true;
            }

            public boolean onReceive(IBlock block)
            {
                System.out.println("received block "+block.getID());
                return false;
            }

            public List<Block> getBlock(int i)
            {
                return null;
            }

            @Override
            public int getLastKnownBlockId()
            {
                return 0;
            }
        }, port);
        cm.start();

        boolean stop=false;
        System.out.println("CommunicationsManager is connecting...");
        try
        {
            cm.waitConnection(-1);
        }
        catch (InterruptedException e)
        {
            stop = true;
        }
        System.out.println("Connection established.");
        printInfo(cm);
        System.out.println("commands:" +
                "\n\t q = close" +
                "\n\t t = send test transaction" +
                "\n\t b = send test block" +
                "\n\t 'info' = get communications manager info" +
                "\n\t 'info-detail' = get detailed communications manager info"+
                "\n\t 'sub' = search for new subscriptions"+
                "\n\t 'rescan' = rescans the known nodes to check their reachability");
        while(!stop)
        {
            System.out.print(">");
            Scanner inputReader = new Scanner(System.in);
            String input = inputReader.nextLine();
            switch (input)
            {
                case "q":
                    stop = true;
                    break;
                case "t":
                {
                    Integer[] res = cm.spreadTransaction(new Transaction(), 100, null);
                    System.out.println("transaction sent to " + res[0] + " nodes, " + res[1] + " sends were successful");
                    break;
                }
                case "b":
                {
                    Integer[] res = cm.spreadBlock(new Block(0), 100, null);
                    System.out.println("block sent to " + res[0] + " nodes, " + res[1] + " sends were successful");
                    break;
                }
                case "sub":
                {
                    cm.updateChosenNodesAsync(0);
                    //System.out.println("block sent to " + res[0] + " nodes, " + res[1] + " sends were successful");
                    break;
                }
                case "info-detail":
                {
                    System.out.println("local port = " + cm.getLocalPort());
                    List<Node> l = cm.getKnownNodes();
                    System.out.println("" + l.size() + " known nodes");
                    System.out.println(l);
                    l = cm.getReachableNodesList();
                    System.out.println("" + l.size() + " reachable nodes");
                    System.out.println(l);
                    l = cm.getNodesThatChoseUs();
                    System.out.println("" + l.size() + " nodes subscribed to us");
                    System.out.println(l);
                    l = cm.getNodesWeChose();
                    System.out.println("" + l.size() + " nodes we are subscribed to");
                    System.out.println(l);
                    break;
                }
                case "rescan":
                {
                    cm.updateReachableNodesAsync();
                }
                case "info":
                {
                    printInfo(cm);
                    break;
                }
            }
        }

        System.out.println("shutting down...");
        cm.stop();
        System.out.println("program ended.");
    }
/*
    public static void test1()
    {
        System.out.println("ConnectionTests.main starting...");
        IncomingConnectionsHandler cw = new IncomingConnectionsHandler();
        cw.start();

        System.out.println("IncomingConnectionsHandler started on localhost");

        RemoteNode nc;
        try
        {
            nc = new RemoteNode("localhost");
        }
        catch (UnknownHostException e)
        {
            System.out.println("Unknown host, terminating");
            return;
        }

        System.out.println("asking for block to localhost");
        SuperCommand.Result r = nc.askForBlock(0);
        System.out.println("isSendSuccessful="+r.wasCommandExecutedCorrectly());
        SuperCommand.Response response = r.getResponse();
        if(response.isOk())
            System.out.println("received block: "+response.getArgs()[0]);
        else
            System.out.println("response with error: "+response.getErrorMsg());

        System.out.println("stopping IncomingConnectionsHandler");
        cw.stop();
        System.out.println("stopped. that's nice");
    }*/

/*
    public static void test2()
    {
        CommunicationsManager cm;
        try
        {
            cm = new CommunicationsManager(new ArrayList<>(Arrays.asList(new RemoteNode[]{new RemoteNode("192.168.1.7")})));
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
            return;
        }

        System.out.println("known nodes: "+cm.getKnownNodes());

        cm.start();
        System.out.println("ConnectionsManager started");
        System.out.println("known nodes: "+cm.getKnownNodes());


        cm.stop();
        System.out.println("ConnectionsManager stopped");
    }



    public static void test2b()
    {
        CommunicationsManager cm;
        try
        {
            cm = new CommunicationsManager(new ArrayList<>(Arrays.asList(new RemoteNode[]{
                    new RemoteNode("192.168.1.7"),
                    new RemoteNode("192.168.1.3")})));
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
            return;
        }

        System.out.println("known nodes num: "+cm.getKnownNodes().size());
        System.out.println("known nodes: "+cm.getKnownNodes());

        cm.start();
        System.out.println("ConnectionsManager started");
        System.out.println("known nodes: "+cm.getKnownNodes());

        for(int i=0;i<30;i++)
        {
            System.out.println(""+i);
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
                break;
            }
        }

        cm.stop();
        System.out.println("ConnectionsManager stopped");
    }
<<<<<<< HEAD
=======

>>>>>>> 2e2c55224f49f18225dd823eead635df87b187a3
*/

}
