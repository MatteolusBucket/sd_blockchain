package communications;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import utils.Commons;

import java.net.*;
import java.util.List;


/**
 * Class representing a remote serverNode
 */
public class RemoteNode implements Comparable<RemoteNode>
{
    private Node node;

    private int connectionTimeoutsCount;
    private int connectionEstablishedCount;
    private int incomingRequestsCount;
    private long lastTimeConnectedMillis;
    private long lastTimeReceivedRequestMillis;
    private long creationTime;
    private long lastTimeConnectionFailedMillis;
    private long estimatedRttNano=-1;

    static final long serialVersionUID = 1;

    public RemoteNode(String hostname, int port) throws UnknownHostException
    {
        this(new Node(hostname,port));
    }

    public RemoteNode(Node node)
    {
        this.node = new Node(node.getInetAddress(), node.getPort());
        creationTime = System.currentTimeMillis();
    }

    public RemoteNode(InetAddress hostname, int port)
    {
       this(new Node(hostname, port));
    }

    private void noteSendFailed(Dialogue.Result result)
    {
        if(result.getFailReason()==CommunicationErrors.FailReason.TIMEOUT_ESTABLISHING_CONNECTION)
        {
            //this means the connection with the serverNode couldn't be established. It could be because of various things:
            // - The remote host doesn't have this program running, but the firewall is configured to freeConnection
            // - The remote host has our program running but it is behind an annoying router that is blocking incoming connections (uff)
            connectionTimeoutsCount++;
        }
    }

    private void noteSendExecutedSuccessfully()
    {
        connectionEstablishedCount++;
    }

    void noteIncomingRequestReceived()
    {
        incomingRequestsCount++;
        lastTimeReceivedRequestMillis = System.currentTimeMillis();
    }


    long getLastContactTime()
    {
        if(lastTimeReceivedRequestMillis>lastTimeConnectedMillis)
            return (creationTime>lastTimeReceivedRequestMillis)?creationTime:lastTimeReceivedRequestMillis;
        else
            return (creationTime>lastTimeConnectedMillis)?creationTime:lastTimeConnectedMillis;
    }



    public boolean sendTransaction(ITransaction transaction, int timeToLive, int senderPort, CommunicationsManager cm)
    {
        SuperCommand cmd =new CommandSpreadTransaction(transaction,timeToLive);
        SuperCommand.CommandReturn res = sendCommand(cmd,cm);
        if(res==null)
            return false;
        else
            return res.isOk();
    }

    public boolean sendBlock(IBlock block, int timeToLive, int senderPort, CommunicationsManager cm)
    {
        SuperCommand cmd =new CommandSpreadBlock(block, timeToLive);
        SuperCommand.CommandReturn res = sendCommand(cmd,cm);
        if(res==null)
            return false;
        else
            return res.isOk();
    }

    public IBlock askForBlock(int blockId, int senderPort, CommunicationsManager cm)
    {
        CommandAskBlock cmd = new CommandAskBlock(blockId);
        SuperCommand.CommandReturn res = sendCommand(cmd, cm);
        if(res!=null && res.isOk())
            return (IBlock) cmd.getResponseArgsChecking(res);
        else
            return null;
    }


    /**
     * Performs a pingNano (not an ICMP pingNano, it is performed over the sockets layer)
     * @return negative value in case of errors, the pingNano in nanoseconds if all is good
     */
    public long pingNano(int senderPort, CommunicationsManager cm)
    {
        CommandPing cmd = new CommandPing();
        long t0 = System.nanoTime();
        SuperCommand.CommandReturn res = sendCommand(cmd,1000,20_000, cm,true);
        long t1 = System.nanoTime();
        long duration = t1-t0;
        if(res!=null && res.isOk())
            return duration;
        else
            return -1;
    }

    public boolean testMyConnection(int senderPort, CommunicationsManager cm)
    {
        CommandTestMyConnection cmd = new CommandTestMyConnection();
        SuperCommand.CommandReturn res = sendCommand(cmd, cm);
        if(res!=null && res.isOk())
        {
            return (Boolean) cmd.getResponseArgsChecking(res);
        }
        else
        {
            return false;
        }
    }

    List<RemoteNode> askForNeighbours(int senderPort, CommunicationsManager cm)
    {
        CommandAskNeighbours cmd = new CommandAskNeighbours();
        SuperCommand.CommandReturn res = sendCommand(cmd, cm);
        if(res!=null && res.isOk())
            return (List<RemoteNode>) cmd.getResponseArgsChecking(res);
        else
            return null;
    }


    RemoteNode estimateRoundTripTime(int senderPort, CommunicationsManager cm)
    {
        long pingSum=0;
        int validSamples=0;
        for(int i=0;i<5 && validSamples<1;i++)
        {
            long ping = pingNano(senderPort, cm);
            //Commons.logger.info("pingNanoCompleted");
            if(ping>0)
            {
                pingSum+=ping;
                validSamples++;
            }
        }
        if(validSamples>0)
            estimatedRttNano = pingSum/validSamples;
        else
            estimatedRttNano = -1;
        Commons.logger.fine("estimated ping for "+this+" = "+estimatedRttNano);
        return this;
    }


    void setReachable()
    {
        if(estimatedRttNano<0)
            estimatedRttNano=Integer.MAX_VALUE;
    }
    /**
     * Starts the provided dialogue with this serverNode
     * @param dialogue The dialogue to start
     * @param connectionTimeout Timeout for establishing the connection
     * @param responseTimeout Timeout for each response coming from the remote serverNode during the dialogue
     * @param cm The communication manager
     * @return
     */
    public Dialogue.Result startDialogue(Dialogue dialogue, int connectionTimeout, int responseTimeout, CommunicationsManager cm)
    {
        return startDialogue(dialogue,connectionTimeout,responseTimeout,cm,false);
    }
    public Dialogue.Result startDialogue(Dialogue dialogue, int connectionTimeout, int responseTimeout, CommunicationsManager cm, boolean dontPrintErrors)
    {
        Dialogue.Result res = dialogue.start(getNode(),connectionTimeout,responseTimeout,cm, dontPrintErrors);
        if(res.wasCommandExecutedCorrectly())
        {
            noteSendExecutedSuccessfully();
            lastTimeConnectedMillis = System.currentTimeMillis();
        }
        else
        {
            noteSendFailed(res);
            lastTimeConnectionFailedMillis = System.currentTimeMillis();
        }
        Commons.logger.fine("dialogue completed");
        return res;
    }



    @Override
    public String toString()
    {
        return node.getInetAddress().toString()+":"+node.getPort();
    }

    @Override
    public boolean equals(Object o)
    {
        boolean ret = (o instanceof RemoteNode) &&
                node.getInetAddress().equals(((RemoteNode)o).node.getInetAddress()) &&
                node.getPort()==((RemoteNode)o).node.getPort();
        //Commons.logger.info("checking for equality between "+this+" and "+o+", result is "+ret);
        return  ret;
    }


    @Override
    public int compareTo(RemoteNode remoteNode)
    {
        return getNode().compareTo(remoteNode.getNode());
    }


    public Node getNode()
    {
        return node;
    }


    /**
     * Estimated round trip time, if it hasn't been estimated yet it returns -1
     * @return
     */
    public long getEstimatedRttNano()
    {
        return estimatedRttNano;
    }



    private SuperCommand.CommandReturn sendCommand(SuperCommand cmd, CommunicationsManager cm)
    {
        return sendCommand(cmd,10_000,20_000, cm, false);
    }

    private SuperCommand.CommandReturn sendCommand(SuperCommand cmd, int connectionTimeout, int dialogueTimeout, CommunicationsManager cm, boolean dontPrintErrors)
    {
        Commons.logger.fine("sending command "+cmd.getClass().getSimpleName()+" to "+this);
        DialogueCommand diagCmd = new DialogueCommand(cm.getLocalPort(),cmd);
        Dialogue.Result result = startDialogue(diagCmd, connectionTimeout, dialogueTimeout,cm , dontPrintErrors);
        Commons.logger.fine("done dialogue for command "+cmd.getClass().getSimpleName());
        if(result.wasCommandExecutedCorrectly())
        {
            SuperCommand.CommandReturn ret = result.getResponse();
            Commons.logger.fine("received dialogueCommand response for cmd "+cmd.getClass().getSimpleName());
            return ret;
        }
        else
        {
            if(!dontPrintErrors)
                Commons.logger.warning("["+this+"] dialogue execution failed: "+result.getFailReason());
            return null;
        }
    }
}
