package communications;

import utils.Commons;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;
import java.util.function.Consumer;

public class CommUtils
{
    public static int unsignedToBytes(byte b)
    {
        return b & 0xFF;
    }

    public static String unsignedByteArrayToString(byte[] arr)
    {
        String r ="[";
        for(int i=0;i<arr.length;i++)
        {
            r+=unsignedToBytes(arr[i]);
            if(i<arr.length-1)
                r+=",";
        }
        return r+"]";
    }
    /**
     * Gets the subnet address of the provided ip address given  the network prefix length (the number of ones in the subnet mask)
     * @param addr the ipv4 address (must have length==4)
     * @param networkPrefixLength the number of ones in the subnet mask
     * @return the subnet address
     */
    public static byte[] getSubnetAddr(byte[] addr, int networkPrefixLength)
    {
        if(addr.length!=4)
            throw new IllegalArgumentException("invalid address, provided address has length "+addr.length+", should be 4");
        if(networkPrefixLength>32)
            throw new IllegalArgumentException("invalid networkPrefixLength, provided networkPrefixLength is "+networkPrefixLength+", should at max 32");
        //  Commons.logger.warning("getting subnet address for "+ unsignedByteArrayToString(addr)+" with prefixLength "+networkPrefixLength);
        byte[] subnetMask = new byte[]{0,0,0,0};
        int fullOctets = networkPrefixLength/8;
        for(int i=0;i<fullOctets-1;i++)
            subnetMask[i]=(byte)255;
        for(int i=0;i<networkPrefixLength-fullOctets*8;i++)
            subnetMask[fullOctets] |= 1>>i;

        byte[] subnetAddr = new byte[]{0,0,0,0};
        for(int i=0;i<4;i++)
        {
            subnetAddr[i]=(byte)(addr[i] & subnetMask[i]);
        }

        return subnetAddr;
    }

    /**
     * Gets the subnet address associated with the provided address, selecting from the different subnets we are connected to
     * @param addr the ipv4 address (must have length==4)
     * @return the subnet in which the address is
     */
    public static byte[] getSubnet(byte[] addr)
    {
        List<InterfaceAddress> interfaceAddresses = new ArrayList<>();
        try
        {
            for (Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces(); networkInterfaces.hasMoreElements(); )
            {
                NetworkInterface ni = networkInterfaces.nextElement();
                interfaceAddresses.addAll(ni.getInterfaceAddresses());
            }
        }
        catch(SocketException e)
        {
            Commons.logger.severe("Couldn't get network interfaces to determine localhost addresses");
        }

        int longestMatchLength=0;
        byte[] bestMatchingSubnet=null;
        for(InterfaceAddress interfaceAddress : interfaceAddresses)
        {
            if(interfaceAddress.getAddress().getAddress().length>4)
            {
                continue;//not an ipv4 address
            }
            byte[] interfaceSubnet = getSubnetAddr(interfaceAddress.getAddress().getAddress(), interfaceAddress.getNetworkPrefixLength());
            byte[] addrSubnet = getSubnetAddr(addr, interfaceAddress.getNetworkPrefixLength());

            boolean isAMatch = true;
            for(int i=0;i<4;i++)
            {
                if(interfaceSubnet[i]!=addrSubnet[i])
                    isAMatch=false;
            }
            if(isAMatch && longestMatchLength<interfaceAddress.getNetworkPrefixLength())
            {
                longestMatchLength=interfaceAddress.getNetworkPrefixLength();
                bestMatchingSubnet = interfaceSubnet;
            }
        }
        return bestMatchingSubnet;
    }

    /**
     * Checks if the two addresses are in the same subnet from the point of view of this computer. (gets the subnets usign {@link #getSubnet(byte[])}
     * @param addr1 ipv4 address (must have length==4)
     * @param addr2 ipv4 address (must have length==4)
     * @return true if the two addresses are in the same subnet
     */
    public static boolean areInSameSubnet(InetAddress addr1, InetAddress addr2)
    {
        byte[] subnet1 = getSubnet(addr1.getAddress());
        byte[] subnet2 = getSubnet(addr2.getAddress());
        for(int i=0;i<4;i++)
        {
            if(subnet1[i]!=subnet2[i])
                return false;
        }
        return true;
    }

    static void runInParallel(Consumer<Object> job, int maxThreadNumber, NavigableMap<Node, ?> nodes)
    {
        final StackTraceElement caller = Thread.currentThread().getStackTrace()[2];
        //Commons.logger.info("runInParallel (from "+caller+")");
        if(nodes.size()==0)
            return;
        NodeDispenser rnd = new NodeDispenser(nodes);
        int threadsNum = Math.min(nodes.size(), maxThreadNumber);
        //Commons.logger.info("threadsNum="+threadsNum);
        List<Thread> threads = new ArrayList<>(threadsNum);
        for(int i=0;i<threadsNum;i++)
        {
            threads.add(new Thread(()->{
                //Commons.logger.info("running thread... (from "+caller+")");
                Object n = rnd.getNode();
                //Commons.logger.info("n="+n);
                while(n!=null && !Thread.currentThread().isInterrupted())
                {
                    //Commons.logger.info("running accept...");
                    job.accept(n);//do the job on this serverNode
                    n = rnd.getNode();
                }
            }));
        }
        for(Thread t : threads)
            t.start();
        while(threads.size()>0)
        {
            for(int i=0;i<threads.size();i++)
            {
                Thread t = threads.get(i);
                if(!t.isAlive())
                {
                    threads.remove(i);
                }
                else
                {
                    try
                    {
                        t.join();
                        threads.remove(i);
                    }
                    catch (InterruptedException e)
                    {
                        t.interrupt();
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
    }

    static class NodeDispenser
    {
        NavigableMap<Node, ?> nodes;
        Node lastUsedKey;
        boolean finished=false;
        public NodeDispenser(NavigableMap<Node, ?> nodes)
        {
            this.nodes=nodes;
        }

        public synchronized Object getNode()
        {
            if(finished)
                return null;
            Node node;
            if(lastUsedKey ==null)
            {
                if(nodes.size()>0)
                    node=nodes.firstKey();
                else
                    node=null;
            }
            else
            {
                node=nodes.higherKey(lastUsedKey);
            }
            lastUsedKey =node;
            if(lastUsedKey ==null)
            {
                finished=true;
                return null;
            }
            return nodes.get(lastUsedKey);
        }
    }
}
