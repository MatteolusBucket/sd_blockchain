package test;

import miner.MinerApp;
import org.apache.commons.lang3.StringUtils;
import utils.Commons;
import utils.Utils;
import wallet.WalletApp;

import java.io.File;
import java.util.List;

/**
 * This class creates applications
 */
public class AppsCreator extends Thread
{
    private List<WalletApp> walletApps;
    private List<MinerApp> minerApps;
    private int numberApps;
    private int appsStartListeningPort;
    private String type = "";
    private int appsBridgePort;
    private String pathDirectory = ".";
    private String fileNameBlockChain = "blockchain.data";
    private String fileNameConfig = "config.data";

    /**
     * Constructor of the class
     * @param minerApps is the list of miners
     * @param numberApps is the number of miners
     * @param appsStartListeningPort is the starting port used by the first miner
     */
    public AppsCreator(List minerApps, int numberApps, int appsStartListeningPort)
    {
        this.minerApps = minerApps;
        this.numberApps = numberApps;
        this.appsStartListeningPort = appsStartListeningPort;
    }

    /**
     * Constructor of the class
     * @param walletApps is the list of wallets
     * @param minerApps is the list of miners
     * @param numberWalletApps is the number of wallets
     * @param appsStartListeningPort is the starting port used by the first wallet
     * @param appsBridgePort is the port used to connect miners and wallets
     */
    public AppsCreator(List walletApps, List minerApps, int numberWalletApps, int appsStartListeningPort, int appsBridgePort)
    {
        this(minerApps, numberWalletApps, appsStartListeningPort);
        this.appsBridgePort = appsBridgePort;
        this.walletApps = walletApps;
    }

    /**
     * This function sets type = m
     * @return the {@link AppsCreator} instance
     */
    public AppsCreator createMiners()
    {
        type = "m";

        return this;
    }

    /**
     * This function sets type = w
     * @return the {@link AppsCreator} instance
     */
    public AppsCreator createWallets()
    {
        type = "w";

        return this;
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * This function creates miners and wallets connected together
     */
    public void run()
    {
        StringBuilder stringBuilderArgs = new StringBuilder();
        Utils.CliArgs parsedArgs;
        File directory;
        boolean sameDirectory = true;

        switch(type)
        {
            case "m":
            {
                for(int i = 0; i < numberApps; i++)
                {
                    // clean the stringBuilder
                    stringBuilderArgs.delete(0, stringBuilderArgs.length());

                    // create the args used to connect the miner
                    if(i > 0)
                    {
                        stringBuilderArgs.append("localhost:" + (appsStartListeningPort + i - 1));
                    }
                    else
                    {
                         stringBuilderArgs.append(" ");
                    }

                    // for each miner we create a directory where we'll store da local data
                    directory = new File(pathDirectory + "/" + "m" + i);
                    if(!createNewEmptyDirectory(directory))
                    {
                        break;
                    }

                    parsedArgs = new Utils.CliArgs().parseCliInput(StringUtils.split(stringBuilderArgs.toString(), " "));
                    minerApps.add(new MinerApp(parsedArgs.args, appsStartListeningPort + i, null, pathDirectory + "/m" + i, fileNameBlockChain, pathDirectory + "/m" + i, fileNameConfig));
                    minerApps.get(i).start();

                    try
                    {
                        while(!minerApps.get(i).isAlive())
                        {
                            Thread.sleep(100);
                        }
                    }
                    catch(InterruptedException e)
                    {
                        Commons.logger.warning("Interrupt in AppsCreator[m]");
                    }
                }
            } break;
            case "w":
            {
                // we create some wallets with the same address of miners -> in this manner we can re-use the money
                for(int i = 0; i < numberApps; i++)
                {
                    stringBuilderArgs.delete(0, stringBuilderArgs.length());

                    if(i == 0 && minerApps.size() > 0)
                    {
                        stringBuilderArgs.append("localhost:" + appsBridgePort + " -a " + minerApps.get(i).getAddress());
                    }
                    else if(i <= (minerApps.size() / 2) && minerApps.size() > 0)
                    {
                        stringBuilderArgs.append("localhost:" + (appsStartListeningPort + i - 1) + " -a " + minerApps.get(i).getAddress());
                    }
                    else
                    {
                        stringBuilderArgs.append("localhost:" + (appsStartListeningPort + i - 1));
                        sameDirectory = false;
                    }

                    directory = new File(pathDirectory + "/" + "w" + i);
                    if(!createNewEmptyDirectory(directory))
                    {
                        break;
                    }

                    parsedArgs = new Utils.CliArgs().parseCliInput(StringUtils.split(stringBuilderArgs.toString(), " "));
                    walletApps.add(new WalletApp(parsedArgs.args, appsStartListeningPort + i, parsedArgs.address, pathDirectory + "/w" + i, fileNameBlockChain, pathDirectory + (sameDirectory ? "/m" : "/w") + i, fileNameConfig));
                    walletApps.get(i).start();

                    try
                    {
                        while(!walletApps.get(i).isAlive())
                        {
                            Thread.sleep(100);
                        }
                    }
                    catch(InterruptedException e)
                    {
                        Commons.logger.warning("Interrupt in AppsCreator[w]");
                    }
                }

            } break;
        }
    }

    /**
     * This function creates or clean a directory
     * @param directory to manage
     * @return true or false
     */
    private boolean createNewEmptyDirectory(File directory)
    {
        if(!directory.isDirectory())
        {
            if(!directory.mkdirs())
            {
                Commons.logger.severe("Can't create the miner test data directory");
                return false;
            }
        }
        else
        {
            // if already exists -> clean the directory
            for(File f : directory.listFiles())
            {
                f.delete();
            }
        }

        return true;
    }
}
