package test;

import miner.MinerApp;
import utils.Commons;
import utils.Security;
import utils.Utils;
import wallet.WalletApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

public class Test
{
    /**
     * This is the main function of the test class
     * @param args used to start the test
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException
    {
        ParserArgs parserArgs = new ParserArgs().parse(args);
        List<MinerApp> minerAppList = new ArrayList<>();
        List<WalletApp> walletAppList = new ArrayList<>();

        Commons.setLoggerLevel(Level.FINEST);

        // creation of wallets and miners
        AppsCreator appsCreatorMiners = new AppsCreator(minerAppList, parserArgs.numberMiners, parserArgs.minersStartListeningPort).createMiners();
        AppsCreator appsCreatorWallets = new AppsCreator(walletAppList, minerAppList, parserArgs.numberWallets, parserArgs.walletsStartListeningPort, parserArgs.minersStartListeningPort).createWallets();
        appsCreatorMiners.start();
        appsCreatorMiners.join();

        Thread.sleep(1000);

        appsCreatorWallets.start();
        appsCreatorWallets.join();

        Thread.sleep(1000);

        //Begins test random transactions
        testRandomTransaction(walletAppList);
    }

    /**
     * This function creates a test with random transactions between wallets
     * @param walletAppList is the wallets list
     * @throws InterruptedException
     */
    public static void testRandomTransaction(List<WalletApp> walletAppList) throws InterruptedException
    {
        int walletChosen = 0;
        float expendableAmount, amount;
        Utils utils = new Utils();
        WalletApp walletApp;

        while(walletAppList.size() > 0)
        {
            if(walletChosen == 0)
            {
                utils.printCenterConsole("", "*");
                utils.printCenterConsole("", "*");
            }

            walletApp = walletAppList.get(walletChosen);

            expendableAmount = walletApp.getExpendableAmount();
            amount = walletApp.getAmount();

            System.out.println("\nWallet address: " + walletApp.getAddress());
            System.out.println("Wallet public key: " + walletApp.getPublicKey());
            System.out.println("Expendable amount: " + expendableAmount);
            System.out.println("Amount: " + amount + "\n");

            // create transaction
            randomTransaction(walletApp, walletAppList);

            Thread.sleep(1500);

            walletChosen = ++walletChosen % walletAppList.size();

            if(walletChosen == 0)
            {
                utils.printCenterConsole("", "*");
                utils.printCenterConsole("", "*");

                Thread.sleep(10000);
            }
        }
    }

    /**
     * This function creates a random transaction
     * @param wallet is the sender
     * @param walletAppList is the wallets list
     */
    private static void randomTransaction(WalletApp wallet, List<WalletApp> walletAppList)
    {
        Random rand = new Random();
        List<Integer> usedAddresses = new ArrayList<>();
        int maxAttempt = 5;
        float expendableAmount = wallet.getExpendableAmount();

        // foreach wallet we used at most maxAttempt input
        while(maxAttempt > 0)
        {
            if(expendableAmount > wallet.TRANSACTION_FEE)
            {
                int addressReceiver = rand.nextInt(walletAppList.size());

                // don't send money to himself
                if(!walletAppList.get(addressReceiver).getAddress().equals(wallet.getAddress()))
                {
                    // foreach output only one time the same address
                    if(!usedAddresses.contains(addressReceiver))
                    {
                        // calculate a coherent payment amount
                        float payment = rand.nextFloat() * (expendableAmount - wallet.TRANSACTION_FEE);
                        while(expendableAmount - payment < wallet.TRANSACTION_FEE)
                        {
                            payment = rand.nextFloat() * (expendableAmount - wallet.TRANSACTION_FEE);

                            Commons.logger.severe(expendableAmount + " DEADLOCK small amount: " + payment);
                        }

                        System.out.println("Payment -> Attempt: " + maxAttempt + " ### Payment: " + payment + " ### RECEIVER: " + walletAppList.get(addressReceiver).getAddress());

                        usedAddresses.add(addressReceiver);
                        if(wallet.addPaymentReceiver(payment, walletAppList.get(addressReceiver).getAddress(), Security.getPublicKeyFromString(walletAppList.get(addressReceiver).getPublicKey())))
                        {
                            Commons.logger.info("ADD OK");
                        }
                        else
                        {
                            Commons.logger.info("ADD NO");
                        }
                    }
                }
            }

            expendableAmount = wallet.getExpendableAmount();
            maxAttempt--;
        }

        // send the transaction
        wallet.sendPayment();
    }

    /**
     * This class is used to parse the args of the test program
     * Usage e.g.: -m 10 -mp 5000 -w 20 -wp 6000
     */
    public static class ParserArgs
    {
        private static final int DEFAULT_MINERS_START_LISTENING_PORT = 3000;
        private static final int DEFAULT_WALLETS_START_LISTENING_PORT = 6000;
        private int numberMiners;
        private int numberWallets;
        private int minersStartListeningPort = DEFAULT_MINERS_START_LISTENING_PORT;
        private int walletsStartListeningPort = DEFAULT_WALLETS_START_LISTENING_PORT;

        /**
         * Constructor of the class
         * @param args used by the parser
         * @return the instance of the parser {@link ParserArgs}
         */
        public ParserArgs parse(String[] args)
        {
            List<String> argsList = new ArrayList<>(Arrays.asList(args));
            String param;

            for(int i = 0; i < argsList.size(); i++)
            {
                param = argsList.get(i);

                switch(param)
                {
                    case "-m":
                    {
                        if (argsList.size() <= i + 1)
                        {
                            System.out.println("Invalid syntax, missing port number after -m parameter");
                            System.exit(-1);
                        }
                        try
                        {
                            numberMiners = Integer.parseInt(argsList.get(i + 1));
                        }
                        catch (NumberFormatException e)
                        {
                            numberMiners = 1;
                        }
                    } break;
                    case "-w": {
                        if (argsList.size() <= i + 1)
                        {
                            System.out.println("Invalid syntax, missing port number after -w parameter");
                            System.exit(-1);
                        }
                        try
                        {
                            numberWallets = Integer.parseInt(argsList.get(i + 1));
                        }
                        catch (NumberFormatException e)
                        {
                            numberWallets = 1;
                        }
                    } break;
                    case "-mp": {
                        if (argsList.size() <= i + 1)
                        {
                            System.out.println("Invalid syntax, missing miner start listening port number after -mp parameter");
                            System.exit(-1);
                        }
                        try
                        {
                            minersStartListeningPort = Integer.parseInt(argsList.get(i + 1));
                        }
                        catch (NumberFormatException e)
                        {
                            minersStartListeningPort = DEFAULT_MINERS_START_LISTENING_PORT;
                        }
                    } break;
                    case "-wp": {
                        if (argsList.size() <= i + 1)
                        {
                            System.out.println("Invalid syntax, missing miner start listening port number after -wp parameter");
                            System.exit(-1);
                        }
                        try
                        {
                            walletsStartListeningPort = Integer.parseInt(argsList.get(i + 1));
                        }
                        catch (NumberFormatException e)
                        {
                            walletsStartListeningPort = DEFAULT_WALLETS_START_LISTENING_PORT;
                        }
                    } break;
                }

                argsList.remove(i--);
            }

            return this;
        }
    }
}
