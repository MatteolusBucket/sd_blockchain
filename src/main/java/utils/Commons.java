package utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * In this class we have the logger of the project
 */
public class Commons
{
    public static final int LENGTH_HASH = 64;

    /*
    The levels in descending order are:
    SEVERE (highest value)
    WARNING
    INFO
    CONFIG
    FINE
    FINER
    FINEST (lowest value)
     */
    public static Logger logger;

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format","[%1$tF_%1$tT\t%4$s %2$s]\t%5$s%6$s%n");
        logger = Logger.getLogger("sd_blockchain");
        setLoggerLevel(Level.SEVERE);
    }

    /**
     * This function sets the logger leevel
     * @param level is the value to set
     */
    public static void setLoggerLevel(Level level)
    {
        logger.setLevel(level);
    }
}
