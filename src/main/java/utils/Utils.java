package utils;

import communications.CommunicationsManager;
import org.apache.commons.lang3.StringUtils;
import test.Test;
import utils.fields.Address;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * In this class we have some commons utils used in the project
 */
public class Utils
{
    private int sizeConsole = -1;

    /**
     * @return the size of the console (Only the columns)
     */
    private int sizeConsole()
    {
        InputStream inputStream;
        int c;
        StringBuilder str = new StringBuilder();

        try
        {
            // execute a bash command
            inputStream = Runtime.getRuntime().exec(new String[] {
                    "bash", "-c", "tput cols 2> /dev/tty" }).getInputStream();

            while((c = inputStream.read()) != -1 && c  != (int)'\n')
            {
                str.append((char)c);
            }
        }
        catch(IOException e)
        {
            Commons.logger.warning("Error during the check of the console size");
        }

        try
        {
            c = Integer.parseInt(str.toString());
        }
        catch(NumberFormatException e)
        {
            c = 100;
        }

        return c;
    }

    /**
     * This function prints a string in the center of the console
     * @param str
     * @param padding
     */
    public void printCenterConsole(String str, String padding)
    {
        if(sizeConsole == -1)
        {
            sizeConsole = sizeConsole();
        }

        System.out.println(StringUtils.center(str, sizeConsole, padding));
    }

    /**
     * This class is used to parse the args of the program
     * Usage e.g.: -m -p 4000 -a <address> -v
     */
    public static class CliArgs
    {
        public int localPort;
        public boolean emptyNode;
        public boolean launchWallet;
        public boolean launchMiner;
        public String[] args;
        public Address address;

        /**
         * @param args used by the parser
         * @return the instance of the parser {@link CliArgs}
         */
        public CliArgs parseCliInput(String[] args)
        {
            List<String> argsList = new ArrayList<>(Arrays.asList(args));

            localPort = -1;
            for (int i = 0; i < argsList.size(); i++)
            {
                String par = argsList.get(i);
                if(par.equals("-v")) // verbose mode
                {
                    Commons.setLoggerLevel(Level.FINEST);
                    argsList.remove(i--);
                } else if (par.equals("-p")) // set the listening port
                {
                    if (argsList.size() <= i + 1)
                    {
                        System.out.println("Invalid syntax, missing port number after -p parameter");
                        System.exit(-1);
                    }
                    try
                    {
                        localPort = Integer.parseInt(argsList.get(i + 1));
                    }
                    catch (NumberFormatException e)
                    {
                        localPort = CommunicationsManager.DEFAULT_PORT;
                    }
                    if (localPort < 1 || localPort > 65535)
                    {
                        System.out.println("Invalid port number " + argsList.get(i + 1));
                        System.exit(-2);
                    }
                    argsList.remove(i + 1);
                    argsList.remove(i);
                    i--;
                }
                else if(par.equals("--empty-node"))
                {
                    emptyNode = true;
                    argsList.remove(i--);
                }
                else if (par.equals("--help")) // print help menu
                {
                    System.out.println("command [options] [hostname[:port] ...]");
                    System.out.println(" Options:");
                    System.out.println("   -p <portNumber>");
                    System.out.println("      Specifies the local port to be used");
                    System.out.println("   -v");
                    System.out.println("      Verbose mode");
                    System.out.println("   --help");
                    System.out.println("      This help screen");
                    System.out.println("   -w");
                    System.out.println("      Launches the wallet");
                    System.out.println("   -m");
                    System.out.println("      Launches the miner");
                    System.out.println("   -a");
                    System.out.println("      Specifies the address");
                    System.exit(1);
                }
                else if(par.equals("-w")) // start a WalletApp
                {
                    launchWallet=true;
                    argsList.remove(i--);
                }
                else if(par.equals("-m")) // start a MinerApp
                {
                    launchMiner=true;
                    argsList.remove(i--);
                }
                else if(par.equals("-a")) // set the address
                {
                    if (argsList.size() <= i + 1)
                    {
                        System.out.println("Invalid syntax, missing address after -a parameter");
                        System.exit(-1);
                    }

                    String a = argsList.get(i + 1);

                    if(a.length() != Commons.LENGTH_HASH)
                    {
                        System.out.println("Invalid address length");
                        System.exit(-1);
                    }

                    address = new Address(a);
                    argsList.remove(i + 1);
                    argsList.remove(i--);
                }
            }

            this.args = argsList.toArray(new String[0]);
            return this;
        }
    }
}
