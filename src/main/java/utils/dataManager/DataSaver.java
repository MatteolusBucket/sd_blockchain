package utils.dataManager;

import blockchain.BlockChain;
import com.google.gson.*;
import utils.Commons;
import utils.Security;
import utils.fields.Address;

import java.io.*;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * This class is used to save data in Json
 */
public class DataSaver
{
    private static Gson gson;
    private String pathDirectoryBlockChain;
    private String fileNameBlockChain;
    private String pathDirectoryConfig;
    private String fileNameConfig;

    /**
     * Constructor of the class
     * @param pathDirectoryBlockChain is the path of the blockchain file
     * @param fileNameBlockChain is the filename of the blockchain file
     * @param pathDirectoryConfig is the path of the config file
     * @param fileNameConfig is the filename of the config file
     */
    public DataSaver(String pathDirectoryBlockChain, String fileNameBlockChain, String pathDirectoryConfig, String fileNameConfig)
    {
        // create a Gson instance specifying enableComplexMapKeySerialization -> used to save correctly the tree in our project
        gson = new GsonBuilder().enableComplexMapKeySerialization().create();

        this.pathDirectoryBlockChain = pathDirectoryBlockChain;
        this.fileNameBlockChain = fileNameBlockChain;
        this.pathDirectoryConfig = pathDirectoryConfig;
        this.fileNameConfig = fileNameConfig;
    }

    /**
     * This function saves the blockchain
     * @param blockChain is the object to save
     * @return true or false
     */
    public boolean saveBlockChain(BlockChain blockChain)
    {
        // lock the chain before the save operation
        synchronized(blockChain)
        {
            return saveData(blockChain, pathDirectoryBlockChain, fileNameBlockChain, "blockchain");
        }
    }

    /**
     * @return the blockchain stored in the local file or null whether something goes wrong
     */
    protected BlockChain loadBlockChain()
    {
        return (BlockChain)loadData(BlockChain.class, pathDirectoryBlockChain, fileNameBlockChain, "Blockchain");
    }

    /**
     * This function saves the keys in the config file
     * @param address of the user
     * @param keys to save
     * @return true or false
     */
    protected boolean saveKeys(Address address, KeyPair keys)
    {
        // we create a file named: config.data_<address>
        return saveData(keys, pathDirectoryConfig, fileNameConfig + "_" + address, "miner or wallet");
    }

    /**
     * This function loads the keys from the local config file
     * @param address of the user
     * @return a pair of keys {@link KeyPair}
     */
    protected KeyPair loadKeys(Address address)
    {
        return (KeyPair)loadData(KeyPair.class, pathDirectoryConfig, fileNameConfig + "_" + address, "MinerApp or WalletApp");
    }

    /**
     * This function loads a generic object from a json file
     * @param objectClass class type of the object
     * @param pathDirectory is the path where the file is stored
     * @param fileName is the filename
     * @param packageName string used to print a debug message
     * @return the instance of the loaded onject
     */
    private Object loadData(Class objectClass, String pathDirectory, String fileName, String packageName)
    {
        FileReader fileReader;
        BufferedReader bufferedReader;
        String json;
        Object object = null;

        try
        {
            // open the file
            fileReader = new FileReader(pathDirectory + "/" + fileName);
            bufferedReader = new BufferedReader(fileReader);

            // theoretically we have only 1 line
            while ((json = bufferedReader.readLine()) != null)
            {
                object = gson.fromJson(json, objectClass);
            }
        }
        catch(IOException e)
        {
            Commons.logger.info(packageName + " data file is missing..");
        }

        return object;
    }

    /**
     * This function saves a generic object in a json file
     * @param dataToSave is the object to save
     * @param pathDirectory is the path where store the file
     * @param fileName is the filename
     * @param packageName string used to print a debug message
     * @return true or false
     */
    private boolean saveData(Object dataToSave, String pathDirectory, String fileName, String packageName)
    {
        File directory = new File(pathDirectory);
        FileWriter fileWriter;

        // open or create the directory
        if(!directory.isDirectory())
        {
            if(! directory.mkdirs())
            {
                Commons.logger.severe("Can't create the " + packageName + " data directory");
                return false;
            }
        }

        try
        {
            // create the file
            fileWriter = new FileWriter(pathDirectory + "/" + fileName);
            fileWriter.write(gson.toJson(dataToSave));
            fileWriter.close();

            return true;
        }
        catch(IOException e)
        {
            Commons.logger.severe("Can't create the " + packageName + " data file");
        }

        return false;
    }

    /**
     * This class represents a pair of keys
     */
    protected class KeyPair
    {
        private String publicKey;
        private String privateKey;

        /**
         * Constructor of the class
         * @param publicKey is the key to save
         * @param privateKey is the key to save
         */
        public KeyPair(PublicKey publicKey, PrivateKey privateKey)
        {
            this.publicKey = Security.getStringFromPublicKey(publicKey);
            this.privateKey = Security.getStringFromPrivateKey(privateKey);
        }

        /**
         * @return the public key
         */
        public PublicKey getPublicKey()
        {
            return Security.getPublicKeyFromString(publicKey);
        }

        /**
         * @return the private key
         */
        public PrivateKey getPrivateKey()
        {
            return Security.getPrivateKeyFromString(privateKey);
        }
    }
}
