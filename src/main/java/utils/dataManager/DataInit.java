package utils.dataManager;

import blockchain.BlockChain;
import utils.AddressMemory;
import utils.Commons;
import utils.Security;
import utils.fields.Address;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.UUID;

/**
 * This class is used to load the blockchain, the keys and address
 */
public class DataInit extends DataSaver
{
    private KeyPair keys;
    private Address address;

    /**
     * Constructor of the class
     * @param pathDirectoryBlockChain is the location of the local blockchain file
     * @param fileNameBlockChain is the filename of the blockchain
     * @param pathDirectoryConfig is the location of the config file
     * @param fileNameConfig is the filename of the config
     * @param address of the {@link miner.MinerApp} of {@link wallet.WalletApp}
     */
    public DataInit(String pathDirectoryBlockChain, String fileNameBlockChain, String pathDirectoryConfig, String fileNameConfig, Address address)
    {
        super(pathDirectoryBlockChain, fileNameBlockChain, pathDirectoryConfig, fileNameConfig);

        // in this case we create a new address for the user
        if(address == null)
        {
            // to do that we use the UUID class
            this.address = new Address(UUID.randomUUID());
        }
        else
        {
            this.address = address;
        }

        keys = super.loadKeys(this.address);

        // in this case we don't have the config file with the keys -> generate new keys for the address
        if(keys == null)
        {
            // if the address isn't a founder -> generate
            if(!isFounderAddress(this.address))
            {
                java.security.KeyPair keys = Security.generateKeys();
                this.keys = new KeyPair(keys.getPublic(), keys.getPrivate());
            }
            else // otherwise use the stored addressed of the founders
            {
                this.keys = new KeyPair(AddressMemory.getPublickey(this.address.toString()), AddressMemory.getPrivateKey(this.address.toString()));
            }

            // save the keys in a local file
            if(super.saveKeys(this.address, this.keys))
            {
                Commons.logger.info("Saving keys");
            }
            else
            {
                Commons.logger.severe("Error during the saving of keys");
            }
        }
    }

    /**
     * This function checks whether the address is a founder or not
     * @param address to check
     * @return true or false
     */
    private boolean isFounderAddress(Address address)
    {
        return AddressMemory.getPublickey(address.toString()) != null;
    }

    /**
     * This function loads the local blockchain whether exits or creates a new one
     * @return BlockChain instance
     */
    public BlockChain initBlockChain()
    {
        BlockChain blockChain = super.loadBlockChain();
        if(blockChain == null)
        {
            blockChain = new BlockChain(true);
        }
        else
        {
            // update the mining difficulty
            blockChain.updateMiningDifficulty();
        }

        return blockChain;
    }

    /**
     * @return the current address
     */
    public Address initAddress()
    {
        return this.address;
    }

    /**
     * @return the public key
     */
    public PublicKey initPublicKey()
    {
        return keys.getPublicKey();
    }

    /**
     * @return the private key
     */
    public PrivateKey initPrivateKey()
    {
        return keys.getPrivateKey();
    }
}
