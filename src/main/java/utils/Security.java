package utils;

import utils.fields.Hash;
import utils.fields.ObjectSignature;

import java.security.*;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * This class uses the Elliptic Curve Digital Signature Algorithm
 */
public class Security {

    private static final String FUNCTION_USED    = "EC";         //elliptic curve
    private static final String ALG_RANDOM       = "SHA1PRNG";   //algorithm for generate random number
    private static final String HASHING_FUNCTION = "SHA-256";
    private static final String SIGNING_FUNCTION = "SHA1withECDSA";
//    private static MessageDigest digest;
    private static KeyPairGenerator keyGen = null;     //Curve
    private static SecureRandom     random = null;     //Random Algorithm

    static {

        try {
            keyGen = KeyPairGenerator.getInstance(FUNCTION_USED);
            random = SecureRandom.getInstance(ALG_RANDOM);

        } catch (NoSuchAlgorithmException e) {
            System.err.println("NoSuchAlgorithmException: " + e.getMessage());
        }

        keyGen.initialize(256, random);
    }

    public static KeyPair generateKeys(){
        try{
            return keyGen.generateKeyPair();
        } catch (NullPointerException e) {
            assert keyGen != null : "The Security Class isn't initialized Before";
            return keyGen.generateKeyPair();
        }
    }

    /**
     * Calculate Hash
     * @param msg I have to give a byte array of string, for example "ciao".getBytes();
     * @return an Hash
     */
    public static Hash calculateHash(byte[] msg) {
        // msg have to be in utf8Bytes
        try {
            MessageDigest digest = MessageDigest.getInstance(HASHING_FUNCTION);
            byte[] temp = digest.digest(msg);
            return new Hash(Security.bytesToHex(temp));
        } catch (NoSuchAlgorithmException e) {
            Commons.logger.severe("The algorithm "+SIGNING_FUNCTION+" isn't supported");
            System.exit(1); //NOTHING COULD WORK
        } catch (NullPointerException e){
            Commons.logger.warning("Null pointer exception passed");
        }
        return null;
    }

    public static ObjectSignature signHash(PrivateKey priv, Hash hash) {

        try{
            Signature dsa = Signature.getInstance(SIGNING_FUNCTION);
            dsa.initSign(priv);
            dsa.update(hash.toByteArray());
//            dsa.update("ciao".getBytes());
//            System.out.println("DSA.sign: "+Security.bytesToHex(dsa.sign()));
            ObjectSignature temp = new ObjectSignature(dsa.sign());

            return temp; //Signature dell array di byte that I have passed
        } catch (NoSuchAlgorithmException e){
            System.err.println("The algorithm " + SIGNING_FUNCTION + " isn't support!!: " + e);
            System.exit(1); //NOTHING COULD WORK
        } catch (InvalidKeyException e){
            System.err.println("InvalidKeyException: " + e);
        } catch (SignatureException e){
            System.err.println("SignatureException: " + e);
        }
        return null;
    }

    /**
     *
     * @param publ          public key
     * @param MsgToVerify   bytes array of the message to verify
     * @param sign          bytes array with the signature of the message
     * @return              true if correct
     *
     * @throws InvalidKeyException if bad privateKey
     * @throws SignatureException  if bad fields to sign
     */
    public static boolean verifyByteArray(PublicKey publ, byte[] MsgToVerify, ObjectSignature sign)
        throws InvalidKeyException, SignatureException {


        try{
            Signature sig = Signature.getInstance(SIGNING_FUNCTION);
            sig.initVerify(publ);
            sig.update( MsgToVerify );
            if( sig.verify(sign.getByteArray())){
                return true;
            }else {
                Commons.logger.info("VERIFY BYTE ARRAY: \nPbkey: "+publ.toString()+
                        "\nMsgToVerify: "+MsgToVerify.toString()+"\nSign: "+sign.toString()+"\n");

                return false;
            }
        } catch (NoSuchAlgorithmException e) {
            System.err.println("The algorithm " + SIGNING_FUNCTION + " isn't supported: " + e);
            System.exit(1); //NOTHING COULD WORK
        } catch (InvalidKeyException e){
            System.err.println("InvalidKeyException: " + e);
            throw new InvalidKeyException("Bad public Key found: " + e.getMessage());
        } catch (SignatureException e){
            System.err.println("SignatureException: " + e);
            throw new SignatureException("Bad Object to verify: " + e.getMessage());
        }

        return false; //error
    }


    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len - 1; i += 2) {
            try {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i + 1), 16));
            } catch (Exception e) {

                System.out.println("Nothing " + s.length() + " c");
            }
        }

        return data;


    }


    //METODI PER MEMORIZZARE UNA CHIAVE DA UNA STRINGA
    public static String getStringFromPrivateKey(PrivateKey privateKey){
        byte[] privateKeyBytes = privateKey.getEncoded();
        return bytesToHex(privateKeyBytes);
    }

    public static String getStringFromPublicKey(PublicKey publicKey){
        byte[] publicKeyBytes = publicKey.getEncoded();
        return  bytesToHex(publicKeyBytes);
    }

    //Only for String generated with getStringFromPrivateKey
    public static PrivateKey getPrivateKeyFromString(String s){
        byte[] temp = hexStringToByteArray(s);
        EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(temp);
        KeyFactory generator = null;
        try {
            generator = KeyFactory.getInstance(FUNCTION_USED);
            PrivateKey privateKey = generator.generatePrivate(privateKeySpec);
            return  privateKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Only for String generated with getStringFromPublicKey
    public static PublicKey getPublicKeyFromString(String s){
        byte[] temp = hexStringToByteArray(s);
        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(temp);
        KeyFactory generator = null;
        try {
            generator = KeyFactory.getInstance(FUNCTION_USED);
            PublicKey publicKey = generator.generatePublic(publicKeySpec);
            return  publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

}


