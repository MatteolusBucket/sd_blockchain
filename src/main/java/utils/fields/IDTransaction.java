package utils.fields;

/**
 * This class represents the id of the transaction
 */
public class IDTransaction extends ComparableObject
{
    /**
     * Constructor of the class
     * @param id used to init the id of the transaction
     */
    public IDTransaction(String id)
    {
        super(id);
    }
}