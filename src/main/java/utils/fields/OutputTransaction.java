package utils.fields;

import utils.Security;

import java.io.Serializable;
import java.security.PublicKey;

public class OutputTransaction implements Serializable{

    static long serialVersionUID = 1;

    private Address   address;
    private String public_Key;
    private float amount;

    public OutputTransaction(Address address, PublicKey public_Key, float amount){
        this.address = address;
        this.public_Key = Security.getStringFromPublicKey(public_Key);
        this.amount = amount;
    }

    public String toString(){

        String temp = this.public_Key;
        return "address: "+this.address+"\tamount: "+this.amount+"\n\t\tpublic_key: "+temp;
    }

    public PublicKey getPublic_Key() {
         return Security.getPublicKeyFromString(public_Key);
    }

    public Address getAddress() {
        return address;
    }

    public float getAmount(){
        return this.amount;
    }
}
