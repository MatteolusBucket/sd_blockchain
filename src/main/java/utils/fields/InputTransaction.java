package utils.fields;


import java.io.Serializable;

public class InputTransaction implements Serializable{

    static long serialVersionUID = 1;

    private IDTransaction id_transaction;
    private Address address;
    private float amount;

    public InputTransaction(IDTransaction id_transaction, Address address, float amount){
        this.id_transaction = id_transaction;
        this.address = address;
        this.amount  = amount;
    }

    public String toString(){
        return "\nid_trans: "+this.id_transaction+"\naddress: "+this.address+"\namount: "+amount;
    }

    public IDTransaction getID(){
        return this.id_transaction;
    }
    public Address getAddress() {
        return address;
    }

}
