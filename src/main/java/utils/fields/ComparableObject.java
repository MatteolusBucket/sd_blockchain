package utils.fields;

import utils.Security;

import java.io.Serializable;

/**
 * This class represents a comparable object
 */
public class ComparableObject implements Comparable<ComparableObject>, Serializable
{
    static long serialVersionUID = 1;
    private String object;

    /**
     * Constructor of the class
     * @param object is the object used like key
     */
    public ComparableObject(String object)
    {
        this.object = object;
    }

    /**
     * @return a representation of the object using a byte arrray
     */
    public byte[] toByteArray()
    {
        return Security.hexStringToByteArray(object);
    }

    /**
     * @return the object in string format
     */
    @Override
    public String toString()
    {
        return object;
    }

    /**
     * This method compares two objects
     * @param o is the object to check
     * @return the same {@link Comparable}
     */
    @Override
    public int compareTo(ComparableObject o)
    {
        return object.compareTo(o.object);
    }

    /**
     * This function checks if two object are equal
     * @param object to check
     * @return true or false
     */
    @Override
    public boolean equals(Object object)
    {
        return compareTo((ComparableObject)object) == 0;
    }
}
