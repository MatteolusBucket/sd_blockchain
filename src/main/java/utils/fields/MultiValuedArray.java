package utils.fields;

import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import utils.Commons;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MultiValuedArray {

    private ArrayList<List<Block>> list;

    public MultiValuedArray() {
        list = new ArrayList<>();
    }

    //return null if there's no elements at that position
    public List<Block> get(int i) {

        try {
            if (list.get(i).size() == 0) { //Anche se la lista a quell'indice è già stata inizializzata ma per qualche motivo non contiene elementi
                return null;
            }
            return list.get(i);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }

    }

    /**
     *
     * @param i Index of the block
     * @param block that I want to add, assuming the correctness
     * @return  true if it's added correctly, false otherwise
     */
    public boolean add(int i, Block block) {

        assert block != null;

        //If this is the first block in that position create the list
        if (get(i) == null) {

            if (list.size() < i) {
                for (int j = list.size(); j < i; j++) {
                    List<Block> temp = new LinkedList<>();
                    list.add(j, temp);
                }
            }

            List<Block> newList = new LinkedList<>();

            if (!newList.add(block)){
                return false;
            }

            list.add(i, newList);

            return true;

        } else {

            Commons.logger.info("Branching on id: "+block.getID());

            return list.get(i).add(block);
        }
    }

    @Override
    public String toString() {
        StringBuilder temp = new StringBuilder();

        if (list.size() == 0){
            temp.append("[]");
        } else {
            for (int i = 0; i < list.size(); i++) {
                temp.append("\n" + list.get(i));
            }
        }

        return temp.toString();
    }

    /**
     * Return true if a block equals that block that I've passed it's already presents
     * @param block that I want to check
     * @return true if it's available
     */
    public boolean isAvailable(IBlock block) {

        List<Block> temp = get(block.getID());
        if (temp!= null && temp.contains(block)){
            return true;
        }else{
            return false;
        }
    }

    public int size() {
        return list.size();
    }
}