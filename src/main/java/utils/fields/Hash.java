package utils.fields;

/**
 * This class represents an hash
 */
public class Hash extends ComparableObject
{
    /**
     * Constructor of the class
     * @param object used to init the hash
     */
    public Hash(String object)
    {
        super(object);
    }
}
