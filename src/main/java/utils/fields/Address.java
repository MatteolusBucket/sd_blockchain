package utils.fields;

import utils.Security;

import java.util.UUID;

/**
 * This class represents an address
 */
public class Address extends ComparableObject
{
    /**
     * Constructor of the class
     * @param s used to init the address
     */
    public Address(String s)
    {
        super(s);
    }

    /**
     * Constructor of the class
     * @param uuid used to init the address
     */
    public Address(UUID uuid)
    {
        super(Security.calculateHash(uuid.toString().getBytes()).toString());
    }

    /**
     * Constructor of the class
     * @param l used to init the address
     */
    public Address(long l)
    {
        super(Security.calculateHash(String.valueOf(l).getBytes()).toString());
    }
}
