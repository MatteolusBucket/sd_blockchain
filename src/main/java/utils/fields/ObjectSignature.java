package utils.fields;

import utils.Security;

import java.io.Serializable;

public class ObjectSignature implements Serializable{

    static long serialVersionUID = 1;

    private byte[] signature;

    public ObjectSignature(byte[] a){
        this.signature = a;
    }

    public byte[] getByteArray(){
        return signature;
    }

    @Override
    public String toString(){
        return Security.bytesToHex(this.signature);
    }
}
