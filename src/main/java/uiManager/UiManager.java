package uiManager;

import blockchain.BlockChain;
import blockchain.blocks.IBlock;
import miner.MinerApp;
import org.apache.commons.lang3.StringUtils;
import utils.Commons;
import utils.Security;
import utils.fields.Address;
import wallet.WalletApp;

import java.security.PublicKey;
import java.util.Scanner;

/**
 * This class manages the user interface
 */
public class UiManager extends Thread implements IUiManager
{
    private Object app;
    private WalletApp walletApp;
    private MinerApp minerApp;
    private BlockChain blockChain;
    private boolean stopThread = false;

    /**
     * Constructor of the class
     * @param app instance of the current application
     * @param blockChain instance of the blockchain
     */
    public UiManager(Object app, BlockChain blockChain)
    {
        this.app = app;
        this.blockChain = blockChain;

        if (app instanceof WalletApp)
        {
            walletApp = (WalletApp)app;
        }
        else if (app instanceof MinerApp)
        {
            minerApp = (MinerApp)app;
        }
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * Scan commands and execute them
     */
    public void run()
    {
        Scanner inputReader;
        String input;

        System.out.println("Type \"help\" is you need!\n");

        while(!stopThread)
        {
            System.out.print("> ");
            inputReader = new Scanner(System.in);
            input = inputReader.nextLine();

            if (input.length() > 0)
            {
                processCommand(input);
            }
        }
    }

    /**
     * This function parses and executes the command
     * @param command to execute
     */
    private void processCommand(String command)
    {
        String[] commandArgs = StringUtils.split(command, " ");

        switch (commandArgs[0].toLowerCase())
        {
            case "amount":
            {
                if (app instanceof WalletApp)
                {
                    amountCommand();
                }
                else
                {
                    helpCommand();
                }

                break;
            }
            case "payment":
            {
                if (app instanceof WalletApp)
                {
                    if (commandArgs.length == 4)
                    {
                        paymentCommand(commandArgs[1], commandArgs[2], commandArgs[3]);
                    }
                    else
                    {
                        System.out.println("\t\tpayment: wrong syntax");
                    }
                }
                else
                {
                    helpCommand();
                }

                break;
            }
            case "send":
            {
                if (app instanceof WalletApp)
                {
                    sendCommand();
                }
                else
                {
                    helpCommand();
                }

                break;
            }
            case "delete":
            {
                if (app instanceof WalletApp)
                {
                    deleteCommand();
                }
                else
                {
                    helpCommand();
                }

                break;
            }
            case "status": statusCommand(); break;
            case "stats":
            {
                if(commandArgs.length == 2)
                {
                    statsCommand(commandArgs[1]);
                }
                else
                {
                    statsCommand("-1");
                }
            } break;
            case "help": helpCommand(); break;
            case "exit": exitCommand(); break;
            case "network": networkCommand(); break;

            default: helpCommand(); break;
        }
    }

    /**
     * Exit command function
     */
    private void exitCommand()
    {
        if (app instanceof MinerApp)
        {
            minerApp.stopMinerApp();
        }
        else if (app instanceof WalletApp)
        {
            walletApp.stopWalletApp();
        }
    }

    /**
     * Statistics command function
     * @param id of the block
     */
    private void statsCommand(String id)
    {
        int startIdBlock, endIdBlock;

        synchronized(blockChain)
        {
            System.out.println("\tStatistics:\n");
            System.out.println("\t\tThere are " + blockChain.size() + " blocks in the blockchain");
        }

        if(id.equals("-1"))
        {
            startIdBlock = 0;
            endIdBlock = blockChain.size() - 1;
        }
        else
        {
            try
            {
                startIdBlock = Integer.parseInt(id);
            }
            catch(NumberFormatException e)
            {
                System.out.println("\t\t\tError.. Insert an integer!");
                return;
            }

            if(startIdBlock < 0 || startIdBlock > blockChain.size() - 1)
            {
                System.out.println("\n\t\tError.. Insert a valid id!");
                return;
            }

            endIdBlock = startIdBlock;
        }

        for(; startIdBlock <= endIdBlock; startIdBlock++)
        {
            for(IBlock block : blockChain.getBlock(startIdBlock))
            {
                System.out.println(block);
            }
        }
    }

    /**
     * Status command function
     */
    private void statusCommand()
    {
        if (app instanceof MinerApp)
        {
            System.out.println("\tStatistics:\n" + minerApp.getStatus());
        }
        else if (app instanceof WalletApp)
        {
            System.out.println("\tStatistics:\n" + walletApp.getStatus());
            System.out.println("\n\n\tPayments:\n" + walletApp.paymentStatus());
        }
    }

    /**
     * Delete command function
     */
    private void deleteCommand()
    {

        if (walletApp.deletePayment())
        {
            System.out.println("\t\tDelete Success!");
        }
        else
        {
            System.out.println("\t\tSome error occurs, please retry!");
        }
    }

    /**
     * Send command function
     */
    private void sendCommand()
    {
        if (walletApp.sendPayment())
        {
            System.out.println("\t\tSending.. Done!");
        }
        else
        {
            System.out.println("\t\tSome error occurs, please retry!");
        }
    }

    /**
     * Payment command function
     * @param amount of the payment
     * @param address of the receiver
     * @param publicKey of the receiver
     */
    private void paymentCommand(String amount, String address, String publicKey)
    {
        float aFloat;
        Address address1;
        PublicKey pk;

        try
        {
            aFloat = Float.parseFloat(amount);
        }
        catch (NumberFormatException e)
        {
            System.out.println("\t\tError in amount format: " + amount);
            return;
        }

        if (walletApp.getExpendableAmount() < aFloat)
        {
            System.out.println("\t\tYou haven't enough money!");
            return;
        }

        try {
            address1 = new Address(address);
        }
        catch (NumberFormatException e)
        {
            System.out.println("\t\tError in address format: " + address);
            return;
        }

        pk = Security.getPublicKeyFromString(publicKey);
        if (pk == null)
        {
            System.out.println("\t\tBad public key format");
            return;
        }

        if (walletApp.addPaymentReceiver(aFloat, address1, pk))
        {
            System.out.println("\t\tSaving payment.. Done!");
        }
        else
        {
            System.out.println("\t\tSome error occurs, please retry or send the actual payment!");
        }
    }

    /**
     * Amount command function
     */
    private void amountCommand()
    {
        System.out.println("\tAddress: " + walletApp.getAddress());
        System.out.println("\tAmount: " + walletApp.getAmount());
    }

    /**
     * Network command function
     */
    private void networkCommand()
    {
        if (app instanceof MinerApp)
        {
            minerApp.networkStatus();
        }
        else if (app instanceof WalletApp)
        {
            walletApp.networkStatus();
        }
    }

    /**
     * Help command function
     */
    private void helpCommand()
    {
        if (app instanceof WalletApp)
        {
            System.out.println("\tamount\n\t\t" + "- This command returns the amount of the wallet\n");

            System.out.println("\tpayment <amount> <address-receiver> <public-key-receiver>\n\t\t" + "- This command saves the payment\n");

            System.out.println("\tsend\n\t\t" + "- This command sends all the saved payments\n");

            System.out.println("\tdelete <ID-transaction>\n\t\t" + "- This command deletes a saved payment before send\n");
        }

        System.out.println("\tstatus\n\t\t" +
                "- This command shows the payments status\n");

        System.out.println("\tstats\n\t\t" +
                "- This command shows the blockchain status\n");

        System.out.println("\tnetwork\n\t\t" +
                "- This command shows the network status\n");

        System.out.println("\thelp\n\t\t" +
                "- This command prints this menu\n");

        System.out.println("\texit\n\t\t" +
                "- This command conquers the world");
    }

    /**
     * This function stops the user interface manager
     */
    public void stopManager()
    {
        this.stopThread = true;

        Commons.logger.info("UiManager stopped");
    }
}