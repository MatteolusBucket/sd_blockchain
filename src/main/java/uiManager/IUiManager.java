package uiManager;

public interface IUiManager
{
    /**
     * This function stops the user interface manager
     */
    void stopManager();
}
