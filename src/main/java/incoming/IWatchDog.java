package incoming;

public interface IWatchDog
{
    /**
     * This function stops the WatchDog
     */
    void stopWatchDog();

    /**
     * This method overrides the Thread.start()
     * We check if the chain is updated or not
     */
    void start();
}
