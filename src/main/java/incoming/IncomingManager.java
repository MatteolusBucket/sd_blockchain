package incoming;

import blockchain.BlockChain;
import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import communications.CommunicationsManager;
import communications.Node;
import miner.MinerManager;
import miner.MinerApp;
import utils.Commons;
import utils.dataManager.DataInit;
import wallet.WalletApp;

import java.util.*;

/**
 * This class receives and saves blocks and transactions by the network that in this case is represented
 * by the {@link CommunicationsManager}
 */
public class IncomingManager extends Thread implements IIncomingManager
{
    private boolean stopIncomingManagerThread = false;
    private final IIncomingContainer incomingTransactionsHandler, incomingBlocksHandler;
    private CommunicationsManager communicationsManager;
    private final BlockChain blockChain;
    private Object app;
    private MinerManager miner = null;
    private  DataInit dataInit;
    private boolean isUpdatedBlockChain = false;
    private static final int UPDATE_ATTEMPTS = 10;
    private static final int TIMER_WAIT_CONNECTION_MS = 60000;
    private static final int TIMER_CHECK_BLOCKS_BUFFER_MS = 1000;
    private static final int NUMBER_BLOCKS_ASK = 5;
    private static final int TRANSACTIONS_THRESHOLD_BUFFER_WALLET = 100;

    /**
     * Constructor of the class
     * @param app is the instance of @{@link MinerApp} or {@link WalletApp}
     * @param blockChain is the chain {@link BlockChain}
     * @param initialHosts is the array with the initial host to connect
     * @param localPort is the listening port for the app
     * @param dataInit is the saver class used by the blockchain {@link DataInit}
     */
    public IncomingManager(Object app, BlockChain blockChain, String[] initialHosts, int localPort, DataInit dataInit)
    {
        this.app = app;
        this.blockChain = blockChain;
        this.dataInit = dataInit;
        incomingTransactionsHandler = new IncomingContainer();
        incomingBlocksHandler = new IncomingContainer();

        // creation the instance used to communicate
        communicationsManager = new CommunicationsManager(initialHosts, this, localPort);
        communicationsManager.start(); // is a thread -> we need to call the start method

        System.out.println("Connecting to the network..");
    }

    /**
     * This method overrides the Thread.start()
     * We check if the chain is updated or not
     */
    @Override
    public void start()
    {
        if(!isUpdatedBlockChain)
        {
            Commons.logger.severe("Before start the IncomingManager you must update the blockchain");

            stopApp(app);
        }
        else
        {
            super.start();
        }
    }

    /**
     * This method stops the app using the correct instance of the object
     * @param app to stop
     */
    public static void stopApp(Object app)
    {
        if(app instanceof WalletApp)
        {
            ((WalletApp)app).stopWalletApp();
        }
        else if(app instanceof MinerApp)
        {
            ((MinerApp)app).stopMinerApp();
        }
        else
        {
            Commons.logger.severe("The instance of the app is unknown!");

            System.exit(-1);
        }
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * This function checks incomingBlocksHandler and inserts the new block in the blockchain
     * If there is a gap between the new block and the actual status of the chain this method will resolve it
     */
    @Override
    public void run()
    {
        Iterator keys, iteratorBlocks;
        Set<IBlock> setBlocks;
        Block block;
        boolean isAdded = true, isBranchesOk = false;
        int idToAsk;

        while(!stopIncomingManagerThread)
        {
            // lock the blocks buffer
            synchronized(incomingBlocksHandler)
            {
                if(incomingBlocksHandler.size() > 0)
                {
                    // if we have blocks we'll try to insert them into the chain
                    keys = incomingBlocksHandler.keySetIterator();
                    while(keys.hasNext())
                    {
                        block = (Block)incomingBlocksHandler.get(keys.next());

                        // lock the chain
                        synchronized(blockChain)
                        {
                            // in this case we have a gap between the block we are trying to insert and the last block in the chain
                            if(block.getID() > blockChain.size())
                            {
                                idToAsk = blockChain.size();
                                while(!isBranchesOk)
                                {
                                    isAdded = false;

                                    // ask block to the network
                                    setBlocks = communicationsManager.getBlocks(idToAsk);
                                    if(setBlocks.size() > 0)
                                    {
                                        iteratorBlocks = setBlocks.iterator();
                                        while(iteratorBlocks.hasNext())
                                        {
                                            // trying to fill the gap
                                            isAdded = blockChain.addBlock((Block)iteratorBlocks.next());

                                            // if something goes wrong means that the branch or the branches aren't ok
                                            if(!isAdded)
                                            {
                                                // go back to ask the block
                                                idToAsk--;
                                                break;
                                            }
                                        }
                                    }

                                    if(isAdded)
                                    {
                                        // trying to fix the chain
                                        if(idToAsk < blockChain.size())
                                        {
                                            idToAsk++;
                                        }
                                        else
                                        {
                                            isBranchesOk = true;
                                        }
                                    }
                                }
                            }

                            // at this point we have resolved the gap -> we can try to insert our block!
                            if(isAdded)
                            {
                                isAdded = blockChain.addBlock(block);

                                // if we are executing this class in a MinerApp and we are mining a block
                                // in this case we stop
                                if(miner instanceof MinerManager && isAdded)
                                {
                                    // this happens only if the received block has id higher or equal to the mining one
                                    if(block.getID() >= miner.getIDMiningBlock())
                                    {
                                        System.out.println("Stop mining block " + block.getID());
                                        miner.stopMining();
                                    }
                                }
                            }
                        }

                        // if something goes wrong during this procedure we re-ask the block
                        if(!isAdded)
                        {
                            Commons.logger.warning("Failed to insert block " + block.getID());

                            setBlocks = communicationsManager.getBlocks(block.getID());
                            for(IBlock b : setBlocks)
                            {
                                // insert in our buffer -> incomingBlocksHandler
                                onReceive(b);
                            }
                        }
                        else
                        {
                            // if it'is all well we save locally the blockchain
                            if(!dataInit.saveBlockChain(blockChain))
                            {
                                Commons.logger.severe("Saving blockchain.. Failed!");
                            }
                        }

                        //remove the current block from the incomingBlocksHandler
                        keys.remove();
                    }
                }
            }

            try
            {
                Thread.sleep(TIMER_CHECK_BLOCKS_BUFFER_MS);
            }
            catch(InterruptedException e)
            {
                Commons.logger.warning("Interrupted during the check of new blocks to insert into the blockchain");
            }
        }
    }

    /**
     * This function updates the blockchain
     * If something goes wrong we aren't able to continue the execution of the application
     */
    public void updateBlockChain()
    {
        try
        {
            Commons.logger.info("Waiting for connection..");
            communicationsManager.waitConnection(TIMER_WAIT_CONNECTION_MS);

            if(communicationsManager.isConnected())
            {
                boolean failed = false;
                // we request the needed blocks to the network
                LinkedList<Set<IBlock>> blocks = requestBlocks();
                int attempt = 0;
                Iterator iteratorSets, iteratorBlocks;
                Set<IBlock> tmpSetBlocks;
                Block tmpBlock;

                iteratorSets = blocks.iterator();
                // trying to update che blockchain -> many attempts :D
                while(attempt < UPDATE_ATTEMPTS && iteratorSets.hasNext())
                {
                    tmpSetBlocks = (Set<IBlock>)iteratorSets.next();
                    iteratorBlocks = tmpSetBlocks.iterator();
                    while(iteratorBlocks.hasNext())
                    {
                        tmpBlock = (Block)iteratorBlocks.next();
                        synchronized(blockChain)
                        {
                            failed = !blockChain.addBlock(tmpBlock);
                        }

                        if(failed)
                        {
                            // TODO: 05/06/18 failed == true works but never seen in our test :)
                            blocks = requestBlocks();
                            iteratorSets = blocks.iterator();
                            Commons.logger.warning("Updating blockchain failed.. ATTEMPT #" + attempt + 1);

                            attempt++;
                        }
                        else
                        {
                            Commons.logger.info("Updating blockchain.. Block " + tmpBlock.getID() + " received");
                        }
                    }
                }

                if(failed)
                {
                    Commons.logger.severe("Failed during the update of the blockchain");

                    isUpdatedBlockChain = false;
                }
                else
                {
                    // if it'is all well we save locally the blockchain
                    if(!dataInit.saveBlockChain(blockChain))
                    {
                        Commons.logger.severe("Saving blockchain.. Failed!");

                        isUpdatedBlockChain = false;
                    }
                    else
                    {
                        isUpdatedBlockChain = true;
                    }
                }
            }
            else
            {
                Commons.logger.severe("Connection not available");

                isUpdatedBlockChain = false;
            }
        }
        catch(InterruptedException e)
        {
            isUpdatedBlockChain = false;
            Commons.logger.warning("Interrupted during the waitConnection");
        }
    }

    /**
     * This function requests the initial blocks to update the chain
     * @return
     */
    private LinkedList<Set<IBlock>> requestBlocks()
    {
        int startID, lastKnownBlockID = communicationsManager.getLastKnownBlockID(); // we ask to the network the last known block id
        LinkedList<Set<IBlock>> blocks = new LinkedList<>();
        Set<IBlock> tmpSetBlocks;

        synchronized(blockChain)
        {
            // if the local chain is back
            if(lastKnownBlockID > blockChain.size())
            {
                startID = (blockChain.size() > NUMBER_BLOCKS_ASK) ? (blockChain.size() - 1) - NUMBER_BLOCKS_ASK : 1;
            }
            else
            {
                // in this case our our local chain is ahead -> strange case but we need to manege it
                startID = (lastKnownBlockID - NUMBER_BLOCKS_ASK > 0) ? lastKnownBlockID - NUMBER_BLOCKS_ASK : 1;
            }
        }

        // asking the blocks
        for(int i = startID; i <= lastKnownBlockID; i++)
        {
            tmpSetBlocks = communicationsManager.getBlocks(i);
            if(tmpSetBlocks.size() > 0)
            {
                blocks.add(tmpSetBlocks);
            }
        }

        return blocks;
    }

    /**
     * This function sets the miner
     * @param miner is the miner to set
     */
    public void setMiner(MinerManager miner)
    {
        this.miner = miner;
    }

    /**
     * This method sends a block on the network
     * @param block to be sent
     */
    public void spreadBlock(IBlock block)
    {
        communicationsManager.spreadBlockAsync(block);
    }

    /**
     * This method sends a transaction on the network
     * @param transaction to be sent
     */
    public void spreadTransaction(ITransaction transaction)
    {
        communicationsManager.spreadTransactionAsync(transaction);
    }

    /**
     * This method will be executed when a transaction is received
     * @param transaction the received transaction
     * @return true if the transaction isn't already received (i.e. it was accepted), false otherwise
     */
    public boolean onReceive(ITransaction transaction)
    {
        // this cache is used only in the case we are using this class in the WalletApp
        TreeMap cacheTransactions = new TreeMap();

        synchronized(blockChain)
        {
            // we save only transactions that aren't used
            if (blockChain.verifyTransactionLongestBranch(transaction) != BlockChain.TRANSACTION_STATE.USED)
            {
                synchronized(incomingTransactionsHandler)
                {
                    // save in the buffer
                    if(incomingTransactionsHandler.onReceive(transaction))
                    {
                        // this means that we are in a WalletApp
                        if(miner == null)
                        {
                            // in a WalletApp we don't use the transactions, therefore after a threshold we'll delete them
                            if(incomingTransactionsHandler.size() > TRANSACTIONS_THRESHOLD_BUFFER_WALLET)
                            {
                                incomingTransactionsHandler.remove(cacheTransactions.firstEntry());
                                cacheTransactions.remove(cacheTransactions.firstKey());
                            }

                            cacheTransactions.put(System.currentTimeMillis(), transaction.getID());
                        }

                        return true;
                    }
                }
            }

            return false;
        }
    }

    /**
     * This method will be executed when a block is received
     * @param block the received block
     * @return true if the block isn't already received (i.e. it was accepted), false otherwise
     */
    public boolean onReceive(IBlock block)
    {
        List<Block> listOfBlocks;

        // we have to lock the incomingBlocksHandler before the blockchain in order to avoid deadlock
        synchronized(incomingBlocksHandler)
        {
            synchronized(blockChain)
            {
                // we accept only valid block to insert into the buffer
                if (blockChain.verifyBlock(block) == IBlock.STATE.VALID)
                {
                    listOfBlocks = blockChain.getBlock(block.getID());
                    if(listOfBlocks != null)
                    {
                        for (Block b : listOfBlocks)
                        {
                            // in the case we already have the received block in the chain
                            if (b.equals(block))
                            {
                                Commons.logger.info("We have already this block");
                                return false;
                            }
                        }
                    }

                    // saving in the buffer
                    if(!incomingBlocksHandler.onReceive(block))
                    {
                        Commons.logger.info("Something goes wrong during the insertion in the buffer");
                        return false;
                    }

                    return true;
                }
            }

            Commons.logger.info("The block isn't valid");
            return false;
        }
    }

    /**
     * This method returns a list of blocks with the same ID
     * @param i is the id of the block
     * @return the block or null if the block doesn't exist
     */
    public List<Block> getBlock(int i)
    {
        synchronized(blockChain)
        {
            return blockChain.getBlock(i);
        }
    }

    /**
     * @return This method returns the transactions
     */
    public IIncomingContainer getTransactions()
    {
        synchronized(incomingTransactionsHandler)
        {
            return incomingTransactionsHandler;
        }
    }

    /**
     * @return This method returns the blocks
     */
    public IIncomingContainer getBlocks()
    {
        synchronized(incomingBlocksHandler)
        {
            return incomingBlocksHandler;
        }
    }

    /**
     * Returns the id of the newest block in the blockchain
     * @return the id
     */
    public int getLastKnownBlockId()
    {
        return blockChain.size() - 1;
    }

    /**
     * This method stops the manager and the communication with the network
     */
    public void stopManager()
    {
        Commons.logger.info("IncomingManager stopped");

        communicationsManager.stop();

        stopIncomingManagerThread = true;
    }

    /**
     * This method returns if the chain is updated
     * @return true or false
     */
    public boolean isUpdatedBlockChain()
    {
        return isUpdatedBlockChain;
    }

    /**
     * Print the status of the network
     */
    public void networkStatus()
    {
        int port = communicationsManager.getLocalPort();
        System.out.println("\t\t\tLocal port = "+port);
    /*    double quality = communicationsManager.getConnectivityQuality();
        System.out.println("\t\t\tConnectivity quality = "+quality);*/

        List<Node> l = new ArrayList<>();
        l = communicationsManager.getKnownNodes();
        System.out.println("\t\t\tKnown nodes                = "+l.size());

        l = communicationsManager.getReachableNodesList();
        System.out.println("\t\t\tReachable nodes            = "+l.size());

        l = communicationsManager.getNodesThatChoseUs();
        System.out.println("\t\t\tNodes subscribed to us     = "+l.size());

        l = communicationsManager.getNodesWeChose();
        System.out.println("\t\t\tNodes we are subscribed to = "+l.size());
    }
}
