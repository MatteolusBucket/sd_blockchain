package incoming;

import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;

import java.util.List;

public interface IBlocksTransactionsExchanger
{
    /**
     * This method will be executed when a transaction is received
     * @param transaction the received transaction
     * @return true if the transaction isn't already received (i.e. it was accepted), false otherwise
     */
    boolean onReceive(ITransaction transaction);

    /**
     * This method will be executed when a block is received
     * @param block the received transaction
     * @return true if the block isn't already received (i.e. it was accepted), false otherwise
     */
    boolean onReceive(IBlock block);

    /**
     * This method returns a list of blocks with the same id
     * @param i is the id of the block
     * @return the block or null if the block doesn't exist
     */
    List<Block> getBlock(int i);

    /**
     * Returns the id of the newest block in the blockchain
     * @return the id
     */
    int getLastKnownBlockId();
}
