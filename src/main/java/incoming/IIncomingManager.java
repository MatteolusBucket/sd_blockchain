package incoming;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import miner.MinerManager;

public interface IIncomingManager extends IBlocksTransactionsExchanger
{
    /**
     * This method sends a block on the network
     * @param block to be sent
     */
    void spreadBlock(IBlock block);

    /**
     * This method sends a transaction on the network
     * @param transaction to be sent
     */
    public void spreadTransaction(ITransaction transaction);

    /**
     * @return This method returns the transactions
     */
    IIncomingContainer getTransactions();

    /**
     * @return This method returns the blocks
     */
    IIncomingContainer getBlocks();

    /**
     * This method stops the manager and the communication with the network
     */
    void stopManager();

    /**
     * This method overrides the Thread.start()
     * We check if the chain is updated or not
     */
    void start();

    /**
     * This method returns if the chain is updated
     * @return true or false
     */
    boolean isUpdatedBlockChain();

    /**
     * This function updates the blockchain
     * If something goes wrong we aren't able to continue the execution of the application
     */
    void updateBlockChain();

    /**
     * This function sets the miner
     * @param miner is the miner to set
     */
    void setMiner(MinerManager miner);

    /**
     * Print the status of the network
     */
    void networkStatus();
}
