package incoming;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;

import java.util.Iterator;
import java.util.TreeMap;

/**
 * This class is a container of generic objects
 */
public class IncomingContainer implements IIncomingContainer
{
    private TreeMap incomingContainer;

    /**
     * Constructor of the class
     */
    public IncomingContainer()
    {
        incomingContainer = new TreeMap();
    }

    /**
     * This method will be executed when an object is received
     * @param object the received object
     * @return true if the object isn't already received (i.e. it was accepted), false otherwise
     */
    public synchronized boolean onReceive(Object object)
    {
        synchronized(incomingContainer)
        {
            if(object instanceof ITransaction)
            {
                if(!incomingContainer.containsKey(((ITransaction)object).getID()))
                {
                    incomingContainer.put(((ITransaction)object).getID(), object);
                    return true;
                }
            }
            else if(object instanceof IBlock)
            {
                if(!incomingContainer.containsKey(((IBlock)object).getBlockHash()))
                {
                    incomingContainer.put(((IBlock)object).getBlockHash(), object);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return the number of objects
     */
    public synchronized int size()
    {
        synchronized(incomingContainer)
        {
            return incomingContainer.size();
        }
    }

    /**
     * This method will remove an entry
     * @param key the identifier of the entry
     * @return true if the entry has been deleted, false otherwise
     */
    public synchronized boolean remove(Object key)
    {
        synchronized(incomingContainer)
        {
            return incomingContainer.remove(key) != null;
        }
    }

    /*
     * @return a navigable key set iterator of the values contained
     */
    public synchronized Iterator keySetIterator()
    {
        synchronized(incomingContainer)
        {
            return incomingContainer.navigableKeySet().iterator();
        }
    }

    /**
     * This method checks if the object is present in the container
     * @param key object to check
     * @return true or false
     */
    public synchronized boolean containsKey(Object key)
    {
        synchronized(incomingContainer)
        {
            return incomingContainer.containsKey(key);
        }
    }

    /**
     * This method returns an object
     * @param key is the key of the object to return
     * @return the asked object
     */
    public synchronized Object get(Object key)
    {
        synchronized(incomingContainer)
        {
            return incomingContainer.get(key);
        }
    }
}