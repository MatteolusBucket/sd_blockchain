package incoming;

import java.io.Serializable;
import java.util.Iterator;

public interface IIncomingContainer extends Serializable
{
    /**
     * This method will be executed when an object is received
     * @param object the received object
     * @return true if the object isn't already received (i.e. it was accepted), false otherwise
     */
    boolean onReceive(Object object);

    /*
     * @return a navigable key set iterator of the values contained
     */
    Iterator keySetIterator();

    /**
     * This method will remove an entry
     * @param id the identifier of the entry
     * @return true if the entry has been deleted, false otherwise
     */
    boolean remove(Object id);

    /**
     * @return the number of objects
     */
    int size();

    /**
     * This method checks if the object is present in the container
     * @param key object to check
     * @return true or false
     */
    public boolean containsKey(Object key);

    /**
     * This method returns an object
     * @param key is the key of the object to return
     * @return the asked object
     */
    public Object get(Object key);
}