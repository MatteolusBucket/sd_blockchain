package incoming;

import blockchain.BlockChain;
import miner.MinerApp;
import utils.Commons;
import utils.dataManager.DataInit;
import wallet.WalletApp;

/**
 * This class implements a guard that every TIMER_WATCHDOG ms refreshes the wallet {@link wallet.WalletApp.TransactionCache}
 */
public class WatchDog extends Thread implements IWatchDog
{
    private Object app;
    private IncomingManager incomingManager;
    private DataInit dataInit;
    private BlockChain blockChain;
    private WalletApp.TransactionCache transactionCache;
    private boolean stopThread = false;
    private static final int TIMER_WATCHDOG = 15000;

    /**
     * Constructor of the class
     * @param app is the instance of @{@link MinerApp} or {@link WalletApp}
     * @param blockChain is the chain {@link BlockChain}
     * @param transactionCache is the wallet {@link wallet.WalletApp.TransactionCache}
     * @param incomingManager is the manager of the network {@link IncomingManager}
     * @param dataInit is the saver class used by the blockchain {@link DataInit}
     */
    public WatchDog(Object app, BlockChain blockChain, WalletApp.TransactionCache transactionCache, IncomingManager incomingManager, DataInit dataInit)
    {
        this.app = app;
        this.blockChain = blockChain;
        this.transactionCache = transactionCache;
        this.dataInit = dataInit;
        this.incomingManager = incomingManager;
    }

    /**
     * This method overrides the Thread.start()
     * We check if the chain is updated or not
     */
    @Override
    public void start()
    {
        if(!incomingManager.isUpdatedBlockChain())
        {
            Commons.logger.severe("Before start the WatchDog you must update the blockchain");

            IncomingManager.stopApp(app);
        }
        else
        {
            super.start();
        }
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * Every TIMER_WATCHDOG ms this thread refreshes the cache
     */
    @Override
    public void run()
    {
        while(!stopThread)
        {
//            synchronized(blockChain)
//            {
//                if(app instanceof WalletApp)
//                {
//                    // load the local data of the chain
//                    blockChain = dataInit.initBlockChain();
//
//                    transactionCache = ((WalletApp)app).new TransactionCache();
//                    transactionCache.refresh();
//
//                    Commons.logger.severe("WatchDog");
//                }
//            }

            try
            {
                Thread.sleep(TIMER_WATCHDOG);
            }
            catch(InterruptedException e)
            {
                Commons.logger.warning("Interrupted in the WatchDog");
            }
        }
    }

    /**
     * This function stops the WatchDog
     */
    public void stopWatchDog()
    {
        interrupt();

        stopThread = true;
    }
}
