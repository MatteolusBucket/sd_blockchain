import miner.MinerApp;
import utils.Utils;
import wallet.WalletApp;

/**
 * This class is the main class of the project
 */
public class Main
{
    public static void main(String[] args)
    {
        // TODO: 13/07/18 use the parser to set this infos
        String pathDirectory = ".";
        String fileNameBlockChain = "blockchain.data";
        String fileNameConfig = "config.data";

        System.out.println("\nHi!\nI'm ApolloCoin and I'm a beautiful cryptocurrency...\n");

        // parsing of the args
        Utils.CliArgs parsedArgs = new Utils.CliArgs().parseCliInput(args);

        // start a miner or wallet
        if(parsedArgs.launchWallet)
        {
            WalletApp walletApp = new WalletApp(parsedArgs.args, parsedArgs.localPort, parsedArgs.address, pathDirectory, fileNameBlockChain, pathDirectory, fileNameConfig);
            walletApp.start();
        }
        else if(parsedArgs.launchMiner)
        {
            MinerApp minerApp = new MinerApp(parsedArgs.args, parsedArgs.localPort, parsedArgs.address, pathDirectory, fileNameBlockChain, pathDirectory, fileNameConfig);
            minerApp.start();
        }
        else
        {
            System.out.println("You should launch either the miner or the waller. Use \"help\" for info");
        }
    }
}

