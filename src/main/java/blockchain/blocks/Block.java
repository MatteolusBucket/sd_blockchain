package blockchain.blocks;

import blockchain.transactions.Transaction;
import utils.Commons;
import utils.Security;
import utils.fields.Address;
import utils.fields.Hash;
import utils.fields.ObjectSignature;

import javax.management.BadAttributeValueExpException;
import java.io.Serializable;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.util.*;

public class Block implements IBlock, Serializable {
    // used by java for serialization
    static long serialVersionUID = 1;

// Variables of the block
    private LinkedList<Transaction> transactions;
    public static final short MAX_TRANSACTIONS = 10;    // the minimum is 2
public static final int NONCE_THRESHOLD = 1000000;
    private ObjectSignature signature;                  //signature of the block must be include in the hash
    private String minerPublicKey;
    private Hash blockHash;
    private String infoToSignCache;                     //This is only a cache to optimization

    private short maxTransaction = MAX_TRANSACTIONS;
    private int id = -1;
    private Hash prevBlockHash;
    private Hash transactionsHash;
    private Address minerAddress;
    private long timeStamp = 0;
    private int nonce = 0;                     //Incremental number used for changing the blockHash of the block
    private float minerReward = 0;              //Fee plus mining rewards
    private int difficulty = 0;

    /**
     * Constructor
     *
     * @param id an int id of the block
     */
    public Block(int id) {
        this.id = id;
        transactions = new LinkedList<>();
}

    /**
     * Insert a transaction into a block if it's not already inserted
     *
     * @param transaction transaction object to be inserted
     * @return false if the block is full or if the transaction it's already inserted, true otherwise
     */
    public boolean addTransaction(Transaction transaction) {

        if (transactions.size() >= maxTransaction) {
            Commons.logger.warning("Try to insert into a full block");
            return false;
        } else {
            if (this.transactions.contains(transaction)) {
                Commons.logger.warning("Try to insert a transaction already insert in the same block");
                return false;
            } else {
                //Update miner reward
                if (transaction.getType() == Transaction.TYPE.STANDARD) {
                    minerReward += transaction.getFee();
                } else if (transaction.getType() == Transaction.TYPE.BLOCKREWARD) {
                    //Block reward transaction, no reward needed
                }
                return transactions.add(transaction);
            }
        }
    }

    /**
     * Contain all the block info that I have to hash!
     *
     * @return String with all the info that I have to hash
     */
    private String infoToHash() throws AssertionError {

        //All information must different from null
        //Must contain all the information plus the signature
        StringBuilder tmp = new StringBuilder();

        tmp.append(infoToSign());

        tmp.append(nonce);
        tmp.append(timeStamp);

        assert signature != null : "Try to Hash a block before sign it!";
        tmp.append(signature);  //MUST HASH ALSO THE SIGNATURE

        return tmp.toString();
    }

    /**
     * This is the things that I have to sign, this is not for print
     *
     * @return a string with the info of the block to sign
     * @throws NullPointerException if required some field not initialized
     */
    private String infoToSign() throws AssertionError {



        //All information must different from null
        if (infoToSignCache == null) {

            StringBuilder tmp = new StringBuilder();

            assert id != -1 : "ID null";
            tmp.append(id);
            assert prevBlockHash != null : "PrevBlockHash null";
            tmp.append(prevBlockHash);
            assert transactionsHash != null : "TransactionHash null";
            tmp.append(transactionsHash);
            assert minerAddress != null : "minerAddress null";
            tmp.append(minerAddress);
            tmp.append(minerReward);
            assert difficulty != 0 : "Difficulty = 0";
            tmp.append(difficulty);
            assert minerPublicKey != null : "MinerPublicKey null";
            tmp.append(minerPublicKey);

            infoToSignCache = tmp.toString(); //Initialize the cache

            return tmp.toString();

        } else {
            return infoToSignCache;
        }

    }

    /**
     * Calculate the hash of the block and return it
     *
     * @return the hash of the block
     * @throws BadAttributeValueExpException if some field it's not initialized properly
     */
    public Hash getBlockHash() {
        //Make the hash with the info inside the header and the signature and public key
        try {
            return Security.calculateHash(infoToHash().getBytes());
        } catch (AssertionError e) {
            throw new AssertionError(e);
        }
    }

    public int getDifficulty() {
        return difficulty;
    }

    /**
     * @return the id of the block, null if it's not set
     */
    public int getID() {
        return id;
    }

    /**
     * @return minerAddress, null if it's not set
     */
    public Address getMinerAddress() {
        return minerAddress;
    }


    /**
     * @return the sum of fee contained in the block plus a remuneration for mining the block
     */
    public float getMinerReward() {
        assert minerReward >= 0 : "Miner Reward < 0";
        return minerReward;
    }

    public int getNonce() {
        return nonce;
    }

    /**
     * @return the signature of the block, nullif it's not initialized
     */
    public ObjectSignature getSignature() {
        return this.signature;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * @return a linkedList with all the transactions contained into the block
     */
    public LinkedList<Transaction> getTransactions() {
        return transactions;
    }

    public Hash getPrevBlockHash() {
        return prevBlockHash;
    }

    /**
     * Insert the block hash after verification
     * @param hash pre computed block hash
     */
    public void setBlockHash(Hash hash) {
        this.blockHash = hash;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * This method insert the miner address into the block, only miner must do this
     *
     * @param minerAddress the address of the miner
     */
    public void setMinerAddress(Address minerAddress) {
        this.minerAddress = minerAddress;
    }

    /**
     * Set the parameter nonce just for set the right mining hash difficulty
     *
     * @param nonce an int parameter to variate the block's hash
     */
    public void setNonce(int nonce) {
        this.nonce = nonce;
    }

    /**
     * Set the parameter, to do before sign and hash
     *
     * @param prevBlockHash the hash of previous in the chain
     */
    public void setPrevBlockHash(Hash prevBlockHash) {
        this.prevBlockHash = prevBlockHash;
    }

    /**
     * Sign the string of the block all info to do before hash the block
     *
     * @param privateKey private key of the miner
     * @param publicKey  i have to insert the public key of the miner in order to distribute the miner public key
     *                   maybe we can guarantee a different distribution of the miner public key
     * @return boolean, true if the signature is fine , false otherwise
     */
    public boolean setSignature(PrivateKey privateKey, PublicKey publicKey) throws BadAttributeValueExpException {
        //This method should be fast

        if (signature == null) {
            if (transactionsHash == null) { //calcolo solo la prima volta che firmo il blocco poi basta
                setTransactionsHash(); //Set transaction hash
            }

            assert publicKey != null : "Public key is null";
            this.minerPublicKey = Security.getStringFromPublicKey(publicKey);

            assert transactionsHash != null : "TransactionHash is null";

            try {
                //salvarsi l'hash delle info che nn siano nonce e timestamp e calcolare la firma solo sulle info vecchie piuù timestamp nuovo e nonce nuovo
                Hash h = Security.calculateHash(infoToSign().getBytes());

                signature = Security.signHash(privateKey, h);
                assert signature != null : "Signature not set";
                return true;
            } catch (AssertionError e) {
                Commons.logger.warning(e.getMessage());
                throw new BadAttributeValueExpException(e.getMessage());
            }

        }

        return true;
    }

    /**
     *
     * @param signature
     * @param publicKey
     * @return
     */
    public boolean setSignature(byte[] signature, String publicKey) {

        //This method should be fast
        if (transactionsHash == null) { //Calculate only the first time when sign the block
            setTransactionsHash(); //Set transaction hash
        }

        assert publicKey != null : "Public key is null";
        this.minerPublicKey = publicKey;

        assert transactionsHash != null : "TransactionHash is null";

        this.signature = new ObjectSignature(signature);
        return true;
    }

    /**
     * Insert the timestamp of block creation when mine a block to do before sign
     *
     * @param timeStamp the timestamp
     */
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * Calculate the hash of all the transaction and put in header.transaction hash
     * Call this method before sign the transaction
     * to do before sign
     */
    public void setTransactionsHash() {
        StringBuilder tmp = new StringBuilder();
        for (Transaction item : transactions) {
            tmp.append(item.allTransactionInfo());
        }

        this.transactionsHash = Security.calculateHash(tmp.toString().getBytes());
    }

    /**
     * @return the number of contained transactions
     */
    public int sizeTransactions() {
        return transactions.size();
    }

    /**
     * Return a string with all the interesting information inside the block
     * I can print also if not all the field are complete
     *
     * @return String info about the block
     */
    public String toString() {

        //Print anyway also when there aren't all the information
        StringBuilder tmp = new StringBuilder();
        final String def = "NOT SET"; //Default output we a field isn't not already set
        assert id >= 0 : "Bad id blocks print";
        tmp.append("\n#-----------------------------BLOCKS "); tmp.append(getID()); tmp.append(" ---------------------------------------");
        tmp.append("\n#\tHASH: "); tmp.append(blockHash);
        tmp.append("\n#\tPrevious Block hash    ");
        tmp.append((prevBlockHash != null) ? prevBlockHash : def);
        tmp.append("\n#\tMiner Address          ");
        tmp.append((minerAddress != null) ? minerAddress : def);
        tmp.append("\n#\tTimestamp              "); tmp.append(timeStamp);
        tmp.append("\n#\tNonce                 "); tmp.append(nonce);
        tmp.append("\n#\tReward for mining      "); tmp.append(minerReward);
        tmp.append("\n#\tDifficulty             "); tmp.append(difficulty);
        tmp.append("\n#\tTransaction Capacity:  "); tmp.append(maxTransaction);

        tmp.append("\n#\tTransactions list [" + transactions.size() + "]: ");
        for (Transaction t : transactions) {
            tmp.append("\n#\tID:   "); tmp.append(t.getID());
        }

//        tmp.append("\n#\tPublic key miner:      ");
//        tmp.append((minerPublicKey != null) ? minerPublicKey : def);
        tmp.append("\n#-----------------------------------------------------------------------------");

        return tmp.toString();
    }


    /**
     * Verify the integrity of the fields using the public key inside the block
     *
     * @return true if it's correct false otherwise
     */
    public boolean verify() {
        return verify(Security.getPublicKeyFromString(this.minerPublicKey));
    }

    /**
     * Verify the signature and the integrity of the block
     *
     * @param publicKey public key of the miner that have mined the block
     * @return true if all the fields are correct false otherwise
     */
    public boolean verify(PublicKey publicKey) {

        //Possible verify improvement
        assert publicKey != null : "PublicKey is null";
        assert signature != null : "Signature is null";
        Hash h = Security.calculateHash(infoToSign().getBytes());

        //CHECK THE SIGNATURE
        try {

            if (!Security.verifyByteArray(publicKey, h.toByteArray(), signature)) {
                Commons.logger.warning("Bad block signature in block id: " + this.getID());
                return false;
            }
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        //check if the number of contained transaction it's correct

        //check all the transactions only from syntax point of view

        return true;
    }


    /**
     * Method equals to compare two blocks, two block are equale if they have the same hash
     * @param block2 the second block that I want to compare to
     * @return true or false
     */
    @Override
    public boolean equals(Object block2) {
        return blockHash.compareTo(((IBlock)block2).getBlockHash()) == 0;
    }
}