package blockchain.blocks;

import blockchain.transactions.Transaction;
import utils.fields.Address;
import utils.fields.Hash;

import javax.management.BadAttributeValueExpException;
import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;


/**
 * ---------------------------BLOCK-----------------------------------------
 * Index univoco
 * <p>
 * CREATE A BLOCK
 * 1) IBlock block = new Block(int id);
 * 2) block.addTransaction(ITransaction transaction);
 * <p>
 * 3) ADD MINING TRANSACTION
 * <p>
 * 4) SIGN THE BLOCK
 * 5) HASH THE BLOCK
 */
public interface IBlock extends Serializable {


    /**
     * VALID:               Valid according to his predecessor
     * BADTRANSACTIONS:     Some transactions aren't valid
     * BADCONSTRUCTION:     The block isn't well construct (signature, hash or other field)
     */
    enum STATE {
        VALID, BADTRANSACTIONS, BADCONSTRUCTION, WRONGREWARD
    }

    /**
     * Insert a transaction into a block if it's not already inserted
     *
     * @param transaction ITransaction object to be inserted
     * @return false if the block is full or if the transaction it's already inserted, true otherwise
     */
    boolean addTransaction(Transaction transaction); //da fare prima di firmare

    /**
     * Method equals to compare two blocks, two block are equale if they have the same hash
     * @param block2 the second block that I want to compare to
     * @return true or false
     */
    boolean equals(Object block2);

    /**
     * Calculate the hash of the block and return it
     *
     * @return the hash of the block
     */
    Hash getBlockHash();


    /**
     * @return the id of the block
     */
    int getID();

    /**
     * @return minerAddress, null if it's not set
     */
    Address getMinerAddress();

    /**
     * @return the sum of fee contained in the block plus a remuneration for mining the block
     */
    float getMinerReward();

    /**
     *
     * @return the nonce of the block
     */
    int getNonce();

    /**
     * Get timestamp of block creations
     * @return timestamp of block creation
     */
    long getTimeStamp();

    /**
     * @return a linkedList with all the transactions contained into the block
     */
    LinkedList<Transaction> getTransactions();

    /**
     * Insert the block hash after verification
     * To do as the last action in the process of block creation
     * @param hash pre computed block hash
     * @return true if it's set correctly , false otherwise
     */
    void setBlockHash(Hash hash);

    void setDifficulty(int difficulty);     //da fare prima di hash

    /**
     * This method insert the miner address into the block, only miner must do this
     *
     * @param minerAddress the address of the miner
     */
    void setMinerAddress(Address minerAddress);  //da fare prima di firmare


    /**
     * Set the parameter nonce just for set the right mining hash difficulty
     *
     * @param nonce an int parameter to variate the block's hash
     */
    void setNonce(int nonce);  //To set before hash

    /**
     * Set the parameter, to do before sign and hash
     *
     * @param prevBlockHash the hash of previous in the chain
     */
    void setPrevBlockHash(Hash prevBlockHash); //To set before signature

    /**
     * Sign the string of the block all info to do before hash the block
     *
     * @param privateKey private key of the miner
     * @param publicKey  i have to insert the public key of the miner in order to distribute the miner public key
     *                   maybe we can guarantee a different distribution of the miner public key
     * @return boolean, true if the signature is fine , false otherwise
     */
    boolean setSignature(PrivateKey privateKey, PublicKey publicKey) throws BadAttributeValueExpException; //da fare prima di hash

    //To sign the zero block
    boolean setSignature(byte[] signature, String publicKey) throws BadAttributeValueExpException; //da fare prima di hash

    /**
     * Insert the timestamp of block creation when mine a block to do before sign
     *
     * @param timeStamp the timestamp
     */
    void setTimeStamp(long timeStamp); //To set before sign

    /**
     * Calculate the hash of all the transaction and put in header.transaction hash
     * Call this method before sign the transaction
     * to do before sign
     */
    void setTransactionsHash();

    /**
     * @return the number of contained transactions
     */
    int sizeTransactions();

    /**
     * Return a string with all the interesting information inside the block
     *
     * @return String info about the block
     */
    String toString(); //Print all block info if complete

    /**
     * Verify the integrity of the fields using the public key inside the block
     *
     * @return true if it's correct false otherwise
     */
    boolean verify();

    /**
     * Verify the signature and the integrity of the block
     *
     * @param publicKey public key of the miner that have mined the block
     * @return true if all the fields are correct
     */
    boolean verify(PublicKey publicKey);


    Hash getPrevBlockHash();
}