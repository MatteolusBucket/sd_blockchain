package blockchain;

import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import utils.fields.Address;
import utils.fields.Hash;
import utils.fields.IDTransaction;

import java.util.*;

/**
 * BLOCK CHAIN:
 *  short_description: each application have it's own local copy of this chain that it's equal end consistent in each node of
 *                      the network. This chain don't save personal information like private / public key of the wallet or
 *                      miner so if I want to add mine block or transaction I have to provide mine info.
 *
 *  I can receive a block and a transaction so the chain provide a method to control if this object are correct and so I can
 *  add to my chain
 *  ---> METHOD TO VERIFY IF A TRANSACTION OR A BLOCK ARE CORRECT
 *  ---> METHOD THAT VERIFY AND INSERT THE BLOCK INTO THE CHAIN
 *  ---> PRIVATE METHOD THAT ADD THE TRANSACTION NOT USED INTO A CACHE TO SPEED UP THE VERIFICATION
 *
 *  Useful method:
 *      - a way to print the info that my local block chain has
 *  ---> TOSTRING of the block chain
 *      - check integrity
 *  ---> CHECKINTEGRITY check if all the block chain are consistent
 *  ---> UPDATE LOCAL CACHE to see if there's error
 *  ---> getSize
 *
 *
 */
public interface IBlockChain
{
    /** This status regards the transaction into a block-chain context, so it refers to a specific transaction and a specific block hash
     * VALID:           the transaction is well formed and the transaction inputs does not spend before
     * USED:            some input field was already used before
     * NOT_VALIDATED:   the transaction could try to spend some input not already validated in a block
     */
    enum TRANSACTION_STATE{ VALID , USED , NOT_VALIDATED }

    /**
     * If the block is correct according to its previous block, insert into the block-chain
     * @param block that I want to verify and add if it's correct
     * @return true if correct and add, false otherwise
     */
    boolean addBlock(Block block);

    /**
     * @return the current mining difficulty of the chain
     */
    int getMiningDifficulty();

    /**
     *
     * @param id index of block required
     * @return null if something bad or if there's no block with that id otherwise return a list with all the block with that id
     */
    List<Block> getBlock(int id);

    /**
     * This method return the hash of the last block inserted
     * @return hash of the last block, null if some dangerous error occurs!
     */
    Hash getHashLastBlock();

    /**
     * Return the number of blocks contained into my block chain
     * block(0)---block(n)
     * @return n+1 where n is the index of last block into the longest branch
     */
    int size();

    /**
     * A method that show all the info contained in my block chain
     * If I have some not already set parameters I print nothing so I can call it always
     * @return String with block chain info
     */
    String toString();

    /**
     * This method is used to check if a block is valid or not, valid means that we can add in our blockchain
     * @param block the block that we want to insert
     * @return a block STATE that can be: VALID, BADTRANSACTION, BADCONSTRUCTION OR WRONGREWARD
     */
    IBlock.STATE verifyBlock(IBlock block);

    /**
     * Return the available transaction for a specific address according to the first longest branch
     *
     * @param address the owner of transactions
     * @return a treeMap with the available transactions
     */
    TreeMap<IDTransaction, Float> getAddressAvailableIDTransactions(Address address);

    /**
     * This method calculate the mining difficult into the blockchain to decide the best one to ensure
     * a mining time constant. The miner should lunch this command before mining each block.
     */
    void updateMiningDifficulty();

    /**
     * Check the correctness of a transaction according to a specific block hash before mining into a block
     * @param transaction The transaction that I want to check
     * @param blockHash The block hash, at which the transaction must be correct
     * @return a TRANSACTION_STATE that allow to check the type of error or correctness
     */
    TRANSACTION_STATE verifyTransactionBeforeMiningIntoBlock(ITransaction transaction, Hash blockHash);

    /**
     * Check the correctness of a transaction according to the longest branch
     * Not recomanded fot well verification but only approximal
     * @param transaction The transaction that I want to check
     * @return a TRANSACTION_STATE that allow to check the type of error or correctness
     */
    TRANSACTION_STATE verifyTransactionLongestBranch(ITransaction transaction);
}