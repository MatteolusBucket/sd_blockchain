package blockchain.chainElement;

import blockchain.transactions.ITransaction;
import blockchain.transactions.Transaction;
import utils.fields.Address;
import utils.fields.IDTransaction;

import java.io.Serializable;

public class ChainRing implements Serializable
{
    private Transaction transaction;    //Transactions
    private int idBlock;                //Block in which the transaction is validated
    private byte[] outputAvailable;

    public ChainRing(Transaction transaction, int idBlock) {
        this.transaction = transaction;
        this.idBlock = idBlock;

        //For each output, save if it's expendable or not
        //When all the output of a transaction are spend, remove the chain ring
        outputAvailable = new byte[transaction.getOutputTransaction().size()];
        for (int i = 0; i < transaction.getOutputTransaction().size(); i++) {
            outputAvailable[i] = 1; //As default output is valid when they were inserted
        }
    }

    public ChainRing(ChainRing i) {
        this.transaction = i.transaction;
        this.idBlock = i.idBlock;

        //For each output, save if it's expendable or not
        //When all the output of a transaction are spend, remove the chain ring
        outputAvailable = new byte[transaction.getOutputTransaction().size()];
        for (int j = 0; j < transaction.getOutputTransaction().size(); j++) {
            if (i.outputAvailable[j] == 0) {  //Build cache as the predecessor
                outputAvailable[j] = 0;
            } else {
                outputAvailable[j] = 1;
            }
        }
    }

    /**
     * Set as spend an output of the address given as parameter
     * @param address the address that we want to refer
     */
    public void setUsed(Address address) {

        for (int i = 0; i < transaction.getOutputTransaction().size(); i++) {
            if (transaction.getOutputTransaction().get(i).getAddress().toString().equals(address.toString())) { //mettere il compare to nei address

                outputAvailable[i] = 0; //Set the state spend
            }
        }

    }

    /**
     * Used to check the output availability in the transaction with multiple output
     * @param address that have output available or not
     * @return the expendable amount, 0 if not expendable
     */
    public float isUsed(Address address) {
        for (int i = 0; i < transaction.getOutputTransaction().size(); i++) {
            if (transaction.getOutputTransaction().get(i).getAddress().toString().equals(address.toString())) { //mettere il compare to nei address
                //Return the expendable amount
                return outputAvailable[i] == 0 ? 0 : transaction.getOutputTransaction().get(i).getAmount();
            }
        }
        return 0; //Default is used
    }

    /**
     * Used to check the output availability in the transaction with only one output
     * @return the expendable amount, 0 if not expendable
     */
    public float isUsed() {

        return outputAvailable[0] == 0 ? 0 : transaction.getOutputTransaction().get(0).getAmount();
    }

    public IDTransaction getID() {
        return transaction.getID();
    }

    public ITransaction getTransaction(){
        return transaction;
    }

    public int getIdBlock(){
        return idBlock;
    }

    public byte[] getOutputAvailable(){
        return outputAvailable;
    }
}
