package blockchain.chainElement;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import utils.fields.Address;
import utils.fields.Hash;
import utils.fields.IDTransaction;

import java.util.TreeMap;

public interface ICacheTransaction {

    /**
     * Show the state of the cache with a human readable form
     * @return string with state
     */
    @Override
    String toString();

    /**
     * To use with standard transaction
     * This method verify if an output of a specific transaction is available according
     * to the state of blockchain on block with hash specified.
     * @param idTransaction Id of transaction to verify
     * @param address Address of the receiver of the output
     * @param hash The hash of block that we refer
     * @return true if it's spendable false otherwise
     */
    boolean isAvailable(IDTransaction idTransaction, Address address, Hash hash);

     /**
     * To use only for transaction of type MinerReward
     * This method verify if a minerReward is available according to the state of blockchain
     * on block with hash specified.
     * @param idTransaction Id of transaction to verify
     * @param hash The hash of block that we refer
     * @return true if it's spendable false otherwise
     */
    boolean isAvailable(IDTransaction idTransaction, Hash hash);


    /**
     * Get the transaction inside the cache of block with hash equal to hash parameter
     *
     * @param idTransaction id transaction
     * @param hash          hash of the block that contains the transaction
     * @return the required transaction
     */
    ITransaction getTransaction(IDTransaction idTransaction, Hash hash);


    /**
     * Create a new cache updated until the block ( not included )  that I've passed
     *
     * @param block block
     * @return true if all it's correct false if there's already a cache for that block and this is an ERROR
     */
    boolean updateCache(IBlock block);


    /**
     * Give a list of all transaction available for a specific address refer to
     * a specific block hash.
     * @param address The owner of the output transaction
     * @param hash the hash of the block to which the state is refer
     * @return a list with available transaction or null
     */
    TreeMap<IDTransaction, Float> getAddressAvailableTransactions(Address address, Hash hash);


}
