package blockchain.chainElement;

import blockchain.transactions.ITransaction;
import blockchain.transactions.Transaction;
import utils.Commons;
import utils.fields.Address;
import utils.fields.IDTransaction;
import utils.fields.InputTransaction;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * This is the cache of transaction coherent from the block zero to the block
 * that is associated to.
 *
 * Only the output of this transaction are expendable;
 * A transaction is spent when all of it's output are spent.
 */
public class LeafCache implements Serializable {

    private TreeMap<IDTransaction, ChainRing> cacheTransaction;

    public LeafCache() {
        cacheTransaction = new TreeMap<>();
    }

    public LeafCache(TreeMap<IDTransaction, ChainRing> itemTreeMap) {
        cacheTransaction = new TreeMap<>();

        for (Map.Entry<IDTransaction, ChainRing> element : itemTreeMap.entrySet()) {
            ChainRing item = new ChainRing(element.getValue());

            cacheTransaction.put(element.getKey(), item);
        }
    }

    /**
     * This method return the the index of block in witch the transaction it is validated
     *
     * @param idTransaction
     * @return -1 if it's not present , id block if the transaction it's validated
     */
    private synchronized int containsKey(IDTransaction idTransaction) {

        if (cacheTransaction.containsKey(idTransaction)) {          //the transaction is present and it's stored in the block id
            return cacheTransaction.get(idTransaction).getIdBlock();
        }

        return -1; //The transaction  it's not present so maybe I have to add it if
    }

    /**
     * This method add transaction if it's not already inserted into the cache
     *
     * @param transaction transaction to be inserted
     * @param idBlock     index of block where the transaction it's validated
     * @return true if the transaction is inserted correctly, false otherwise
     */
    public synchronized boolean addTransaction(Transaction transaction, int idBlock) {

        if (containsKey(transaction.getID()) >= 0) {
            //the transaction it's already inserted
            return false;
        } else {
            //new transaction I can add it
            ChainRing temp = new ChainRing(transaction, idBlock);

            if (transaction.getType() == ITransaction.TYPE.BLOCKREWARD) { //Does not have input to insert
                cacheTransaction.put(transaction.getID(), temp);

            } else {

                //Here I assume that the transaction it is expendable
                //Remove from available the transaction that are no more expendable
                for (InputTransaction input : transaction.getInputTransaction()) {
                    //No more available
                    //Retrieve, from the cache transaction, the transaction spend
                    // as input and set the output address to false
                    try{
                        cacheTransaction.get(input.getID()).setUsed(input.getAddress());
                    }catch (NullPointerException e){

                        Commons.logger.warning("Try to insert a not valid transaction in cache");
                        return false;
                    }
                }
                cacheTransaction.put(transaction.getID(), temp);
            }

            return true;
        }
    }

    /**
     * Return the transaction with id specified in parameter
     * @param idTransaction id
     * @return null if not exist
     */
    public synchronized ITransaction getTransaction(IDTransaction idTransaction) {
        ChainRing item = cacheTransaction.get(idTransaction);
        return item == null ? null : item.getTransaction();
    }

    /**
     *
     * @return a string that contains all the transaction's id that cache contains
     */
    public synchronized String toString() {
        StringBuilder tmp = new StringBuilder();
        for (ChainRing t : cacheTransaction.values()) {
            tmp.append("\n\tBlock: " + t.getIdBlock() + "\tID_t: " + t.getTransaction().getID());

            for (int i = 0; i < t.getOutputAvailable().length; i++) {
                byte b = t.getOutputAvailable()[i];
                Address address = t.getTransaction().getOutputTransaction().get(i).getAddress();
                if (b == 1) {
                    tmp.append("\n\t\tValid add: " + address);
                } else {
                    tmp.append("\n\t\tNot Valid add: " + address);
                }
            }
        }

        return tmp.toString();
    }

    /**
     * Check if the output of a transaction is available to spent.
     * @param idTransaction that i want to spent
     * @param address the output address to check availability
     * @return if the specified output is available or not
     */
    public synchronized boolean isAvailable(IDTransaction idTransaction, Address address) {

        //In the same transaction, can not be two output with the same address
        try {
            ChainRing chainRing = cacheTransaction.get(idTransaction);
            if(chainRing != null)
            {
                return !(chainRing.isUsed(address) == 0);
            }
        } catch (NullPointerException e) {
            Commons.logger.warning("Some error occur in isAvailable there's some bug!!" + e);
            e.printStackTrace();
        }

        Commons.logger.warning("Default isAvailable");
        return false; //Default
    }

    /**
     * As the method with address parameter but for the minerreward transaction where there's only one output
     * Check if the output of a transaction is available to spent.
     * @param idTransaction that i want to spent
     * @return if the specified output is available or not
     */
    public synchronized boolean isAvailable(IDTransaction idTransaction) {

        //In the same transaction, can not be two output with the same address
        try {
            ChainRing chainRing = cacheTransaction.get(idTransaction);
            if(chainRing != null)
            {
                return !(chainRing.isUsed() == 0);
            }
        } catch (NullPointerException e) {
            Commons.logger.warning("Some error occur in isAvailable there's some bug!!" + e);
            e.printStackTrace();
        }

        return false; //Default
    }

    /**
     * Get a list with all the transaction that have at least one output expendable
     * @param address the owner of transactions
     * @return a TreeMap<IDTransaction, Float> with transactions id
     */
    public TreeMap<IDTransaction, Float> getAddressAvailableTransactions(Address address) {

        TreeMap<IDTransaction, Float> availableTransactions = new TreeMap<>();

        for ( Map.Entry<IDTransaction, ChainRing> tmp : cacheTransaction.entrySet()) {
            float amount = tmp.getValue().isUsed(address);
            if (amount > 0) availableTransactions.put(tmp.getKey(), amount);
        }

        Commons.logger.fine("Transactions available for address: " + address + "\n" + availableTransactions);
        return availableTransactions;

    }

    /**
     * @return the cache transactions
     */
    public TreeMap<IDTransaction, ChainRing> getCacheTransaction(){
        return cacheTransaction;
    }

}

