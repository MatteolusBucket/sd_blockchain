package blockchain.chainElement;

import blockchain.blocks.IBlock;
import blockchain.transactions.ITransaction;
import blockchain.transactions.Transaction;
import org.apache.commons.lang3.SerializationUtils;
import utils.Commons;
import utils.fields.Address;
import utils.fields.Hash;
import utils.fields.IDTransaction;
import utils.fields.MultiValuedArray;

import java.util.TreeMap;


/**
 * A transaction is valid if:
 *  - signatures are valid
 *  - the input used must be mined into ancient block
 *  - the input must be expendable
 *
 * In the cache, save for each transaction the state of each output state.
 */
public class CacheTransaction implements ICacheTransaction {

    //Save a treeMap of cache, each cache has as key the hash of the block that it's referred to.
    //Each cache is valid from the block of the hash until the zero block.
    private TreeMap<Hash, LeafCache> treeMap;

    public CacheTransaction(MultiValuedArray chain) {
        treeMap = new TreeMap<>(); //Initialize the structure

        //I have to insert only the cache of block zero because the other block insertion create the cache autonomously
        if (chain.get(0) != null) {

            LeafCache leafCache = new LeafCache();

            for (Transaction t : chain.get(0).get(0).getTransactions()) {
                if (!leafCache.addTransaction(t, chain.get(0).get(0).getID())) {
                    Commons.logger.warning("leafCache.addTransaction return false!");
                }
            }

            //Put only when it's finished
            treeMap.put(chain.get(0).get(0).getBlockHash(), leafCache);
        }

    }

    /**
     * Show the state of the cache with a human readable form
     * @return string with state
     */
    public String toString() {
        StringBuilder temp = new StringBuilder();

        for (Hash h : treeMap.keySet()) {
            temp.append("\nCache related with block hash: ");
            temp.append(h);
            temp.append("\n");
            temp.append(treeMap.get(h));
        }

        return temp.toString();
    }

    /**
     * To use with standard transaction
     * This method verify if an output of a specific transaction is available according
     * to the state of blockchain on block with hash specified.
     * @param idTransaction Id of transaction to verify
     * @param address Address of the receiver of the output
     * @param hash The hash of block that we refer
     * @return true if it's spendable false otherwise
     */
    public boolean isAvailable(IDTransaction idTransaction, Address address, Hash hash) {
        try {

            LeafCache temp = this.getLeafCacheHash(hash);
            if (temp == null) {
                Commons.logger.info("getLeafCacheHash(hash) \nhash: " + hash.toString() + "non disponibile!");
                return false;
            }

            return temp.isAvailable(idTransaction, address);

        } catch (NullPointerException e) {
            Commons.logger.warning("Bad things happened!" + e);
            e.printStackTrace();
        }

        return false;

    }

    /**
     * To use only for transaction of type MinerReward
     * This method verify if a minerReward is available according to the state of blockchain
     * on block with hash specified.
     * @param idTransaction Id of transaction to verify
     * @param hash The hash of block that we refer
     * @return true if it's spendable false otherwise
     */
    public boolean isAvailable(IDTransaction idTransaction, Hash hash) {

        try {

            LeafCache temp = this.getLeafCacheHash(hash);
            return temp != null && temp.isAvailable(idTransaction);
//            if (temp == null) return false;
//            return temp.isAvailable(idTransaction);

        } catch (NullPointerException e) {
            Commons.logger.warning("Bad things happened!" + e);
            e.printStackTrace();
        }

        return false;

    }


    /**
     * Get the transaction inside the cache of block with hash equal to hash parameter
     *
     * @param idTransaction id transaction
     * @param hash          hash of the block that contains the transaction
     * @return the required transaction
     */
    public ITransaction getTransaction(IDTransaction idTransaction, Hash hash) {
        return treeMap.get(hash).getTransaction(idTransaction);
    }


    /**
     * Create a new cache updated until the block ( not included )  that I've passed
     *
     * @param block block
     * @return true if all it's correct false if there's already a cache for that block and this is an ERROR
     */
    public boolean updateCache(IBlock block) {

        //Ensure that not previous cache already exist
        if (treeMap.containsKey(block.getBlockHash())) {
            Commons.logger.severe("Bad the code try to create an already available cache!");
            return false;
        } else {

            //Take the predecessor cache, clone it and update
            LeafCache leafCache = getLeafCacheHash(block.getPrevBlockHash()); //Predecessor cache

            assert leafCache != null : "LeafCache = null check method getLeafCacheHash";

            if (leafCache == null) {
                Commons.logger.severe("ERROR during the request of the cache from the block: " + block);
                return false;
            } else {

                for (Transaction t : block.getTransactions()) {
                    if (!leafCache.addTransaction(t, block.getID())) {
                        Commons.logger.warning("leafCache.addTransaction return false!");
//                        return false;
                    }
                }

                //Put only when it's finished
                treeMap.put(block.getBlockHash(), leafCache);

                return true;
            }
        }
    }

    /**
     * Give a list of all transaction available for a specific address refer to
     * a specific block hash.
     * @param address The owner of the output transaction
     * @param hash the hash of the block to which the state is refer
     * @return a list with available transaction or null
     */
    public TreeMap<IDTransaction, Float> getAddressAvailableTransactions(Address address, Hash hash) {
        LeafCache temp = treeMap.get(hash);
        assert temp != null : "This is a bug, the leaf must be already created before!";
        return temp.getAddressAvailableTransactions(address);
    }

    /**
     * Return the cache associated with the block with the given hash or create it.
     *
     * @param hash the hash of desired block
     * @return the valid cache
     */
    private LeafCache getLeafCacheHash(Hash hash) {
        if (treeMap.containsKey(hash)) {
            Commons.logger.fine("Found cache for hash: " + hash);
            return SerializationUtils.clone(treeMap.get(hash)); // return a copy of the object
        } else {
            //acquisire una lista dal hash a ritroso salvando gli hash fino al blocco zero
            //costruire la cache a partire dal hash la cui cache è presente e risalire
            //if I implement the deletion of the old piece of cache I also have to implement this method
            Commons.logger.severe("Ask the cache of hash: " + hash + "  that it's not present!");

            //crea la cache da zero o retrocedendo fino alla prima cache valida che trovo e da li la ricostruisco
            //TODO IMPLEMENT METHOD BUILD OLD CACHE
            Commons.logger.severe("TODO THE METHOD");
            return null;
        }
    }

}
