package blockchain;

import blockchain.chainElement.CacheTransaction;
import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import incoming.IIncomingManager;
import blockchain.transactions.ITransaction;
import blockchain.transactions.Transaction;
import utils.AddressMemory;
import utils.Commons;
import utils.Security;
import utils.fields.*;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;

public class BlockChain implements IBlockChain {
    private int miningDifficulty = 50;

    private MultiValuedArray chain;
    private CacheTransaction cache;

    public BlockChain(boolean newBlockChain) {
        //This is a fake parameter just for JSON

        chain = new MultiValuedArray();     //Initialize the chain structure

        //CREATE THE ZERO BLOCK
        if (createBlockZero()) {
            Commons.logger.info("Block zero was created!");
        } else {
            Commons.logger.warning("Block zero not properly created");
        }

        cache = new CacheTransaction(chain); //Initialize the cache

        updateMiningDifficulty();
    }

    /**
     * @return the current mining difficulty of the chain
     */
    public synchronized int getMiningDifficulty() {
        //With the method updateMiningDifficulty() we expect that the saved difficulty it's correct
        // I just use this value.. I expect it's coherent
        return miningDifficulty;
    }

    /**
     * This method calculate the mining difficult into the blockchain to decide the best one to ensure
     * a mining time constant. The miner should lunch this command before mining each block.
     */
    public synchronized void updateMiningDifficulty() {
        //TODO: 3/22/18 implement Mining Difficult algorithm
        //We have to look inside the chain in the last X block and calculate the mean time of mining
        miningDifficulty = 6;
    }

    /**
     * Check if it can add the block in the chain
     * check block verifyBlock
     * check if the block contains the previous block
     *
     * @param block that i want to check
     * @return true if ok false if the block is not ok or if the chain doesn't not contain the previous block hash
     */
    private boolean checkBlockBeforeAdd(Block block) {

        boolean statement = false;

        switch (verifyBlock(block)) {

            case VALID:
                statement = true;
                break;

            case BADTRANSACTIONS:
                Commons.logger.info("NOT PROBLEM... try to add a block with BADTRANSACTIONS: " + block.toString());
                break;

            case BADCONSTRUCTION:
                Commons.logger.info("NOT PROBLEM... try to add a BADCONSTRUCTION in block: " + block.toString());
                statement = false;
                break;

            case WRONGREWARD:
                Commons.logger.info("NOT PROBLEM... try to add a block with wrong miner reward: " + block.toString());
                statement = false;
                break;

            default:
                Commons.logger.severe("verifyBlock return a not valid state, CODE ERROR");
                statement = false;
        }

        if (statement) {

            if (containsBlockHash(block.getID() - 1, block.getPrevBlockHash())) {
                return true;
            } else {
                Commons.logger.info("Try to add a block but its prev block hash it's not contained into the chain!");
                return false;
            }
        }

        return false;
    }

    /**
     * Check if the chain contains the searched block with a specified hash and id
     */
    private boolean containsBlockHash(int blockId, Hash blockHash) {
        //Check if the chain contains the prev block
        if (chain.get(blockId) == null) {
            return false;
        } else {
            for (IBlock iBlock : chain.get(blockId)) {
                if (iBlock.getBlockHash().equals(blockHash)) {
                    return true; //The chain contains the previous block hash
                }
            }
        }

        return false;
    }

    /**
     * If the block is correct according to its previous block, insert into the block-chain
     * @param block that I want to verify and add if it's correct
     * @return true if correct and add, false otherwise
     */
    public synchronized boolean addBlock(Block block) {

        if (block != null && containsBlockHash(block.getID(), block.getBlockHash())) {
            //OK
            Commons.logger.info("Try to add a block that it's already inserted into the chain!");
            return true;
        } else {

            if (checkBlockBeforeAdd(block)) {

                //ADD BLOCK INTO THE CHAIN STRUCTURE
                if (!chain.add(block.getID(), block)) {
                    return false;
                }

                //Update the transaction cache with the block's transactions
                if (!cache.updateCache(block)) {
                    Commons.logger.severe("Inserted block with hash: " + block.getBlockHash() + " the block is correct for the chain but not for the cache!");
                    return false;
                }

                Commons.logger.info("We have insert the block: \n" + block);
                return true;

            }

        }

//        Commons.logger.severe("False from addBlock!! id: "+block.getID());
        return false;
    }

    /**
     * This method is used to check if a block is valid or not, valid means that we can add in our blockchain
     * @param block the block that we want to insert
     * @return a block STATE that can be: VALID, BADTRANSACTION, BADCONSTRUCTION OR WRONGREWARD
     */
    public synchronized IBlock.STATE verifyBlock(IBlock block) {

        IBlock.STATE stateDefault = IBlock.STATE.VALID;

        assert block != null : "Block = null";

        //Check miner reward
        if (block.getMinerReward() + 0.0001f < (block.getTransactions().size() - 1)) {
            //The block reward must be at least #numberOfTransaction - 1
            return IBlock.STATE.WRONGREWARD; //wrong miner reward
        }

        if (!block.verify()) { //verify the field of the block
            Commons.logger.warning("BAD BLOCK Construction\n" + block);
            stateDefault = IBlock.STATE.BADCONSTRUCTION;
        } else { //Assuming the block construction is correct

            //Check all the transactions validity according with the prevBloch hash
            for (ITransaction t : block.getTransactions()) {

                switch (t.getType()) {

                    case BLOCKREWARD:
                        break;

                    case INITIAL:

                        if (block.getID() != 0) {
                            stateDefault = IBlock.STATE.BADTRANSACTIONS;
                            Commons.logger.severe("Initial transaction within a block different from block zero");
                        }

                        break;

                    case STANDARD:

                        TRANSACTION_STATE state = verifyOldTransaction(t, block.getPrevBlockHash());

                        if (state == null || state != TRANSACTION_STATE.VALID) {
                            Commons.logger.warning("Bad transaction: " + t.getID() + "\nComplete transaction's info: " + t + "\nInto the block: \n" + block + "\n");
                            stateDefault = IBlock.STATE.BADTRANSACTIONS;
                        }

                        break;

                }
            }
        }


        return stateDefault;
    }

    /**
     * Verify that the input of a transaction is spendable
     *
     * @param transaction of type standard!
     * @param hash        of the block where the availability is referred to
     * @return
     */
    private synchronized TRANSACTION_STATE verifyOldTransaction(ITransaction transaction, Hash hash) {

        if (transaction.getType() == ITransaction.TYPE.STANDARD) { //TYPE 1

            //Array that contains the list of public keys to verify the signature
            ArrayList<PublicKey> publicKeys = new ArrayList<>();

            //Search the public keys in the transaction contained is input
            for (InputTransaction inputTransaction : transaction.getInputTransaction()) {

                assert inputTransaction != null : "input transaction = null!!";

                //Search transaction with this id in input transactions
                if (cache.isAvailable(inputTransaction.getID(), inputTransaction.getAddress(), hash)) { //se non è stata spesa

                    ITransaction t = cache.getTransaction(inputTransaction.getID(), hash); //Mi faccio dare la transazione che dovrebbe contenere in output la public key che mi serve

                    try {

                        Address address = inputTransaction.getAddress(); //Ho l'indirizzo where I have to search
                        assert t != null : "t = null!"; //test
                        PublicKey publicKey = t.getOutputKeyFromAddress(address);

                        publicKeys.add(publicKey);

                    } catch (NullPointerException e) { //This transaction isn't valid
                        Commons.logger.warning("Problem: " + e);
                        return TRANSACTION_STATE.USED;
                    }
                } else { //Already spent

                    Commons.logger.warning("Input already spent, id: " + inputTransaction.getID());
                    return TRANSACTION_STATE.USED;
                }
            }

            //Check the transaction's signature using the public key contained in the transaction coming from
            boolean isOk = transaction.verify(publicKeys);
            return isOk ? TRANSACTION_STATE.VALID : TRANSACTION_STATE.USED;

        }else {
            Commons.logger.severe("You have call the method verify old transaction in a transaction of type different from standard!");
            return null;
        }
    }

    /**
     * Check the correctness of a transaction according to a specific block hash before mining into a block
     * @param transaction The transaction that I want to check
     * @param blockHash The block hash, at which the transaction must be correct
     * @return a TRANSACTION_STATE that allow to check the type of error or correctness
     */
    public synchronized TRANSACTION_STATE verifyTransactionBeforeMiningIntoBlock(ITransaction transaction, Hash blockHash) {

        switch (transaction.getType()){
            case STANDARD:
                return verifyOldTransaction(transaction, blockHash);

            case INITIAL:
                return TRANSACTION_STATE.VALID;

            case BLOCKREWARD:
                return TRANSACTION_STATE.VALID;

                default:

                    return TRANSACTION_STATE.NOT_VALIDATED;
        }

//        return verifyOldTransaction(transaction, blockHash);
    }

    /**
     * Check the correctness of a transaction according to the longest branch
     * Not recomanded fot well verification but only approximal
     * @param transaction The transaction that I want to check
     * @return a TRANSACTION_STATE that allow to check the type of error or correctness
     */
    public synchronized TRANSACTION_STATE verifyTransactionLongestBranch(ITransaction transaction) {
        switch (transaction.getType()){
            case STANDARD:
                //controlla la transazione se è in accordo con la cache del primo blocco inserito del ramo più lungo
                return verifyOldTransaction(transaction, chain.get(chain.size() - 1).get(0).getBlockHash());

            case INITIAL:
                return TRANSACTION_STATE.VALID;

            case BLOCKREWARD:
                return TRANSACTION_STATE.VALID;

            default:

                return TRANSACTION_STATE.NOT_VALIDATED;
        }
    }

    /**
     * Return the number of blocks contained into my block chain
     * block(0)---block(n)
     * @return n+1 where n is the index of last block into the longest branch
     */
    public synchronized int size() {
        return chain.size();
    }

    /**
     *
     * @param id index of block required
     * @return null if something bad or if there's no block with that id otherwise return a list with all the block with that id
     */
    public synchronized List<Block> getBlock(int id) {
        assert id >= 0 : "ID must be positive";
        return chain.get(id);
    }

    /**
     * This method return the hash of the last block inserted
     * @return hash of the last block, null if some dangerous error occurs!
     */
    public synchronized Hash getHashLastBlock() {

        Hash temp = (chain.get(chain.size() - 1).get(0)).getBlockHash();
        assert temp != null : "GetHashLastBlock return null value!";

        return temp;
    }

    /**
     * A method that show all the info contained in my block chain
     * If I have some not already set parameters I print nothing so I can call it always
     * @return String with block chain info
     */
    public synchronized String toString() {

        StringBuilder tmp = new StringBuilder();

        for (int i = 0; i < chain.size(); i++) {
            for (IBlock b : chain.get(i)) {
                tmp.append("\n#");
                tmp.append(b.getID());
            }
        }

        return tmp.toString();
    }

    /**
     * Return the available transaction for a specific address according to the first longest branch
     *
     * @param address the owner of transactions
     * @return a treeMap with the available transactions
     */
    public synchronized TreeMap<IDTransaction, Float> getAddressAvailableIDTransactions(Address address) {

        return cache.getAddressAvailableTransactions(address, chain.get(chain.size() - 1).get(0).getBlockHash());
    }

    /**
     * Create the block zero and return true if it's correct false otherwise
     * This is a private method called only into the constructor
     *
     * @return true if creation is correct
     */
    private synchronized boolean createBlockZero() {

        String publKeyStr = "3059301306072A8648CE3D020106082A8648CE3D03010703420004F32146939570E9672EFE30B2589000B76019F06F1345863A0924F292681C723C14EEB11F760C6B86D42AE7DBD14B26FDE9547B8BD98F5B594A0A153CC1FE3CDC";
        PublicKey publicKey0 = Security.getPublicKeyFromString(publKeyStr);    //initialize the public key

        String privKeyStr = "3041020100301306072A8648CE3D020106082A8648CE3D0301070427302502010104202CE4285954D4BCE9072307141EAC42AB0F900E926FCBA4C27388D8B969579E7C";

        Block zeroBlock = new Block(0);

        zeroBlock.setNonce(10);    //Random but different from 0

        //This is the money of the founders
        ArrayList<String> idStr = new ArrayList<>();
        idStr.add("61F65B499141549003F05E44F698579EF73AF7B64D4227B6C04A3A0A5C60AF48");
        idStr.add("1FB22F5A9DB2F775267ADAE4F53F294858AF30C0A99B39CBE2A4BBB49FBBF560");
        idStr.add("250E4E99C2DB95F2117FC211B19337DEE4EE1CDEC6F9ECD1D19F897C152B9DED");
        idStr.add("24ADBE13897996C49038D4B34DC70A7ACA850369E9336CA175D6E39192084894");
        idStr.add("E2A7B126BC449E2CD2EDDB1F33CE728F58C309AC522504EB1F8F9065FA3BF2CB");
        idStr.add("5F472B3F65E7DC7AD70ACEEC7CF650B7947A53810B30B84B61A7486F46E6E5A3");
        idStr.add("5D6D04D702CBCFB3B6C2DE13AF4E6EADE208E45AA1D5C7B96F91B397983F3FF3");
        idStr.add("953C42BDECAA86C621AC0C3DA6EE61A17A9C232B600F8D371C7CE6399E8A98C8");
        idStr.add("30BB147AAD0C254E50663B66EF2B1EC008161AB4AE46C43DE8D35A16B72EF86A");
        idStr.add("7C66DDA852ECF4B365872796F6BBA2DDA6F3A24AF1822D71E585008FCA0A69BD");

        AddressMemory addressMemory = new AddressMemory();

        //Create initial transactions
        int address = 0; //Address from 1 to 10

        for (String item : idStr) {
            address += 1;
            Transaction t = new Transaction(new IDTransaction(item), 0, ITransaction.TYPE.INITIAL);
            Address a = new Address(address);
            t.addOutput(a, addressMemory.getPublickey(a.toString()), 100.0f);
            zeroBlock.addTransaction(t);
        }

        zeroBlock.setPrevBlockHash(new Hash("0000000000000")); //Random
        zeroBlock.setMinerAddress(new Address(0)); //Random
        zeroBlock.setTimeStamp(3);
        zeroBlock.setDifficulty(3);


        zeroBlock.setSignature(Security.hexStringToByteArray("3045022100A1630F55DEC9271771CF5892B69B192143392307762C0C287179BF4B2A97330602205AEB69B66C0FD1F871065EF45D131D15BAC6F8C43430CC48C02AC1370A1EEAFF"), Security.getStringFromPublicKey(publicKey0));

        //Make hash of the signature and the info and insert in the code as something fixed
        // TODO: 3/19/18 set hash block zero
        Hash blockHash = zeroBlock.getBlockHash();
        assert blockHash != null : "BlockHash = null";
        zeroBlock.setBlockHash(blockHash); //Calculate and set block's hash

        chain.add(0, zeroBlock);

        return true;
    }

    /**
     * This is a test for the blockchain
     *
     * @param incomingManager  the instance of incoming manager
     * @param address          address of the miner
     * @param numb_transaction number of transactions that I want generate
     */
    @Deprecated
    public void testBlockchain(IIncomingManager incomingManager, int address, int numb_transaction) {

        System.out.println("BEGIN TEST! MINER MUST HAVE ADDRESS = 1");

        AddressMemory a = new AddressMemory();

        Random rand = new Random();

        int addressSender = address;

        for (int j = 1; j <= numb_transaction; j++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (address == -1) {
                addressSender = rand.nextInt(9) + 1;
            }

            Transaction t = new Transaction();

            ArrayList<PrivateKey> privateKeys = new ArrayList<>();

            //Insert some available for address transaction
            int numberIn = rand.nextInt(2) + 1;
            int sumIn = 0;
            TreeMap<IDTransaction, Float> transaction = this.getAddressAvailableIDTransactions(new Address(addressSender));
            assert transaction != null : "Transaction = null!";
            Iterator<Map.Entry<IDTransaction, Float>> x = transaction.entrySet().iterator();
            for (int i = 0; i < numberIn; i++) {
                if (x.hasNext()) {
                    Map.Entry<IDTransaction, Float> s = x.next();
                    t.addInput(s.getKey(), new Address(addressSender), s.getValue());
                    sumIn += s.getValue();
                    privateKeys.add(AddressMemory.getPrivateKey(new Address(addressSender).toString()));
                }
            }

            //Add output
            for (int i = 1; i < 3; i++) {
                int addressReceiver = rand.nextInt(10) + 1;
                if (sumIn > 1) {
                    float somma = rand.nextInt(sumIn - 1) + 1;
                    sumIn -= somma;
                    if (somma > 0) {
                        t.addOutput(new Address(addressReceiver), AddressMemory.getPublickey(new Address(addressReceiver).toString()), somma);
                    }
                }
            }

            if (t.getInputTransaction().size() > 0 && t.getOutputTransaction().size() > 0) {
                if (t.finish(privateKeys)) {
                    System.out.println("Create Transaction: " + t);
                    incomingManager.onReceive(t);
                }
            }
        }
    }

    @Deprecated
    public synchronized int getLastBlockId() {
        List<Block> blocks = chain.get(chain.size() - 1);
        if (blocks == null)
            return 0;
        else
            return blocks.get(0).getID();
    }
}
