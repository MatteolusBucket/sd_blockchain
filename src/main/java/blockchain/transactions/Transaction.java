package blockchain.transactions;

import utils.Commons;
import utils.Security;
import utils.fields.*;

import java.io.Serializable;
import java.security.*;
import java.util.*;

public class Transaction implements ITransaction, Serializable{

    static long serialVersionUID = 1;       //Used by java for serialization

    private Hash header_hash = null;        //Hash of the header
    private ArrayList<ObjectSignature> sign_list = null; //List of signature

    //BEGIN HEADER
    private IDTransaction id;
    private long timestamp = 0;
    private float sum_In   = 0;
    private List<InputTransaction>  input_list;  //Transactions used as input
    private float sum_Out = 0;
    private List<OutputTransaction> output_list; //Transactions used as output
    private TYPE type;
    //END HEADER

    /**
     * CONSTRUCTOR
     *
     * @param id        transaction id
     * @param timestamp a timestamp
     * @param type      a type of transaction 1 standard and 2 for mining reward
     */
    public Transaction(IDTransaction id, long timestamp, TYPE type) {

        this.id = id;
        this.timestamp = timestamp;
        this.input_list = new ArrayList<>();
        this.output_list = new ArrayList<>();
        this.type = type;

        if (type == TYPE.INITIAL){
            sum_In = 100.0f;        //Just to try
        }
    }

    /**
     * CONSTRUCTOR default type 1 =  standard transaction
     */
    public Transaction(){ //default type 1

        this.id = generateId(UUID.randomUUID());
        this.timestamp = System.currentTimeMillis();
        this.input_list = new ArrayList<>();
        this.output_list = new ArrayList<>();
        this.type = TYPE.STANDARD;
    }

    /**
     * CONSTRUCTOR TYPE 2
     */
    public Transaction(Address minerAddress, PrivateKey privateKey , PublicKey publicKey, float amount){

        this.id = generateId(UUID.randomUUID());
        this.timestamp = System.currentTimeMillis();
        this.input_list = new ArrayList<>();
        this.output_list = new ArrayList<>();
        this.type = TYPE.BLOCKREWARD;

        this.addInput(id, minerAddress, amount);
        this.addOutput(minerAddress, publicKey, amount);
        this.finish(new ArrayList<>(Arrays.asList(privateKey)));
    }

    public IDTransaction getID() {
        return id;
    }

    public float getSumIn() {
        if (sum_In < 0 ){
            Commons.logger.warning("Negative sum_In was found");
        }
        return sum_In;
    }

    public float getSumOut() {
        if (sum_Out < 0 ){
            Commons.logger.warning("Negative sum_out was found");
        }
        return sum_Out;
    }

    public float getFee() {
        //FEE SHOULD BE POSITIVE
        return sum_In - sum_Out;

    }

    public TYPE getType(){
        return type;
    }

    public void addInput(IDTransaction id_transaction, Address address, float amount) {

        if (amount < 0){
            Commons.logger.warning("Bad argument input, amount: "+amount+ " is not permitted");
        } else { //all is ok
            //Verifico tra tutte quelle già inserite che nn c'è ne siano di uguali
            for (InputTransaction in : input_list ) {

                //se c'è un altro input con lo stesso address e stesso id do errore
                if ((in.getID().compareTo(id_transaction) == 0) && in.getAddress().toString().equals(address.toString())){
                    Commons.logger.warning("Try two insert two equal transactions as input!");
                    return;
                }
            }

            InputTransaction temp = new InputTransaction(id_transaction, address, amount);
            input_list.add(temp);
            sum_In = sum_In + amount;
        }

    }


    public boolean addOutput(Address address, PublicKey public_Key, float amount) {

        //There must be no two output address with the same address
        for (OutputTransaction output : output_list) {
            if (output.getAddress().equals(address)){
                Commons.logger.warning("Try to add two output transaction with the same address in the transaction: "+this.toString()+" second address: "+address.toString());
                return false;
            }
        }

        //Check if we can spend this amount
        float availability = getSumIn()-getSumOut();
        if (availability >= amount && amount > 0){
            OutputTransaction temp = new OutputTransaction(address, public_Key, amount);
            output_list.add(temp);
            sum_Out += amount;

        }else  {
            Commons.logger.warning("Bad amount: "+amount+ " is not permitted, available inside transaction: "+availability );
            return false;
        }

        return true;
    }

    public long getTimeStamp() {
        return timestamp;
    }


    public Hash getHash() throws NullPointerException {
        if (header_hash == null) {
            throw new NullPointerException("Hash not initialized");
        } else {
            return header_hash;
        }
    }

    public Hash calculateHeaderHash() {
        return Security.calculateHash(infoTosign().getBytes());
    }

    public boolean finish(ArrayList<PrivateKey> privKeys)
    {

        //Generate the hash and put into variable
        header_hash = calculateHeaderHash();
        this.sign_list = new ArrayList<>();

        //I need as inputsize as privatekeys size
        if (privKeys.size() != input_list.size()){
            Commons.logger.warning("Numero di input e private keys differenti!");
            System.exit(1);
            return false;
        }

        for (int i = 0; i < input_list.size(); i++) {

            ObjectSignature a = signHash(privKeys.get(i));
            sign_list.add(i, a);
        }
        return true; //If it's all ok

    }

    public boolean verify(ArrayList<PublicKey> puclickeys)
    {

        //Verify the hash correctness
        if (header_hash.compareTo(this.calculateHeaderHash()) == 0) {
            //The hash is correct
        } else {
            Commons.logger.warning("Hash wrong");
            return false; //Wrong hash
        }

        //Check if all the signature are correct, according with the given publicKeys
        PublicKey publicKey;
        ObjectSignature signature;
        byte[] hash = header_hash.toByteArray();

        if (type == TYPE.STANDARD){
            //Have to verify all the list of signature

            if (input_list.size() != puclickeys.size()) {
                Commons.logger.warning("Problema nel numero di chiavi pubbliche rispetto alle transazioni usate in input!");
                return false;
            }

            for (int i = 0; i < input_list.size(); i++) {

                try {

                    publicKey = puclickeys.get(i);
                    signature = sign_list.get(i);

                    if (Security.verifyByteArray(publicKey, hash, signature)) { //correct

                    } else { //incorrect

                        Commons.logger.warning("Signature " + i + " wrong verification!!");
                        return false;
                    }

                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                    return false;
                } catch (SignatureException e) {
                    e.printStackTrace();
                    return false;
                }
            }


        } else if (type == TYPE.BLOCKREWARD){//Have to verify only one signature

                publicKey = puclickeys.get(0);
                signature = sign_list.get(0);

                try {
                    if (Security.verifyByteArray(publicKey, hash, signature)) { //correct

                    } else { //incorrect
                        Commons.logger.warning("Signature in type 0 wrong verification!!");
                        return false;
                    }
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                    return false;
                } catch (SignatureException e) {
                    e.printStackTrace();
                    return false;
                }


        }

        return true;

    }

    public void random() {

        //ID and also timestamp are already setting
        Random rand = new Random();

        ArrayList<PrivateKey> privList = new ArrayList<>();
        ArrayList<PublicKey>  publList = new ArrayList<>();

        try {
            KeyPairGenerator kpGen = KeyPairGenerator.getInstance("EC");
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            kpGen.initialize(256, random);
            //Add n input random transaction
            int n = rand.nextInt(3);
            if (n == 0) n = 1;

            for (int i = 0; i < 1; i++) {
                KeyPair kp = kpGen.generateKeyPair();
                privList.add(kp.getPrivate());
                publList.add(kp.getPublic());
                Address ad = new Address(rand.nextInt());
                addInput(generateId(), ad, rand.nextInt(20));
            }

            //Add y output random transaction
            int y = rand.nextInt(3);
            if (y == 0) y = 1;
            for (int i = 0; i <= y; i++) {
                KeyPair kp = kpGen.generateKeyPair();
                Address ad = new Address(rand.nextInt());
                addOutput(ad, kp.getPublic(), rand.nextInt(20));
            }

            this.finish(privList);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        type = TYPE.STANDARD; //Standard type default

    }


    public String toString()
    {
        StringBuilder tmp = new StringBuilder();
        String def = "NOT SET";
        tmp.append("\n#----------------------TRANSACTION---------------------------------");
        if (id == null) tmp.append("\n#\tID: " + def);
        else tmp.append("\n#\tID: " + id+"  Type: " + type);
        tmp.append("\n#\tsum_In: " + sum_In+"  sum_Out: " + sum_Out+ "  Fee: "+ getFee());
        tmp.append("\n#\tInput_list: ");
        if (input_list.isEmpty())  tmp.append("\n#\t\t"+ def);
        else {
            for (InputTransaction item : input_list) {
                tmp.append("\n#\t\t" + item);
            }
        }

        tmp.append("\n#\tOutput_list: ");

        if (output_list.isEmpty()) tmp.append("\n#\t\t"+def);
        else {
            for (OutputTransaction item : output_list){
                tmp.append("\n#\t\t"+item);
            }
        }

        tmp.append("\n#-------------------------------------------------------------------\n\n\n");

        return tmp.toString();
    }

    /**
     * This is a method that return a string with all the transaction info that we must sign
     * @return String with all transaction info to sign
     * @throws NullPointerException If some field it's not proper initialized
     */
    private String infoTosign() throws NullPointerException{

        StringBuilder tmp = new StringBuilder();

        tmp.append(""+id+""+timestamp+""+sum_In+""+sum_Out+""+type);
        //All input transactions
        for (InputTransaction item : input_list){
            tmp.append(item);
        }

        //All output transactions
        for (OutputTransaction item : output_list){
            tmp.append(item);
        }

        return tmp.toString();
    }

    /**
     * This method return all the info contained in transaction, also hash and signature
     * @return  String with all the important info
     * @throws NullPointerException if some value it's not properly set
     */
    public String allTransactionInfo()  throws NullPointerException{
        StringBuilder tmp = new StringBuilder();
        tmp.append(infoTosign());
        tmp.append(header_hash);

        //Add signature info
        if (type != TYPE.INITIAL){
            for (ObjectSignature item : sign_list){
                tmp.append(item);
            }
        }

        return tmp.toString();
    }

    public List<InputTransaction> getInputTransaction(){
        return input_list;
    }

    public PublicKey getOutputKeyFromAddress(Address address){

        for (OutputTransaction o : output_list) {
            if (o.getAddress().toString().equals(address.toString())){
                return o.getPublic_Key();
            }
        }

        return null;
    }

    public  List<OutputTransaction> getOutputTransaction(){
        return output_list;
    }

    //PRIVATE METHOD ----------------------------------------------------------------------------
    private IDTransaction generateId(UUID uuid)
    {
        Hash temp = Security.calculateHash(uuid.toString().getBytes());

        if (temp != null) {
            return new IDTransaction(temp.toString());
        } else {
            Commons.logger.warning("There's a problem in ID hash generation");
            System.exit(1);
        }

        return null;
    }

    @Deprecated
    private IDTransaction generateId() {

        //Generate with timestamp plus a random number
        Random rand = new Random();
        long magicNumber = rand.nextLong();
        long time = System.currentTimeMillis();

        StringBuilder tmp = new StringBuilder();
        tmp.append(magicNumber);
        tmp.append(time);

        Hash temp = Security.calculateHash(tmp.toString().getBytes());

        if (temp != null) {
            return new IDTransaction(temp.toString());
        } else {
            Commons.logger.warning("There's a problem in ID hash generation");
            System.exit(1);
        }

        return null;
    }

    private ObjectSignature signHash(PrivateKey privateKey) {

        return Security.signHash(privateKey, getHash());
    }
    //END PRIVATE METHOD ----------------------------------------------------------------------------
}

