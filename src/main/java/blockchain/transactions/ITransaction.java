package blockchain.transactions;


import utils.fields.*;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

/**
 * TRANSACTION OBJECT
 * fee = the reward for the miner = sum_in - sum_out
 * TYPE:   0 = the big bang transaction created only with the code
 * 1 = the standard transactions DEFAULT
 * 2 = the transaction created by the block mining, the amount is the block fees plus the generation crypto rate(hardcoded?)
 * <p>
 * <p>
 * CONSTRUCTOR TYPE:
 * 1) require timestamp and id as parameters
 * 2) no parameters are required
 * <p>
 * <p>
 * CREATE A TRANSACTION:
 * ITransaction a = new Transaction(); //default a standard transaction
 * a.addInput(IDTransaction id_transaction, Address address, int amount); for each input transaction
 * a.addOutput(Address address, PublicKey public_Key, int amount); for each output transaction
 * a.finish(PrivateKey[] my_keys);  this method set signature and put the hash into transaction,
 * as parameter it needs the list of private keys of the input
 * transactions in the same order
 * P.S. TO CREATE A TRANSACTION REWARDS CREATE A TRANSACTION WITH PARAMETER 2 IN THE CONSTRUCTOR
 * <p>
 * <p>
 * VERIFY A TRANSACTION:
 * transaction.verify(PublicKey[] my_public_keys);
 * with the public keys of input transactions as parameters
 */
public interface ITransaction extends Serializable {
    /**
     * INITIAL      transaction used only in block zero
     * STANDARD     default transactions
     * BLOCKREWARD  transaction for give te ward to the miner
     */
    enum TYPE {
        INITIAL, STANDARD, BLOCKREWARD
    }

    IDTransaction getID();

    float getSumIn();

    float getSumOut();

    float getFee();

    /**
     * @param id_transaction ID della transazione della quale voglio usare parte dell'output
     * @param address        indirizzo del destinatario dell'output della transazione dell'ID sopra indicata
     * @param amount         amount of t deve essere inferiore o meglio = alla somma indicata in output con address address alla transazione ID indicata sopra
     */
    void addInput(IDTransaction id_transaction, Address address, float amount);

    /**
     * @param address    The receiver address of the money.
     * @param public_Key PublicKey of the receiver address
     * @param amount     sum that the address is going to receive
     * @return true the output is added correctly false otherwise
     */
    boolean addOutput(Address address, PublicKey public_Key, float amount);

    long getTimeStamp();

    /**
     * @return the precomputed hash if it's just do otherwise return error
     * @throws NullPointerException if the hash it's not done yet
     */
    Hash getHash();                                         //Return the hash pre saved

    Hash calculateHeaderHash();                             //Calculate the hash and return

    /**
     * This method insert the correct header hash and sign the transaction with the private keys
     *
     * @param privKeys a list of private key to sign the transaction, order as the owner order of input of transaction
     * @return true if correct false otherwise
     */
    boolean finish(ArrayList<PrivateKey> privKeys);


    boolean verify(ArrayList<PublicKey> my_public_keys);    //Verify if all is setting correct

    /**
     * This method return a visual representation of the info contained in transaction
     * Avoid to use this method for hash or signature because here the field it's not mandatory
     */
    @Override
    String toString();

    /**
     * Initialize the transaction with random data, only for test
     */
    @Deprecated
    void random();

    /**
     * Get the type of transaction
     *
     * @return could be one of this: INITIAL, STANDARD, BLOCKREWARD
     */
    TYPE getType();


    /**
     * To use in hash transactions in the blocks
     *
     * @return a string with all important info
     */
    String allTransactionInfo();

    /**
     * This method return a list with the id of input transaction, useful to retrieve the publicKeys from the chain
     *
     * @return a list with id of input transactions
     */
    List<InputTransaction> getInputTransaction();

    PublicKey getOutputKeyFromAddress(Address address);

    List<OutputTransaction> getOutputTransaction();
}

