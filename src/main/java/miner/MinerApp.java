package miner;

import blockchain.*;
import incoming.IncomingManager;
import incoming.WatchDog;
import utils.Commons;
import utils.Security;
import utils.dataManager.DataInit;
import utils.fields.Address;
import uiManager.UiManager;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * This class is the main application for the miner
 */
public class MinerApp extends Thread implements IMinerApp
{
    private BlockChain blockChain;
    private Address minerAddress;
    private IncomingManager incomingManager;
    private UiManager uiManager;
    private MinerManager miner;
    private WatchDog watchDog;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private static final int THREAD_WAIT_ATTEMPTS = 10;

    /**
     * Constructor of the class
     * @param initialHosts is the array with the initial host to connect
     * @param localPort is the listening port for the app
     * @param address is the miner address
     * @param pathDirectoryBlockChain is the local path of the directory that contains the blockchain file
     * @param fileNameBlockChain is the filename of the blockchain
     * @param pathDirectoryConfig is the local path of the directory that contains the config file
     * @param fileNameConfig is the filename of the config
     */
    public MinerApp(String[] initialHosts, int localPort, Address address, String pathDirectoryBlockChain, String fileNameBlockChain, String pathDirectoryConfig, String fileNameConfig)
    {
        // used to laod data from the local directory
        DataInit dataInit = new DataInit(pathDirectoryBlockChain, fileNameBlockChain, pathDirectoryConfig, fileNameConfig, address);

        // if this data are already defined it return them otherwise it creates new data
        minerAddress = dataInit.initAddress();
        publicKey = dataInit.initPublicKey();
        privateKey = dataInit.initPrivateKey();
        blockChain = dataInit.initBlockChain();

        // we create the instance of the classes -> later we'll start these threads
        // the connection manager
        incomingManager = new IncomingManager(this, blockChain, initialHosts, localPort, dataInit);
        // the WatchDog
        watchDog = new WatchDog(this, blockChain, null, incomingManager, dataInit);
        // the miner manager -> availableProcessors() returns the number of the threads in our pc
        miner = new MinerManager(this, blockChain, Runtime.getRuntime().availableProcessors(), incomingManager, dataInit);
        // the user interface manager
        uiManager = new UiManager(this, blockChain);
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * This function starts all the threads we have instanced above
     */
    @Override
    public void run()
    {
        incomingManager.updateBlockChain();
        incomingManager.start();

        waitThread(incomingManager);

        if(incomingManager.isAlive())
        {
            watchDog.start();

            waitThread(watchDog);

            if(watchDog.isAlive())
            {
                miner.start();

                waitThread(miner);

                if(miner.isAlive())
                {
                    incomingManager.setMiner(miner);
                    uiManager.start();

                    waitThread(uiManager);

                    if(uiManager.isAlive())
                    {
                        try
                        {
                            // before stops this thread we wait the death of all the other threads :)
                            miner.join();
                            watchDog.join();
                            incomingManager.join();
                            uiManager.join();
                        }
                        catch(InterruptedException e)
                        {
                            Commons.logger.severe("Interrupt in the MinerApp!");

                            uiManager.stopManager();
                            miner.stopMiner();
                            watchDog.stopWatchDog();
                            incomingManager.stopManager();
                        }
                    }
                    else
                    {
                        Commons.logger.severe("Can't start the uiManager!");

                        miner.stopMiner();
                        watchDog.stopWatchDog();
                        incomingManager.stopManager();
                    }
                }
                else
                {
                    Commons.logger.severe("Can't start the MinerManager!");

                    watchDog.stopWatchDog();
                    incomingManager.stopManager();
                }
            }
            else
            {
                Commons.logger.severe("Can't start the WatchDog!");

                incomingManager.stopManager();
            }
        }
        else
        {
            Commons.logger.severe("Can't start the IncomingManager!");
        }
    }

    /**
     * This function waits the creation of the thread
     * @param thread is the thread to wait
     */
    private void waitThread(Thread thread)
    {
        int attempt = 0;

        // we have a max attempts -> we don't want to wait forever :)
        while(!thread.isAlive() && attempt++ < THREAD_WAIT_ATTEMPTS)
        {
            try
            {
                Thread.sleep(100);
            }
            catch(InterruptedException e)
            {
                Commons.logger.severe("Interrupt in the MinerApp!");
            }
        }
    }

    /**
     * @return the private key of the minerad
     */
    protected PrivateKey getPrivateKey()
    {
        return this.privateKey;
    }

    /**
     * @return the public key of the miner
     */
    protected PublicKey getPublicKey(){
        return this.publicKey;
    }

    /**
     * @return the miner address
     */
    public Address getAddress()
    {
        return minerAddress;
    }

    /**
     * This function stops this manager
     */
    public void stopMinerApp()
    {
        miner.stopMiner();
        uiManager.stopManager();
        watchDog.stopWatchDog();
        incomingManager.stopManager();
    }

    /**
     * Show the status of the wallet from the transaction confirmation or not point of view
     * @return a list with all the transactions regarding the wallet and their status
     */
    public String getStatus()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n\t\tAddress: " + minerAddress);
        stringBuilder.append("\n\t\tPublic Key: " + Security.getStringFromPublicKey(publicKey));

        return stringBuilder.toString();
    }

    /**
     * Print the status of the network
     */
    public void networkStatus()
    {
        incomingManager.networkStatus();
    }
}