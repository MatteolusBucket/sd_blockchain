package miner;

public interface IMinerManager
{
    /**
     * This method stops the miner manager
     */
    void stopMiner();

    /**
     * This function stops the mining process
     */
    void stopMining();

    /**
     * @return the id of the current mining block
     */
    int getIDMiningBlock();
}