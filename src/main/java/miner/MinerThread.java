package miner;

import blockchain.blocks.Block;
import blockchain.transactions.Transaction;
import utils.Commons;
import utils.fields.Hash;

import javax.management.BadAttributeValueExpException;
import java.util.Date;
import java.util.Iterator;

/**
 * This class represents the miner thread
 * It receives a block and computes the hash
 */
public class MinerThread extends Thread implements IMinerThread
{
    private int id;
    private Block block;
    private MinerManager miner;

    /**
     *
     * @param minerManager {@link MinerManager}
     * @param block is the block to mine {@link Block}
     * @param id is the id of this thread
     */
    public MinerThread(MinerManager minerManager, Block block, int id)
    {
        this.miner = minerManager;
        this.id = id;
        this.block = block;
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * This function mines a block
     */
    @Override
    public void run()
    {
        Hash hashBlock;
        long start, end;
        int nonce = 0;
        boolean checkHash = false;
        String info;

        start = System.currentTimeMillis();

        // while we discover the hash AND no one has already discovered
        while(!checkHash && !miner.isStopThreads())
        {
            block.setNonce(nonce);

            // if we surpass the threshold -> reset
            if(nonce % Block.NONCE_THRESHOLD == 0)
            {
                block.setTimeStamp(System.currentTimeMillis());
                nonce = 0;

                // debug
                end = System.currentTimeMillis();
                Commons.logger.info("THREAD[" + id + "] is mining block " + block.getID() + "... time = " + ((end - start) / 1000.0) + "s");
            }
            nonce++;

            try
            {
                block.setSignature(miner.getPrivateKey(), miner.getPublicKey());
            }
            catch(BadAttributeValueExpException e)
            {
                Commons.logger.warning("Error during the signature of the block");
                miner.setStopThreads(true);
            }

            hashBlock = block.getBlockHash();
            checkHash = checkHash(hashBlock);
            if(checkHash)
            {
                synchronized(miner)
                {
                    if(!miner.isStopThreads())
                    {
                        end = System.currentTimeMillis();

                        block.setBlockHash(hashBlock);
                        miner.setWinnerThread(id);

                        try
                        {
                            Thread.sleep(300); // the sleep is necessary to set correctly the winner thread id
                        }
                        catch(InterruptedException e)
                        {
                            Commons.logger.warning("Interrupt during the setting of the winner thread id");
                        }

                        miner.setStopThreads(true);

                        // the following are info for debug
                        info = "\n[THREAD " + id + "]"
                                +"\n\tBLOCK ID: " + block.getID()
                                +"\n\tBLOCK TIMESTAMP: " + block.getTimeStamp()
                                +"\n\tTIME: " + new Date(block.getTimeStamp())
                                +"\n\tHASH : " + hashBlock
                                +"\n\tMINING TIME: " + ((end - start) / 1000.0)
                                +"\n\tNONCE: " + block.getNonce()
                                +"\n\tTRANSACTIONs ID [SIZE: " + block.sizeTransactions() + "]";

                        Iterator<Transaction> iterator = block.getTransactions().iterator();
                        while(iterator.hasNext())
                        {
                            info += "\n\t\t" + iterator.next().getID();
                        }

                        Commons.logger.info(info);
                    }
                    else
                    {
                        break; // exit loop whether someone other thread has found the hash before
                    }
                }
            }
        }
    }

    /**
     * This function checks the hash of the block
     * @param hashBlock hash to check
     * @return true or false
     */
    private boolean checkHash(Hash hashBlock)
    {
        try
        {
            return hashBlock.toString().startsWith(miner.getMiningDifficultyToString());
        }
        catch(NullPointerException e)
        {
            Commons.logger.warning("The method checkHash in MinerThread is not working");
        }

        return false;
    }

    /**
     * @return the mined block if this is the winner thread, null otherwise
     */
    public Block getMinedBlock()
    {
        if(miner.getWinnerThread() == id)
        {
            return block;
        }

        return null;
    }
}
