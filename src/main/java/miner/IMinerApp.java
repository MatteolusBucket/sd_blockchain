package miner;

import utils.fields.Address;

public interface IMinerApp
{
    /**
     * This function stops this manager
     */
    void stopMinerApp();

    /**
     * Show the status of the wallet from the transaction confirmation or not point of view
     * @return a list with all the transactions regarding the wallet and their status
     */
    String getStatus();

    /**
     * @return the miner address
     */
    Address getAddress();

    /**
     * Print the status of the network
     */
    void networkStatus();
}
