package miner;

import blockchain.BlockChain;
import blockchain.blocks.Block;
import blockchain.blocks.IBlock;
import blockchain.IBlockChain;
import incoming.IIncomingContainer;
import incoming.IIncomingManager;
import blockchain.transactions.Transaction;
import incoming.IncomingManager;
import org.apache.commons.lang3.SerializationUtils;
import utils.Commons;
import utils.Security;
import utils.Utils;
import utils.dataManager.DataInit;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * This class receives transactions, creates a block and computes its hash
 * Mining a block means calculate its hash and to reach this goal we use many threads that work parallely
 * on the block with the same transactions but with a different timestamp in order to avoid working with the same parameters
 */
public class MinerManager extends Thread implements IMinerManager
{
    private Block currentMiningBlock;
    private IIncomingManager incomingManager;
    private  DataInit dataInit;
    private IIncomingContainer incomingTransactionsHandler;
    private IIncomingContainer copyIncomingTransactionsHandler;
    private LinkedList<Transaction> usedTransactions;
    private MinerThread minerThreads[];
    private int numThreads;
    private boolean stopThreads = true;
    private boolean stopMiner = false;
    private int winnerThread = -1;
    private boolean isInterrupted = false;
    private String miningDifficulty = "0000000000000000000000000000000000000000000"; // very difficult by default
    private MinerApp minerApp;
    private BlockChain blockChain;
    private final int TIMER_REFRESH_TRANSACTIONS_MS = 500;
    private final int TIMER_WAIT_TRANSACTIONS_MS = 10000;

    /**
     * Constructor of the class
     * @param minerApp {@link MinerApp}
     * @param blockChain is the chain {@link BlockChain}
     * @param numThreads is the number of thread used to mining
     * @param incomingManager is the manager of the network {@link IncomingManager}
     * @param dataInit is the saver class used by the blockchain {@link DataInit}
     */
    public MinerManager(MinerApp minerApp, BlockChain blockChain, int numThreads, IIncomingManager incomingManager, DataInit dataInit)
    {
        minerThreads = new MinerThread[numThreads];

        this.numThreads = numThreads;
        this.minerApp = minerApp;
        this.blockChain = blockChain;
        this.incomingManager = incomingManager;
        this.dataInit = dataInit;
        this.incomingTransactionsHandler = incomingManager.getTransactions();

        usedTransactions = new LinkedList<>();
    }

    /**
     * This method overrides the Thread.start()
     * We check if the chain is updated or not
     */
    @Override
    public void start()
    {
        if(!incomingManager.isUpdatedBlockChain())
        {
            Commons.logger.severe("Before start the MinerManager you must start IncomingManager");

            minerApp.stopMinerApp();
        }
        else
        {
            super.start();
        }
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * This thread manages the mining of a block and the {@link MinerThread} used to work in parallel
     */
    @Override
    public void run()
    {
        boolean isAdded;
        Utils utils = new Utils();

        while(!stopMiner)
        {
            if(isStopThreads())
            {
                // creation of the block
                createBlock(minerApp);
                // this method could interrupt the thread
                setTransactionSyncWithTimer();

                if(!isInterrupted)
                {
                    // mining method waits until all the threads are over
                    miningBlock(numThreads);

                    // at this point one thread discovers the hash of the block
                    if(winnerThread >= 0)
                    {
                        // stop all the working threads
                        setStopThreads(true);

                        // insert the block in the blockchain
                        synchronized(blockChain)
                        {
                            isAdded = blockChain.addBlock(minerThreads[winnerThread].getMinedBlock());
                        }

                        if(!isAdded)
                        {
                            // Restoring the transactions if the add fails
                            restoreTransactions(minerThreads[winnerThread].getMinedBlock());

                            Commons.logger.info("Failed to insert the block");
                        }
                        else
                        {
                            System.out.println("\n\n");
                            utils.printCenterConsole("", "*");
                            utils.printCenterConsole("  " + minerApp.getAddress().toString() + "  ", "*");
                            utils.printCenterConsole("  Congratulation.. this block is your :)  ", "*");
                            utils.printCenterConsole("  NEW REWARD!!!  ", "*");
                            utils.printCenterConsole("", "*");
                            utils.printCenterConsole("", "*");
                            System.out.println("\n\n");

                            // saving the blockchain in the local data file
                            if(!dataInit.saveBlockChain(blockChain))
                            {
                                Commons.logger.severe("Saving blockchain.. Failed!");
                            }

                            // spread the block on the network
                            incomingManager.spreadBlock(minerThreads[winnerThread].getMinedBlock());
                        }
                    }
                    else // if winnerThread == -1 means someone stops the mining process and we need to reinsert the transactions that we were computing in the incoming handler
                    {
                        // Restoring the transactions of the stopped block
                        restoreTransactions(currentMiningBlock);
                    }
                }
            }
        }
    }

    /**
     * This method reinserts the transactions of a block in incomingTransactionsHandler
     * @param block
     */
    private void restoreTransactions(IBlock block)
    {
        // Restoring the transactions
        for(Transaction transaction : block.getTransactions())
        {
            // Type 2 transaction is special transaction for the miner reward
            if(transaction.getType() != Transaction.TYPE.BLOCKREWARD)
            {
                synchronized(incomingTransactionsHandler)
                {
                    if(!incomingTransactionsHandler.onReceive(transaction))
                    {
                        Commons.logger.warning("Unable to restore transactions");
                    }
                }
            }
        }
    }

    /**
     * @return the private key of the miner
     */
    protected PrivateKey getPrivateKey()
    {
        return minerApp.getPrivateKey();
    }

    /**
     * @return the public key of the miner
     */
    protected PublicKey getPublicKey()
    {
        return minerApp.getPublicKey();
    }

    /**
     * This method updates the incomingTransactionsHandler removing the transactions that the miner is computing
     * At the end it creates a copy of the incomingTransactionsHandler used for not locking for to much time the incomingTransactionsHandler
     * that is the our transactions buffer
     */
    private void refreshNewTransaction()
    {
        // removing the transactions already inserted in the block
        for(Transaction transaction : currentMiningBlock.getTransactions())
        {
            synchronized(incomingTransactionsHandler)
            {
                if(incomingTransactionsHandler.containsKey(transaction.getID()))
                {
                    if(!incomingTransactionsHandler.remove(transaction.getID()))
                    {
                        Commons.logger.warning("Unable to remove the transaction");
                    }
                }
            }
        }

        // removing the used transactions
        while(usedTransactions.size() > 0)
        {
            synchronized(incomingTransactionsHandler)
            {
                incomingTransactionsHandler.remove(usedTransactions.removeFirst().getID());
            }
        }

        synchronized(incomingTransactionsHandler)
        {
            copyIncomingTransactionsHandler = SerializationUtils.clone(incomingTransactionsHandler);
        }
    }

    /**
     * This method inserts the transactions inside the block until it isn't full
     * Or whether there aren't enough transactions it waits for TIMER_REFRESH_TRANSACTIONS_MS milliseconds and then computes
     * the hash of the block if there is at least one transaction inside
     */
    private void setTransactionSyncWithTimer()
    {
        long startTime, endTime;
        Iterator keys;
        Transaction transaction;
        IBlockChain.TRANSACTION_STATE stateTransaction;

        // we create a copy of the transactions
        synchronized(incomingTransactionsHandler)
        {
            copyIncomingTransactionsHandler = SerializationUtils.clone(incomingTransactionsHandler);
        }

        keys = copyIncomingTransactionsHandler.keySetIterator();

        startTime = System.currentTimeMillis();
        // we need 1 location for the miner reward transaction
        while(currentMiningBlock.sizeTransactions() < Block.MAX_TRANSACTIONS - 1)
        {
            if(keys.hasNext())
            {
                startTime = System.currentTimeMillis();

                transaction = (Transaction)copyIncomingTransactionsHandler.get(keys.next());

                synchronized(blockChain)
                {
                    stateTransaction = blockChain.verifyTransactionBeforeMiningIntoBlock(transaction, currentMiningBlock.getPrevBlockHash());
                }

                switch(stateTransaction)
                {
                    case NOT_VALIDATED:
                    {
                        // still in incomingTransactionsHandler
                    } break;
                    case USED:
                    {
                        // we will remove this transactions in the refresh method
                        usedTransactions.add(transaction);
                    } break;
                    case VALID:
                    {
                        if(!currentMiningBlock.addTransaction(transaction))
                        {
                            Commons.logger.warning("Unable to insert the transactions inside the block");
                        }
                        else
                        {
                            Commons.logger.info("Adding transaction inside the block {Block size = " + currentMiningBlock.sizeTransactions() + "}");
                        }
                    } break;
                }
            }
            else // if the buffer is empty, we wait before the refresh
            {
                try
                {
                    // sleeping before the refresh
                    Thread.sleep(TIMER_REFRESH_TRANSACTIONS_MS);
                }
                catch(InterruptedException e)
                {
                    isInterrupted = true;
                    Commons.logger.warning("Interrupted during the refresh of transactions");
                    break;
                }

                refreshNewTransaction();
                keys = copyIncomingTransactionsHandler.keySetIterator();

                endTime = System.currentTimeMillis();


                if(endTime - startTime > TIMER_WAIT_TRANSACTIONS_MS && currentMiningBlock.sizeTransactions() > 0)
                {
                    break; // the timer is over and now we can compute a block with dim < MAX_TRANSACTIONS - 1 and at least one transaction
                }
            }
        }

        if(!isInterrupted)
        {
            refreshNewTransaction();

            // at this point we are sure that the block is full or the timer is over so we can add the reward for the miner and then start the mining
            setMinerTransactionReward();
        }
    }

    /**
     * This method adds the miner transaction reward in the block
     */
    private void setMinerTransactionReward()
    {
        Transaction minerTransaction = new Transaction(currentMiningBlock.getMinerAddress(), minerApp.getPrivateKey(), minerApp.getPublicKey(), currentMiningBlock.getMinerReward());

        if (!currentMiningBlock.addTransaction(minerTransaction))
        {
            Commons.logger.warning("Unable to insert its transaction reward");
        }
    }

    /**
     *  This method creates and sets the initial params of the block
     * @param minerApp used for getting some params {@link MinerApp}
     */
    private void createBlock(MinerApp minerApp)
    {
        synchronized(blockChain)
        {
            currentMiningBlock = new Block(blockChain.size());
            currentMiningBlock.setPrevBlockHash(blockChain.getHashLastBlock());
            currentMiningBlock.setMinerAddress(minerApp.getAddress());
            currentMiningBlock.setDifficulty(blockChain.getMiningDifficulty());
            currentMiningBlock.setTimeStamp(System.currentTimeMillis());
        }
    }

    /**
     * @return a string with a number of zeros equal to the int passed
     */
    private String getZerosString(int zeros)
    {
        StringBuilder difficulty = new StringBuilder();

        for (int i = 0; i < zeros; i++)
        {
            difficulty.append("0");
        }

        return difficulty.toString();
    }

    /**
     * This method starts mining the currentMiningBlock
     * @param numThreads number of threads used for the mining
     */
    private void miningBlock(int numThreads)
    {
        Block copyBlock;

        synchronized(blockChain)
        {
            miningDifficulty = getZerosString(currentMiningBlock.getDifficulty());
        }

        setWinnerThread(-1);
        setStopThreads(false);

        System.out.println("Mining block " + currentMiningBlock.getID() + ".. ");

        for(int i = 0; i < numThreads; i++)
        {
            // For each thread we create a copy which differs from the others just by the timestamp
            // In this manner each thread works on different blocks
            copyBlock = SerializationUtils.clone(currentMiningBlock);
            copyBlock.setTimeStamp(copyBlock.getTimeStamp() + i);

            // start mining threads
            minerThreads[i] = new MinerThread(this, copyBlock, i);
            minerThreads[i].start();
        }

        // we wait until all the threads have finished
        waitMiningThreads();
    }

    /**
     * This method sets the id of the thread that has discovered the hash of the block
     * @param winnerThread id of the thread
     */
    protected synchronized void setWinnerThread(int winnerThread)
    {
        this.winnerThread = winnerThread;
    }

    /**
     * @return the id of the winner thread
     */
    protected synchronized int getWinnerThread()
    {
        return winnerThread;
    }

    /**
     * This method waits until all the threads have finished
     */
    private void waitMiningThreads()
    {
        for(MinerThread minerThread : minerThreads)
        {
            try
            {
                minerThread.join();
            }
            catch(InterruptedException e)
            {
                Commons.logger.warning("It has been invoked the interrupt during the mining");
            }
        }
    }

    /*
     * @return the difficulty of the blockchain
     */
    protected String getMiningDifficultyToString()
    {
        return miningDifficulty;
    }

    /**
     * This method sets the stop flag used by the mining threads
     * @param stopThreads value of the flag
     */
    protected synchronized void setStopThreads(boolean stopThreads)
    {
        this.stopThreads = stopThreads;
    }

    /**
     * @return the stop flag
     */
    protected synchronized boolean isStopThreads()
    {
        return stopThreads;
    }

    /**
     * This method stops the miner manager
     */
    public void stopMiner()
    {
        Commons.logger.info("MinerManager stopped");

        interrupt();
        setStopThreads(true);

        stopMiner = true;
    }

    /**
     * This function stops the mining process
     */
    public void stopMining()
    {
        Commons.logger.info("Stopping mining");

        setStopThreads(true);
    }

    /**
     * @return the id of the current mining block
     */
    public int getIDMiningBlock()
    {
        return currentMiningBlock.getID();
    }
}