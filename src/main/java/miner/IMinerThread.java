package miner;

import blockchain.blocks.IBlock;

public interface IMinerThread
{
    /**
     * @return the mined block if this is the winner thread, null otherwise
     */
    IBlock getMinedBlock();
}