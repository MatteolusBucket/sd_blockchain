package wallet;

import utils.fields.Address;

import java.security.PublicKey;

public interface IWalletApp
{
    /**
     * Create new payment, if an old payment already exist, delete it by default
     *
     * @return true if new payment is created false otherwise
     */
    boolean newPayment();

    /**
     * Public key of the wallet
     * @return the public key of the wallet
     */
    String getPublicKey();

    /**
     * Return the status of the payment not already sent
     * @return local payment status
     */
    String paymentStatus();

    /**
     * Delete the local payment
     *
     * @return true if delete an existing payment or if no one payment is found, false if some errors occurs
     */
    boolean deletePayment();

    /**
     * This method is used to add the receiver into the payment
     *
     * @param amount            that I want to send > 0
     * @param addressReceiver   the receiver of my payment
     * @param receiverPublicKey the receiver public key
     * @return true if the payment receiver is correctly added, false otherwise
     */
    boolean addPaymentReceiver(float amount, Address addressReceiver, PublicKey receiverPublicKey);

    /**
     * Return the expendable money differ to getAmount that does not consider the not confirmed transaction
     *
     * @return the expendable money, not the total one
     */
    float getExpendableAmount();

    /**
     * Return the amount of a wallet, some of this money could not be exchangeable due to transactions
     * not already confirmed into the chain.
     * @return the amount owned by the wallet.
     */
    float getAmount();

    /**
     * Show the status of the wallet from the transaction confirmation or not point of view
     *
     * @return a list with all the transaction regarding the wallet and their status
     */
    String getStatus();

    /**
     * Send the payment
     *
     * @return true if send correctly the payment, false otherwise
     */
    boolean sendPayment();

    /**
     *
      * @return the address of the wallet
     */
    Address getAddress();

    /**
     * Stop the wallet
     */
    void stopWalletApp();

    /**
     * Print the status of the network
     */
    void networkStatus();
}
