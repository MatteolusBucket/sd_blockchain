package wallet;

import blockchain.BlockChain;
import blockchain.IBlockChain;
import incoming.IncomingManager;
import blockchain.transactions.ITransaction;
import blockchain.transactions.Transaction;
import incoming.WatchDog;
import utils.Commons;
import utils.Security;
import utils.dataManager.DataInit;
import utils.fields.Address;
import utils.fields.IDTransaction;
import utils.fields.InputTransaction;
import uiManager.UiManager;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;

/**
 * This class is the main application for the wallet
 */
public class WalletApp extends Thread implements IWalletApp
{
    private long WAITING_TIME_USAGE_CONFIRMATION_THRESHOLD;
    private BlockChain blockChain;
    private UiManager uiManager;
    private IncomingManager incomingManager;
    private WatchDog watchDog;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private Address walletAddress;
    private TransactionCache transactionCache;
    private static final int THREAD_WAIT_ATTEMPTS = 10;
    public final float TRANSACTION_FEE = 1.0f;
    private Payment payment;

    /**
     * Constructor of the class
     * @param initialHosts is the array with the initial host to connect
     * @param localPort is the listening port for the app
     * @param address is the wallet address
     * @param pathDirectoryBlockChain is the local path of the directory that contains the blockchain file
     * @param fileNameBlockChain is the filename of the blockchain
     * @param pathDirectoryConfig is the local path of the directory that contains the config file
     * @param fileNameConfig is the filename of the config
     */
    public WalletApp(String[] initialHosts, int localPort, Address address, String pathDirectoryBlockChain, String fileNameBlockChain, String pathDirectoryConfig, String fileNameConfig)
    {
        // used to laod data from the local directory
        DataInit dataInit = new DataInit(pathDirectoryBlockChain, fileNameBlockChain, pathDirectoryConfig, fileNameConfig, address);

        // if this data are already defined it return them otherwise it creates new data
        walletAddress = dataInit.initAddress();
        publicKey = dataInit.initPublicKey();
        privateKey = dataInit.initPrivateKey();
        blockChain = dataInit.initBlockChain();

        // we create the instance of the classes -> later we'll start these threads
        // the connection manager
        incomingManager = new IncomingManager(this, blockChain, initialHosts, localPort, dataInit);
        //Initialize cache transaction
        transactionCache = new TransactionCache();
        transactionCache.refresh();
        // the WatchDog
        watchDog = new WatchDog(this, blockChain, transactionCache, incomingManager, dataInit);
        // the user interface manager
        uiManager = new UiManager(this, blockChain);

        WAITING_TIME_USAGE_CONFIRMATION_THRESHOLD = 30;
    }

    /**
     * We must create the run method since this class is a Thread, so we must override it
     * This function starts all the threads we have instanced above
     */
    @Override
    public void run()
    {
        incomingManager.updateBlockChain();
        incomingManager.start();

        waitThread(incomingManager);

        if (incomingManager.isAlive())
        {
            watchDog.start();

            waitThread(watchDog);

            if (watchDog.isAlive())
            {
                uiManager.start();

                waitThread(uiManager);

                if (uiManager.isAlive())
                {
                    try
                    {
                        // before stops this thread we wait the death of all the other threads :)
                        incomingManager.join();
                        watchDog.join();
                        uiManager.join();
                    } catch (InterruptedException e)
                    {
                        Commons.logger.severe("Interrupt in the WalletApp!");

                        incomingManager.stopManager();
                        watchDog.stopWatchDog();
                        uiManager.stopManager();
                    }
                }
                else
                {
                    Commons.logger.severe("Can't start the uiManager!");

                    watchDog.stopWatchDog();
                    incomingManager.stopManager();
                }
            }
            else
            {
                Commons.logger.severe("Can't start the WatchDog!");

                incomingManager.stopManager();
            }
        }
        else
        {
            Commons.logger.severe("Can't start the IncomingManager!");
        }
    }

    /**
     * Print the status of the network
     */
    public void networkStatus()
    {
        incomingManager.networkStatus();
    }

    /**
     * This function waits the creation of the thread
     * @param thread is the thread to wait
     */
    private void waitThread(Thread thread)
    {
        int attempt = 0;

        // we have a max attempts -> we don't want to wait forever :)
        while (!thread.isAlive() && attempt++ < THREAD_WAIT_ATTEMPTS)
        {
            try
            {
                Thread.sleep(100);
            }
            catch (InterruptedException e)
            {
                Commons.logger.severe("Interrupt in the WalletApp!");
            }
        }
    }

    /**
     * @return the wallet address
     */
    public Address getAddress() {
        return walletAddress;
    }

    /**
     * Create a new local transaction to fill with the specific method
     * @return true if all go correctly false if there's a not deletable payment
     */
    public boolean newPayment() {

        if (payment != null) {
            if (!deletePayment()) {
                Commons.logger.warning("Error occurs trying to delete an old payment!");
                return false;
            }
        }

        //Create payment
        payment = new Payment();

        return true;
    }

    /**
     * Delete the local build payment before sending it
     * @return always true
     */
    public boolean deletePayment() {

        transactionCache.refresh();

        if (payment != null) {

            payment = null; //Delete payment
        }

        return true;
    }

    /**
     * Method used to send money to someone, create a new local payment if it doesn't
     * exist or simply fill the local payment with a new output
     * @param amount            that I want to send > 0
     * @param addressReceiver   the receiver of my payment
     * @param receiverPublicKey the receiver public key
     * @return the result of the operation
     */
    public boolean addPaymentReceiver(float amount, Address addressReceiver, PublicKey receiverPublicKey) {

        transactionCache.refresh();

        if (payment == null) {
            newPayment();
        }

        if (amount <= 0) {
            Commons.logger.info("Bad amount required: " + amount);
            return false;
        }

        if (getExpendableAmount() <= TRANSACTION_FEE && (payment.sumIn - payment.sumOut <= TRANSACTION_FEE)) { //To save at least one money to fee
            Commons.logger.info("Try to spend money that you can't spend now");
            return false;
        } else {

            TreeMap<IDTransaction, Float> tmp = new TreeMap<>();
            float sumAchieve = 0; //sumAchieve that I have achieved must be higher that amount to spent
            float sumInTmp = payment.sumIn;
            float sumOutTmp = payment.sumOut + amount;

            for (Map.Entry<IDTransaction, Float> x : transactionCache.getAvailableTransaction().entrySet()) {

                if (!payment.inputTransaction.containsKey(x.getKey())) {
                    //Achieve the necessary amount
                    if (sumAchieve >= amount && ((sumInTmp - sumOutTmp) >= TRANSACTION_FEE)) {
                        break; //End for
                    } else {
                        tmp.put(x.getKey(), x.getValue());
                        sumAchieve += x.getValue();
                        sumInTmp += x.getValue();
                    }
                } //else the input is temporary block into payment
            }

            if (sumAchieve >= amount && ((sumInTmp - sumOutTmp) >= TRANSACTION_FEE)) {
                //Ok
                for (Map.Entry<IDTransaction, Float> t : tmp.entrySet()) {
                    payment.addInput(t.getKey(), t.getValue(), walletAddress, privateKey);
                    //                    transactionCache.addPendingInput(t.getKey());
                }

                payment.addOutput(addressReceiver, receiverPublicKey, amount);

            } else {
                Commons.logger.info("Send the previous payment before add another one or you are poor!");
                return false;
            }
        }

        return true;
    }

    /**
     *
     * @return the sum of expendable, so Valid transaction
     */
    public float getExpendableAmount() {

        transactionCache.refresh();

        float temp = 0;

        for (Map.Entry<IDTransaction, Float> i : transactionCache.getAvailableTransaction().entrySet()) {

            if (payment == null) {
                temp += i.getValue();
            } else {
                if (!payment.inputTransaction.containsKey(i.getKey())) {
                    temp += i.getValue();
                }
            }
        }

        return temp;
    }

    /**
     *
     * @return the public key of miner
     */
    public String getPublicKey() {
        return Security.getStringFromPublicKey(publicKey);
    }

    /**
     *
     * @return information regarding the wallet
     */
    public String getStatus() {

        transactionCache.refresh();

        StringBuilder status = new StringBuilder();

        status.append("\n\t\tAddress: " + walletAddress);
        status.append("\n\t\tPublic Key: " + Security.getStringFromPublicKey(publicKey));
        status.append("\n\t\tMoney: " + getAmount());
        status.append("\n\t\tExpendable Money: " + getExpendableAmount());
        status.append("\n\n\t\tTransaction status:\n");

        for (Map.Entry<IDTransaction, TData> entry : transactionCache.treeMap.entrySet()) {
            status.append("\n\t\t\t#ID: ");
            status.append(entry.getKey());
            status.append("   amount:  ");
            status.append(entry.getValue().amount);
            status.append("   status:  ");
            status.append(transactionCache.treeMapState.get(entry.getKey()).state);
        }

        if (transactionCache.treeMap.entrySet().size() == 0) {
            status.append("\n\t\tYou have no transactions");
        }

        return status.toString();
    }

    /**
     * If ok send the payment store in local
     * @return true if send without error
     */
    public boolean sendPayment() {

        boolean sendingOk = false;

        if (payment == null) {
            return false;

        } else {

            ITransaction tmp = payment.finish();

            if(blockChain.verifyTransactionLongestBranch(tmp) != IBlockChain.TRANSACTION_STATE.VALID){
                Commons.logger.info("Wrong transaction public key!!");
                deletePayment();

                //todo scartare la transazione con chiave puibblica errata in modo da nn spenderla più
                return false;
            }

            if (tmp != null) {
                sendingOk = transactionCache.addPendingTransaction(tmp);
                incomingManager.spreadTransaction(tmp);
            }
        }

        Commons.logger.info("Transaction to send: " + payment.transaction.toString());

        //Delete payment
        deletePayment();

        transactionCache.refresh();
        return sendingOk;
    }

    /**
     * Stop the wallet application
     */
    public void stopWalletApp()
    {
        watchDog.stopWatchDog();
        incomingManager.stopManager();
        uiManager.stopManager();
    }

    /**
     * Show the status of payment stored in local and not already sent
     * @return information regarding the local payment
     */
    public String paymentStatus() {
        if (payment == null) {
            return "\t\tYou have no pending payments!";
        } else {
            return payment.toString();
        }
    }

    /**
     * Not all of this money are exchangeable
     * @return the available amount of money
     */
    public float getAmount() {

        transactionCache.refresh();

        float temp = 0;

        for (Map.Entry<IDTransaction, TData> entry : transactionCache.treeMap.entrySet()) {
            if (transactionCache.treeMapState.get(entry.getKey()).state == TRANSACTION_STATE.VALID) {
                temp += entry.getValue().amount;
            } else if (transactionCache.treeMapState.get(entry.getKey()).state == TRANSACTION_STATE.WAITING_CONFIRM) {
                if (transactionCache.treeMap.get(entry.getKey()).owner == walletAddress) {
                    temp += entry.getValue().amount;
                }
            }
        }

        return temp;
    }

    /**
     * @return a tree with all the available transaction of the wallet
     */
    private TreeMap<IDTransaction, TData> getAvailableTransactionFromChain() {

        TreeMap<IDTransaction, TData> tmp = new TreeMap<>();

        for (Map.Entry<IDTransaction, Float> entry : blockChain.getAddressAvailableIDTransactions(walletAddress).entrySet()) {
            tmp.put(entry.getKey(), new TData(entry.getValue(), walletAddress));
        }

        return tmp;
    }

    private class TOutputData {
        public float amount;
        public PublicKey pk;
        public Address addressReceiver;

        TOutputData(Address address, PublicKey publicKey, float amount) {
            this.addressReceiver = address;
            this.pk = publicKey;
            this.amount = amount;
        }
    }

    /**
     * A class to store a payment in local before sending it
     */
    private class Payment {

        private ITransaction transaction;
        private ArrayList<PrivateKey> privateKeys;

        private boolean finished = false;

        public float sumIn = 0;
        public float sumOut = 0;

        public TreeMap<IDTransaction, Float> inputTransaction = new TreeMap<>();
        public TreeMap<Integer, TOutputData> outputTransaction = new TreeMap<>();

        Payment() {

            //Create transaction
            this.transaction = new Transaction();
            //l'idea è che un wallet possa pagare gente con indirizzi e chiavi private varie
            this.privateKeys = new ArrayList<>();
        }

        /**
         * This method is used to add the required input transaction to satisfy the
         * output amount required plus fee
         * @param transaction id of transaction to use
         * @param amount the amount of the transaction used
         * @param privateKey private key with we have to sign the transaction for this input
         * @return true if all is correct
         */
        boolean addInput(IDTransaction transaction, float amount, Address addressWallet, PrivateKey privateKey) {

            Commons.logger.info("Amount addInput" + amount);


            if (finished) {
                Commons.logger.warning("Try to add Input to a payment already finished");
                return false;
            }

            this.inputTransaction.put(transaction, amount);
            this.privateKeys.add(privateKey);
            sumIn += amount;
            return true;
        }

        /**
         * Insert a new receiver into the transaction
         * @param addressReceivere receiver address
         * @param public_Key_Receiver public key of the receiver address
         * @param amount the amount that I want to share
         * @return true if correct false otherwise
         */
        boolean addOutput(Address addressReceivere, PublicKey public_Key_Receiver, float amount) {

            if (finished) {
                Commons.logger.warning("Try to add Output to a payment already finished");
                return false;
            }

            this.outputTransaction.put(outputTransaction.size() + 1, new TOutputData(addressReceivere, public_Key_Receiver, amount));
            sumOut += amount;
            return true;
        }

        /**
         * The method is used to finish a transaction, insert the signature and check
         * the correctness
         * @return the finished transaction
         */
        ITransaction finish() {

            if (finished) {
                Commons.logger.warning("Try to finish a payment that is already finish");
                return this.transaction;
            }

            if (inputTransaction.size() == 0) {
                return null;
            }

            //Add the input transaction in the real payment
            for (Map.Entry<IDTransaction, Float> i : inputTransaction.entrySet()) {
                transaction.addInput(i.getKey(), walletAddress, i.getValue());
            }

            for (Map.Entry<Integer, TOutputData> entry : outputTransaction.entrySet()) {
                transaction.addOutput(entry.getValue().addressReceiver, entry.getValue().pk, entry.getValue().amount);
            }

            Commons.logger.info("The transaction have sumin: " + transaction.getSumIn() + " sum out:  " + transaction.getSumOut());

            float moneyBack = this.transaction.getSumIn() - this.transaction.getSumOut();
            moneyBack += -TRANSACTION_FEE; //For miner reward
            this.transaction.addOutput(walletAddress, publicKey, moneyBack);

            Commons.logger.fine("SumIn: " + this.transaction.getSumIn() + " moneyBak: " + moneyBack + "\nShould be remained " + TRANSACTION_FEE + " money");

            if (this.transaction.finish(this.privateKeys)) {
                finished = true;
                return this.transaction;
            }

            return null;
        }

        /**
         * Method to acquire information about the state of transaction stored in cache
         * @return a string with information regarding transaction stored in cache
         */
        @Override
        public String toString() {

            StringBuilder tmp = new StringBuilder();

            tmp.append("\t\t#ID: " + transaction.getID());
            tmp.append("\n\t\t\tInput: ");

            for (Map.Entry<IDTransaction, Float> e : inputTransaction.entrySet()) {
                tmp.append("\n\t\t\t\t\tID: ");
                tmp.append(e.getKey());
                tmp.append("   Amount: ");
                tmp.append(e.getValue());
            }

            tmp.append("\n\t\t\tOutput: ");


            for (Map.Entry<Integer, TOutputData> e : outputTransaction.entrySet()) {
                tmp.append("\n\t\t\t\t\tAddress Receiver: ");
                tmp.append(e.getValue().addressReceiver);
                tmp.append("  Amount: ");
                tmp.append(e.getValue().amount);
            }

            return tmp.toString();
        }

    }


    /**
     * All the state is referred to the longest branch of the chain
     * VALID: expendable transaction
     * USED: an already spent transaction
     * WAITING_CONFIRM: a transaction that is waiting to be mined
     * WAITING_USAGE_CONFIRMATION: transaction spent in a new transaction not already mined
     */
    enum TRANSACTION_STATE {VALID, USED, WAITING_CONFIRM, WAITING_USAGE_CONFIRMATION}

    /**
     * This is the cache for a wallet, it contains all the transaction referred to
     * the address of the wallet
     */
    public class TransactionCache {

        /**
         * Each transaction have a state and a time which the state is referred
         */
        private class TState {

            TRANSACTION_STATE state = TRANSACTION_STATE.VALID;
            long time;

            TState(TRANSACTION_STATE state, long time) {
                this.state = state;
                this.time = time;
            }

            TState(TRANSACTION_STATE state) {
                this.state = state;
                this.time = System.currentTimeMillis();
            }
        }

        private TreeMap<IDTransaction, TData> treeMap;
        private TreeMap<IDTransaction, TState> treeMapState;

        public TransactionCache() {
            treeMap = getAvailableTransactionFromChain(); //take the available transaction according to the chain
            treeMapState = new TreeMap<>();

            //Initialize the treeMapState
            for (IDTransaction idT : treeMap.keySet()) {
                treeMapState.put(idT, new TState(TRANSACTION_STATE.VALID));
            }
        }

        /**
         * This method compare the local cache with the cache of blockchain ad
         * check the availability of new transaction
         */
        public void refresh() {

            treeMap = getAvailableTransactionFromChain(); //Take the valid transaction from the Blockchain

            StringBuilder output = new StringBuilder();
            output.append("\n\nAll'inizo del refresh: \ntreeMap:");
            for (Map.Entry<IDTransaction, TData> entry : treeMap.entrySet()) {
                output.append("\nId: "+ entry.getKey()+" value: "+ entry.getValue().amount+ " owner: "+entry.getValue().owner);
            }
            output.append("\ntreeMapState :");
            for (Map.Entry<IDTransaction, TState> entry: treeMapState.entrySet()) {
                output.append("\nId: "+entry.getKey() + "    "+entry.getValue().state);
            }

            for (IDTransaction idT : treeMap.keySet()) {
                if (treeMapState.containsKey(idT)) { //se c'era già probabilmente aveva un altro stato od era uguale

                    TState t = treeMapState.get(idT);

                    if (t.state == TRANSACTION_STATE.WAITING_CONFIRM) {
                        //Means that the user is waiting that new transaction is confirmed
                        t.state = TRANSACTION_STATE.VALID;
                        //                        treeMapState.put(idT, new TState(TRANSACTION_STATE.VALID));
                    } else if (t.state == TRANSACTION_STATE.WAITING_USAGE_CONFIRMATION) {
                        //if older than a threshold probably the transaction was rejected


                        if ((System.currentTimeMillis() - t.time) > (WAITING_TIME_USAGE_CONFIRMATION_THRESHOLD * 1000)) {
                            //transazione che era in waiting e ora torna valida in quanto è passato troppo tempo
                            t.state = TRANSACTION_STATE.VALID;
                            Commons.logger.info("Restore a transaction after expiring the timeout");
                        }
                    }

                } else {
                    treeMapState.put(idT, new TState(TRANSACTION_STATE.VALID));
                }
            }

            boolean needCacheRefresch = false;

            List<IDTransaction> badConfirmation = new LinkedList<>();

            for (IDTransaction idT : treeMapState.keySet()) {

                TState t = treeMapState.get(idT);

                if (treeMap.containsKey(idT)) {
                    //DO NOTHING

                } else if (t.state == TRANSACTION_STATE.WAITING_USAGE_CONFIRMATION) {
                    treeMapState.put(idT, new TState(TRANSACTION_STATE.USED));
                } else if (t.state == TRANSACTION_STATE.WAITING_CONFIRM) {
                    if ((System.currentTimeMillis() - t.time) > (WAITING_TIME_USAGE_CONFIRMATION_THRESHOLD * 1000 * 2)) {
                        //Transaction on waiting confirmation state but expire the timeout so delete it
                        badConfirmation.add(idT);
                        Commons.logger.info("Cache delete a transaction that is not confirmed: " + idT);
                        needCacheRefresch = true;
                    }
                }
            }

            for (IDTransaction id : badConfirmation) {
                treeMapState.remove(id);
            }

            output.append("\n\nAt the end of refresh: \ntreeMap:");
            for (Map.Entry<IDTransaction, TData> entry : treeMap.entrySet()) {
                output.append("\nId: " + entry.getKey() + " value: " + entry.getValue().amount);

            }
            output.append("\ntreeMapState :" + "");
            for (Map.Entry<IDTransaction, TState> entry : treeMapState.entrySet()) {
                output.append("\nId: " + entry.getKey() + "    " + entry.getValue().state);
            }

            Commons.logger.finest(output.toString());

            if (needCacheRefresch) {
                this.rebuildCache();
            }


        }


        /**
         * This command delete and rebuild cache from zero!
         */
        public void rebuildCache() {

            treeMap = getAvailableTransactionFromChain(); //take the available transaction according to the chain
            treeMapState = new TreeMap<>();

            //Initialize the treeMapState
            for (IDTransaction idT : treeMap.keySet()) {
                treeMapState.put(idT, new TState(TRANSACTION_STATE.VALID));
            }
            Commons.logger.info("Rebuild cache!");

        }

        /**
         * Set the input of a new transaction as waiting_usage and the transaction ad waiting confirm
         * @param t a new transaction to update local cache
         * @return if all go correctly
         */
        public boolean addPendingTransaction(ITransaction t) {
            treeMapState.put(t.getID(), new TState(TRANSACTION_STATE.WAITING_CONFIRM));


            //All input are pending for usage confirmation so I can't spent
            for (InputTransaction inputT : t.getInputTransaction()) {
                treeMapState.put(inputT.getID(), new TState(TRANSACTION_STATE.WAITING_USAGE_CONFIRMATION));
            }

            Commons.logger.fine("\n\nInside add Pending:\nTreeMapState: " + treeMapState);

            return true;
        }

        /**
         * Method to retrieve available transaction from local cache
         * @return a treeMap with the key of transactions available and their output
         */
        public TreeMap<IDTransaction, Float> getAvailableTransaction() {

            transactionCache.refresh();

            Commons.logger.fine("\n\nTree state nel getAvailable: " + treeMapState);


            TreeMap<IDTransaction, Float> temp = new TreeMap<>();

            try {

                for (IDTransaction idT : treeMapState.keySet()) {
                    if (treeMapState.get(idT).state == TRANSACTION_STATE.VALID)
                        temp.put(idT, treeMap.get(idT).amount);
                }

            } catch (NullPointerException e) {
                Commons.logger.info("Get available return empty tree");

                return temp;
            }

            return temp;
        }
    }

    private class TData {
        float amount;
        Address owner;

        TData(float amount, Address address) {
            this.amount = amount;
            this.owner = address;
        }
    }
}
